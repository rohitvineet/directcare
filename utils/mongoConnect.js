/**
 * Created by Priya Sethi on 11/09/15.
 */

'use strict';
var Mongoose = require('mongoose');
var Config = require('../config');



//Connect to MongoDB
Mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

exports.Mongoose = Mongoose;