'use strict';

module.exports = {
  APP_CONSTANTS: require('./appConstants'),
	serverConfig: require('./serverConfig'),
	dbConfig: require('./dbConfig'),
    awsS3Config: require('./awsS3Config'),
    emailConfig: require('./emailConfig'),
    constants: require('./constants')
};
