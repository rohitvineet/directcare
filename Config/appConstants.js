'use strict';
var Fs = require('fs');

var SERVER = {
    APP_NAME: 'Direct Care',
    http:{port:3000},
    PORTS: {
        HAPI: 3000
    },
    TOKEN_EXPIRATION_IN_MINUTES: 600,
    JWT_SECRET_KEY: 'sUPerSeCuREKeY&^$^&$^%$^%7782348723t4872t34Ends',
    GOOGLE_API_KEY: 'AIzaSyDg8H_HJRFqwmQjJ37s5GSaiXSUqDt3caI',
    COUNTRY_CODE: '+91',
    MAX_DISTANCE_RADIUS_TO_SEARCH: '1',
    THUMB_WIDTH: 50,
    THUMB_HEIGHT: 50,
    BASE_DELIVERY_FEE: 25,
    COST_PER_KM: 9, // In USD
    DOMAIN_NAME: 'http://localhost:3000/',
    SUPPORT_EMAIL: 'support@click-labs.com'
};

var DATABASE = {
    BOOKING_TYPE:{
        SHORT:'SHORT',
        LONG:'LONG'
    },
    BOOKING_STATUS: {
        NOT_ACCEPTED: 'NOT_ACCEPTED',
        NOT_STARTED: 'NOT_STARTED',
        WAITING: 'WAITING',
        BOOKED: 'BOOKED',
        ONGOING: 'ONGOING',
        STARTED: 'STARTED',
        COMPLETED: 'COMPLETED',
        CANCELLED: 'CANCELLED',
        DECLINED: 'DECLINED',
        EXPIRED: 'EXPIRED',
        DROPPED: 'DROPPED',
        REACHED: 'REACHED',
        PENDING: 'PENDING',
        ARRIVED:'ARRIVED'
    },
    ADMIN_TYPE:{
        SUPER_ADMIN:'SUPER_ADMIN',
        FINANCE_ADMIN:"FINANCE_ADMIN",
        SUB_ADMIN:'SUB_ADMIN'
    },
    PROMO_TYPE:{
        PERCENT:'PERCENT',
        AMOUNT:"AMOUNT"
    },
    PAYMENT_STATUS: {
        WAITING: 'WAITING',
        COMPLETED: 'COMPLETED',
        DECLINED: 'DECLINED',
        HOLD: 'HOLD',
        REFUND: 'REFUND'
    },
    GENDER: {
        MALE: 'MALE',
        FEMALE: 'FEMALE',
        EITHER: 'EITHER'
    },
    MASSAGE_TYPE: {
        SWEDISH: 'SWEDISH',
        DEEPTISSUE: 'DEEPTISSUE',
        PRENATAL: 'PRENATAL',
        SPORTMESSAGE: 'SPORTMESSAGE'
    },
    PROFILE_PIC_PREFIX: {
        ORIGINAL: 'profilePic_',
        THUMB: 'profileThumb_'
    },
    LOGO_PREFIX: {
        ORIGINAL: 'logo_',
        THUMB: 'logoThumb_'
    },
    DOCUMENT_PREFIX: 'document_',
    USER_ROLES: {
        ADMIN: 'ADMIN',
        SUBADMIN: 'SUBADMIN',
        PATIENT: 'PATIENT',
        DOCTOR: 'DOCTOR',
        DIAGNOSTIC:'DIAGNOSTIC'
    },
    FILE_TYPES: {
        LOGO: 'LOGO',
        DOCUMENT: 'DOCUMENT',
        OTHERS: 'OTHERS'
    },
    VEHICLE_TYPE: {
        BICYCLE: 'BICYCLE',
        SCOOTER: 'SCOOTER',
        CAR: 'CAR'
    },
    DEVICE_TYPES: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },
    LANGUAGE: {
        EN: 'EN',
        ES_MX: 'ES_MX'
    },
    PAYMENT_OPTIONS: {
        CREDIT_DEBIT_CARD: 'CREDIT_DEBIT_CARD',
        PAYPAL: 'PAYPAL',
        BITCOIN: 'BITCOIN',
        GOOGLE_WALLET: 'GOOGLE_WALLET',
        APPLE_PAY: 'APPLE_PAY',
        EIYA_CASH: 'EIYA_CASH'
    }
};

var STATUS_MSG = {
    ERROR: {
        INVALID_LAT_LONG: {
            statusCode: 400,
            customMessage: 'Please enter valid latitude and longitude.',
            type: 'INVALID_LAT_LONG'
        },
        INVALID_BOOKING:{
            statusCode: 400,
            customMessage: 'Please enter valid booking Id.',
            type: 'INVALID_BOOKING'
        },
        EXPIRED_OTP:{
            statusCode: 400,
            customMessage: 'OTP has Expired',
            type: 'EXPIRED_OTP'

        },
        ALREADY_USED_PHONENUMBER:{
            statusCode: 400,
            customMessage: 'Phone Number is already in use.',
            type: 'ALREADY_USED_PHONENUMBER'
        },
        ALREADY_UNBLOCKED: {
            statusCode: 400,
            customMessage: 'User already unblocked',
            type: 'ALREADY_UNBLOCKED'
        },
        ALREADY_BLOCKED: {
            statusCode: 400,
            customMessage: 'User already blocked',
            type: 'ALREADY_BLOCKED'
        },
        PAYMENT_CARD_ERROR: {
            statusCode: 400,
            customMessage: 'Payment Card Error.',
            type: 'PAYMENT_CARD_ERROR'
        },
        INVALID_MASSAGE: {
            statusCode: 400,
            customMessage: 'Please enter valid massage details according to session type',
            type: 'INVALID_MASSAGE'
        },
        INCORRECT_EMAIL: {
            statusCode: 400,
            customMessage: 'Email not registered with us.',
            type: 'INCORRECT_EMAIL'
        },
        EMAIL_NOT_VERIFIED:{
            statusCode: 400,
            customMessage: 'Email not verified. Please check your mail.',
            type: 'EMAIL_NOT_VERIFIED'
        },
        ADMIN_ALREADY_REGISTERED: {
            statusCode: 400,
            customMessage: 'Admin already registered with this email.',
            type: 'ADMIN_ALREADY_REGISTERED'
        },
        CALL_CONNECT: {
            statusCode: 400,
            customMessage: 'Unable to connect.',
            type: '  CALL_CONNECT'
        },
        ALREADY_REJECTED: {
            statusCode: 400,
            customMessage: 'Already rejected booking . Now cannot accept',
            type: 'ALREADY_REJECTED'
        },
        INVALID_LAT: {
            statusCode: 400,
            customMessage: 'Invalid latitude and longitude.',
            type: 'INVALID_LAT'
        },
        THERAPIST_REJECT: {
            statusCode: 400,
            customMessage: 'You have already reject this booking.',
            type: 'THERAPIST_REJECT'
        },
        NO_THERAPIST: {
            statusCode: 400,
            customMessage: 'No therapist available.',
            type: ' NO_THERAPIST'
        },
        NO_BOOKINGS: {
            statusCode: 400,
            customMessage: 'No Bookings for you.',
            type: 'NO_BOOKINGS'
        },
        SUBSCRIPTION_EXPIRED: {
            statusCode: 400,
            customMessage: 'Subscription selected has expired',
            type: 'SUBSCRIPTION_EXPIRED'
        },
        NO_RECEIVED: {
            statusCode: 400,
            customMessage: 'Not received any gift Card.',
            type: 'NO_RECEIVED'
        },
        TOKEN: {
            statusCode: 400,
            customMessage: 'Please enter valid token.',
            type: 'TOKEN'
        },
        NOT_SERVICE: {
            statusCode: 400,
            customMessage: 'Sorry!! Prices are not set at this location. You can select any other massage.',
            type: ' NOT_SERVICE'
        },
        VALID_COMPANY: {
            statusCode: 400,
            customMessage: 'Please enter valid company address.',
            type: 'VALID_COMPANY'
        },
        VALID_TRUCK: {
            statusCode: 400,
            customMessage: 'Please enter valid truck Id.',
            type: 'VALID_TRUCK'
        },
        ALREADY_DELETE: {
            statusCode: 400,
            customMessage: 'Already a deleted address',
            type: 'ALREADY_DELETE'
        },
        ALREADY_ACCEPTED: {
            statusCode: 400,
            customMessage: 'You have already accepted this booking. Cannot reject now',
            type: 'ALREADY_ACCEPTED'
        },
        ALREADY_BUSY: {
            statusCode: 400,
            customMessage: 'In Busy State.',
            type: 'ALREADY_BUSY'
        },
        ALREADY_CLAIMED_JOB: {
            statusCode: 400,
            customMessage: 'This job is now not in accepted state.',
            type: 'ALREADY_CLAIMED_JOB'
        },
        ALREADY_COMPLETED_JOB: {
            statusCode: 400,
            customMessage: 'Already Completed Job.',
            type: 'ALREADY_COMPLETED_JOB'
        },
        INVALID_ACTION_ADDRESS: {
            statusCode: 400,
            customMessage: 'Address Id does not exists.',
            type: 'INVALID_ACTION'
        },
        DEFAULT_NOT_DELETE: {
            statusCode: 400,
            customMessage: 'You cannot delete default Address.',
            type: ' DEFAULT_NOT_DELETE'
        },
        NOT_LOCATION: {
            statusCode: 400,
            customMessage: 'Sorry!! We do not serve services here.',
            type: 'NOT_LOCATION'
        },
        DEFAULTCARD_NOT_DELETE: {
            statusCode: 400,
            customMessage: 'You cannot delete default card.',
            type: 'DEFAULTCARD_NOT_DELETE'
        },
        INVALID_ACTION: {
            statusCode: 400,
            customMessage: 'Invalid Action',
            type: 'INVALID_ACTION'
        },
        INVALID_APPOINTMENT: {
            statusCode: 400,
            customMessage: 'Invalid Appointment',
            type: 'INVALID_APPOINTMENT'
        },
        NOT_DELETED: {
            statusCode: 400,
            customMessage: 'Unable To Delete.',
            type: 'NOT_DELETED'
        },
        ALREADY_DELETED: {
            statusCode: 400,
            customMessage: 'Card Already Deleted.',
            type: 'ALREADY_DELETED'
        },
        SET_DEFAULT_ERROR: {
            statusCode: 400,
            customMessage: 'Unable to set this as default',
            type: 'SET_DEFAULT_ERROR'
        },
        INVALID_CAR: {
            statusCode: 400,
            customMessage: 'Please enter valid amount according to your card limit.',
            type: 'INVALID_CARD'
        },
        INVALID_CARD: {
            statusCode: 400,
            customMessage: 'Please Enter valid card details.',
            type: 'INVALID_CARD'
        },
        INCORRECT_ACCESSTOKEN: {
            statusCode: 403,
            customMessage: 'Incorrect AccessToken',
            type: 'INCORRECT_ACCESSTOKEN'
        },
        INCORRECT_RESET_TOKEN: {
            statusCode: 403,
            customMessage: 'Incorrect Reset Token',
            type: 'INCORRECT_RESET_TOKEN'
        },
        EMAIL_DOMAIN_ERROR: {
            statusCode: 400,
            customMessage: 'Please enter valid Email Address',
            type: 'EMAIL_DOMAIN_ERROR'
        },
        USER_ALREADY_REGISTERED: {
            statusCode: 409,
            customMessage: 'Your Email Address or Mobile No. is already registered',
            type: 'USER_ALREADY_REGISTERED'
        },
        MOBILE_NO_ALREADY_USED: {
            statusCode: 409,
            customMessage: 'Mobile No. is already used.',
            type: 'MOBILE_NO_ALREADY_USED'
        },
        DUPLICATE_FACEBOOK_ID: {
            statusCode: 400,
            customMessage: 'You are already registered with us.',
            type: 'DUPLICATE_FACEBOOK_ID'
        },
        NOT_UPDATED: {
            statusCode: 501,
            customMessage: 'Unable To Update',
            type: 'NOT_UPDATED'
        },
        NOT_UPDATE: {
            statusCode: 401,
            customMessage: 'New password must be different from old password',
            type: 'NOT_UPDATE'
        },
        WRONG_PASSWORD: {
            statusCode: 400,
            customMessage: 'Invalid old password',
            type: 'WRONG_PASSWORD'
        },
        USER_NOT_FOUND: {
            statusCode: 401,
            customMessage: 'User not found.',
            type: 'USER_NOT_FOUND'
        },
        NOT_VERFIFIED: {
            statusCode: 401,
            customMessage: 'User Not Verified',
            type: 'NOT_VERFIFIED'
        },
        INCORRECT_ID: {
            statusCode: 401,
            customMessage: 'Incorrect User',
            type: 'INCORRECT_ID'
        },
        INCORRECT_MOBILE: {
            statusCode: 401,
            customMessage: 'Incorrect Mobile Number',
            type: 'INCORRECT_MOBILE'
        },
        PASSWORD_CHANGE_REQUEST_EXPIRE: {
            statusCode: 400,
            customMessage: ' Password change request time expired',
            type: 'PASSWORD_CHANGE_REQUEST_EXPIRE'
        },
        PASSWORD_CHANGE_REQUEST_INVALID: {
            statusCode: 400,
            type: 'PASSWORD_CHANGE_REQUEST_INVALID',
            customMessage: 'Invalid password change request.'
        },
        INVALID_USER_PASS: {
            statusCode: 401,
            type: 'INVALID_USER_PASS',
            customMessage: 'Invalid password'
        },
        TOKEN_ALREADY_EXPIRED: {
            statusCode: 401,
            customMessage: 'Token Already Expired',
            type: 'TOKEN_ALREADY_EXPIRED'
        },
        BLOCK_USER:{
            statusCode: 400,
            customMessage: 'You are blocked by admin.',
            type: 'BLOCK_USER'
        },
        SAMPLE_NOT_COLLECTED:{
            statusCode: 400,
            customMessage: 'Please collect sample before ending appointment',
            type: 'SAMPLE_NOT_COLLECTED'
        },
        DB_ERROR: {
            statusCode: 400,
            customMessage: 'DB Error : ',
            type: 'DB_ERROR'
        },
        INVALID_ID: {
            statusCode: 400,
            customMessage: 'Invalid Id Provided : ',
            type: 'INVALID_ID'
        },
        INCORRECT_LOCATION: {
            statusCode: 400,
            customMessage: 'Incorrect Location Provided : ',
            type: 'INCORRECT_LOCATION'
        },
        PREVIOUS_BUSY: {
            statusCode: 400,
            customMessage: 'Previous Therapist is busy.',
            type: 'PREVIOUS_BUSY'

        },
        ADMIN_EMAIL:{
            statusCode: 400,
            customMessage: 'Please register with some another email. You are already registered with this email.',
            type: 'ADMIN_EMAIL'

        },
        DOMAIN_EXIST:{
            statusCode: 400,
            customMessage: 'Domain of service entered already exists.',
            type: 'DOMAIN_EXIST'
        },
        DOMAIN_NOT_EXIST:{
            statusCode: 400,
            customMessage: 'Domain of service entered do not exists.',
            type: 'DOMAIN_NOT_EXIST'
        },
        TYPE_EXIST:{
            statusCode: 400,
            customMessage: 'Type of Service entered already exists.',
            type: 'TYPE_EXIST'
        },
        TYPE_NOT_EXIST:{
            statusCode: 400,
            customMessage: 'Type of Service entered do not exists.',
            type: 'TYPE_NOT_EXIST'
        },
        SUBSCRIPTION_EXIST:{
            statusCode: 400,
            customMessage: 'Subscription entered already exists.',
            type: 'SUBSCRIPTION_EXIST'
        },
        SUBSCRIPTION_ALREADY_BOUGHT:{
            statusCode: 400,
            customMessage: 'Subscription already bought.',
            type: 'SUBSCRIPTION_ALREADY_BOUGHT'
        },
        PROMOCODE_EXIST:{
            statusCode: 400,
            customMessage: 'Promo Code entered already exists.',
            type: 'PROMOCODE_EXIST'
        },
        PROMOCODE_INVALID:{
            statusCode: 400,
            customMessage: 'Promo code entered is invalid.',
            type: 'PROMOCODE_INVALID'
        },
        PROMOCODE_MAXUSAGE:{
            statusCode: 400,
            customMessage: 'Promo code has reached its maximum usage.',
            type: 'PROMOCODE_MAXUSAGE'
        },
        NO_SUBSCRIPTION:{
            statusCode: 400,
            customMessage: 'No available Subscription.',
            type: 'NO_SUBSCRIPTION'
        },
        INVALID_SUBSCRIPTION:{
            statusCode: 400,
            customMessage: 'Invalid Subscription.',
            type: 'INVALID_SUBSCRIPTION'
        },
        DIAGNOSTIC_EXIST:{
            statusCode: 400,
            customMessage: 'Diagnostic type of service entered already exists.',
            type: 'DIAGNOSTIC_EXIST'
        },
        DIAGNOSTIC_NOT_EXIST:{
            statusCode: 400,
            customMessage: 'Diagnostic type of service entered do not exists.',
            type: 'DIAGNOSTIC_NOT_EXIST'
        },
        PRICE_LIST_EXIST:{
            statusCode: 400,
            customMessage: 'Pricing already exists co-responding to domain of service  and type of service.',
            type: 'PRICE_LIST_EXIST'
        },
        GEO_EXIST:{
            statusCode: 400,
            customMessage: 'Geo-Fencing Area entered already exists.',
            type: 'GEO_EXIST'
        },
        APP_ERROR: {
            statusCode: 400,
            customMessage: 'Application Error',
            type: 'APP_ERROR'
        },
            CANNOT_END:{
                statusCode: 400,
                customMessage: 'You cannot end this service now.',
                type: 'CANNOT_END'
            },
        CANNOT_START: {
            statusCode: 400,
            customMessage: 'You cannot start this service now.',
            type: 'CANNOT_START'
        },
        START_TIME: {
            statusCode: 400,
            customMessage: 'Please select Time according to first therapist.',
            type: 'START_TIME'
        },
        ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Address not found',
            type: 'ADDRESS_NOT_FOUND'
        },
        ADDRESS_DELETED: {
            statusCode: 400,
            customMessage: 'this address was deleted by you. Please enter valid address',
            type: ' ADDRESS_DELETED'
        },
        CARD_DELETED: {
            statusCode: 400,
            customMessage: 'this card was deleted by you. Please enter valid card',
            type: ' CARD_DELETED'
        },
        NOT_ASSIGNED: {
            statusCode: 400,
            customMessage: 'BOOKING NOT ASSIGNED TO YOU',
            type: 'NOT_ASSIGNED'
        },
        Valid_DIMENSIONS: {
            statusCode: 400,
            customMessage: 'Please enter valid dimensions.',
            type: 'Valid_DIMENSIONS'
        },
        SAME_ADDRESS_ID: {
            statusCode: 400,
            customMessage: 'Massage type with same area already exists.',
            type: 'SAME_ADDRESS_ID'
        },
        PICKUP_ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Pickup Address not found',
            type: 'PICKUP_ADDRESS_NOT_FOUND'
        },
        DELIVERY_ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Delivery Address not found',
            type: 'DELIVERY_ADDRESS_NOT_FOUND'
        },
        IMP_ERROR: {
            statusCode: 500,
            customMessage: 'Implementation Error',
            type: 'IMP_ERROR'
        },
        APP_VERSION_ERROR: {
            statusCode: 400,
            customMessage: 'One of the latest version or updated version value must be present',
            type: 'APP_VERSION_ERROR'
        },
        INVALID_TOKEN: {
            statusCode: 401,
            customMessage: 'Invalid token provided',
            type: 'INVALID_TOKEN'
        },
        INVALID_CODE: {
            statusCode: 400,
            customMessage: 'Please enter correct verification Code.',
            type: 'INVALID_CODE'
        },
        DEFAULT: {
            statusCode: 400,
            customMessage: 'Error',
            type: 'DEFAULT'
        },
        INVALID_DATES: {
            statusCode: 400,
            customMessage: 'Please enter valid dates according to massageTime',
            type: 'INVALID_DATES'
        },
        WRONG_ID: {
            statusCode: 400,
            customMessage: 'Please enter valid booking Id',
            type: 'WRONG_ID'
        },
        DELETE_CARD: {
            statusCode: 400,
            customMessage: 'You cannot make booking with deleted card.',
            type: 'DELETE_CARD'
        },
        DISCOUNT: {
            statusCode: 400,
            customMessage: 'Both gift Card and promo code cannot be used simultaneously',
            type: 'DISCOUNT'
        },
        PHONE_NO_EXIST: {
            statusCode: 400,
            customMessage: 'Mobile No Already Exist',
            type: 'PHONE_NO_EXIST'
        },
        PHONE_VERIFICATION: {
            statusCode: 400,
            customMessage: 'You are not registered with us.',
            type: 'PHONE_VERIFICATION'
        },
        PHONE_VERIFICATION_COMPLETE: {
            statusCode: 400,
            customMessage: 'Your mobile number verification is already completed.',
            type: 'PHONE_VERIFICATION_COMPLETE'
        },
        EMAIL_EXIST: {
            statusCode: 400,
            customMessage: 'Email Already Exist',
            type: 'EMAIL_EXIST'
        },
        INVALID_STRIPE_ID: {
            statusCode: 400,
            customMessage: 'Send valid stripe Id',
            type: 'INVALID_STRIPE_ID'
        },
        DUPLICATE: {
            statusCode: 400,
            customMessage: 'Duplicate Entry',
            type: 'DUPLICATE'
        },
        DUPLICATE_ADDRESS: {
            statusCode: 400,
            customMessage: 'Address Already Exist',
            type: 'DUPLICATE_ADDRESS'
        },
        DUPLICATE_LABEL: {
            statusCode: 400,
            customMessage: 'Label Already Used',
            type: 'DUPLICATE_LABEL'
        },
        UNIQUE_CODE_LIMIT_REACHED: {
            statusCode: 400,
            customMessage: 'Cannot Generate Unique Code, All combinations are used',
            type: 'UNIQUE_CODE_LIMIT_REACHED'
        },
        INVALID_REFERRAL_CODE: {
            statusCode: 400,
            customMessage: 'Invalid Referral Code',
            type: 'INVALID_REFERRAL_CODE'
        },
        FACEBOOK_ID_PASSWORD_ERROR: {
            statusCode: 400,
            customMessage: 'Only one field should be filled at a time, either facebookId or password',
            type: 'FACEBOOK_ID_PASSWORD_ERROR'
        },
        INVALID_EMAIL: {
            statusCode: 400,
            customMessage: 'Invalid Email Address',
            type: 'INVALID_EMAIL'
        },
        PASSWORD_REQUIRED: {
            statusCode: 400,
            customMessage: 'Password is required',
            type: 'PASSWORD_REQUIRED'
        },
        INVALID_COUNTRY_CODE: {
            statusCode: 400,
            customMessage: 'Invalid Country Code, Should be in the format +52',
            type: 'INVALID_COUNTRY_CODE'
        },
        INVALID_PHONE_NO_FORMAT: {
            statusCode: 400,
            customMessage: 'Phone no. cannot start with 0',
            type: 'INVALID_PHONE_NO_FORMAT'
        },
        COUNTRY_CODE_MISSING: {
            statusCode: 400,
            customMessage: 'You forgot to enter the country code',
            type: 'COUNTRY_CODE_MISSING'
        },
        INVALID_PHONE_NO: {
            statusCode: 400,
            customMessage: 'Phone No. & Country Code does not match to which the OTP was sent',
            type: 'INVALID_PHONE_NO'
        },
        PHONE_NO_MISSING: {
            statusCode: 400,
            customMessage: 'You forgot to enter the phone no.',
            type: 'PHONE_NO_MISSING'
        },
        NOTHING_TO_UPDATE: {
            statusCode: 400,
            customMessage: 'Nothing to update',
            type: 'NOTHING_TO_UPDATE'
        },
        NOT_FOUND: {
            statusCode: 400,
            customMessage: 'User Not Found',
            type: 'NOT_FOUND'
        },
        INVALID_RESET_PASSWORD_TOKEN: {
            statusCode: 400,
            customMessage: 'Invalid Reset Password Token',
            type: 'INVALID_RESET_PASSWORD_TOKEN'
        },
        INCORRECT_PASSWORD: {
            statusCode: 401,
            customMessage: 'Incorrect Password',
            type: 'INCORRECT_PASSWORD'
        },
        EMPTY_VALUE: {
            statusCode: 400,
            customMessage: 'Empty String Not Allowed',
            type: 'EMPTY_VALUE'
        },
        PHONE_NOT_MATCH: {
            statusCode: 400,
            customMessage: "Phone No. Doesn't Match",
            type: 'PHONE_NOT_MATCH'
        },
        SAME_PASSWORD: {
            statusCode: 400,
            customMessage: 'Old password and new password are same',
            type: 'SAME_PASSWORD'
        },
        ACTIVE_PREVIOUS_SESSIONS: {
            statusCode: 400,
            customMessage: 'You already have previous active sessions, confirm for flush',
            type: 'ACTIVE_PREVIOUS_SESSIONS'
        },
       COMPANY_NAME: {
            statusCode: 400,
            customMessage: 'Same Company name. Choose different name.',
            type: 'COMPANY_NAME'
        },
        SESSION_TYPE: {
            statusCode: 400,
            customMessage: 'Please enter valid session types.',
            type: 'SESSION_TYPE'
        },
        SCHEDULE_DATE: {
            statusCode: 400,
            customMessage: 'Service available on future dates.',
            type: 'SCHEDULE_DATE'
        },
        EMAIL_ALREADY_EXIST: {
            statusCode: 400,
            customMessage: 'Email Address Already Exists',
            type: 'EMAIL_ALREADY_EXIST'
        },
        TRUCK_NAME:{
            statusCode: 400,
            customMessage: 'Truck Name already exists.',
            type: 'TRUCK_NAME'

        },
        TRUCK_CONFIG:{
            statusCode: 400,
            customMessage: 'Add truck Configuration correctly.',
            type: 'TRUCK_CONFIG'
        },
        ERROR_PROFILE_PIC_UPLOAD: {
            statusCode: 400,
            customMessage: 'Profile pic is not a valid file',
            type: 'ERROR_PROFILE_PIC_UPLOAD'
        },
        PHONE_ALREADY_EXIST: {
            statusCode: 400,
            customMessage: 'Phone No. Already Exists',
            type: 'PHONE_ALREADY_EXIST'
        },
        EMAIL_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Email Not Found',
            type: 'EMAIL_NOT_FOUND'
        },
        REFREE_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Refree not found',
            type: 'REFREE_NOT_FOUND'
        },
        FACEBOOK_ID_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Facebook Id Not Found',
            type: 'FACEBOOK_ID_NOT_FOUND'
        },
        PHONE_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Phone No. Not Found',
            type: 'PHONE_NOT_FOUND'
        },
        DOCTOR_NOT_AVAILABLE: {
            statusCode: 400,
            customMessage: 'Doctor Not Available',
            type: 'DOCTOR_NOT_AVAILABLE'
        },
        DIAGNOSTICIAN_NOT_AVAILABLE: {
            statusCode: 400,
            customMessage: 'Diagnostician Not Available',
            type: 'DIAGNOSTICIAN_NOT_AVAILABLE'
        },
        INCORRECT_OLD_PASS: {
            statusCode: 400,
            customMessage: 'Incorrect Old Password',
            type: 'INCORRECT_OLD_PASS'
        },
        UNAUTHORIZED: {
            statusCode: 401,
            customMessage: 'You are not authorized to perform this action',
            type: 'UNAUTHORIZED'
        },
        PHONENUMBER_NOT_REGISTERED: {
            statusCode: 401,
            customMessage: 'Mobile Number is not registered with us',
            type: 'PHONENUMBER_NOT_REGISTERED'
        },
        NOT_CANCEL: {
            statusCode: 400,
            customMessage: 'You cannot cancel this booking.',
            type: 'NOT_CANCEL'
        },
        NOT_GIFT:{
            statusCode: 400,
            customMessage: 'You cannot gift to yourself.',
            type: 'NOT_GIFT'
        }

    },
    SUCCESS: {
        PRICE_EDIT: {
            statusCode: 200,
            customMessage: 'Price edit successfully',
            type: 'PRICE_EDIT'
        },
        USER_UNBLOCKED: {
            statusCode: 200,
            customMessage: 'User unblocked successfully',
            type: 'USER_UNBLOCKED'
        },
        USER_BLOCKED: {
            statusCode: 200,
            customMessage: 'User blocked successfully',
            type: 'USER_BLOCKED'
        },
        REVIEW_SAVED: {
            statusCode: 200,
            customMessage: 'Feedback saved successfully.',
            type: 'REVIEW_SAVED'
        },
        ADDRESS_DELETE: {
            statusCode: 200,
            customMessage: 'Address deleted successfully.',
            type: 'ADDRESS_DELETE'
        },
        JUMIO_VERIFY: {
            statusCode: 200,
            customMessage: 'The person is jumio verified.',
            type: 'JUMIO_VERIFY'
        },
        SUBSCRIPTION_BOUGHT: {
            statusCode: 200,
            customMessage: 'Subscription Bought.',
            type: 'SUBSCRIPTION_BOUGHT'
        },
        ADDRESS_CREATE: {
            statusCode: 200,
            customMessage: 'Address added successfully.',
            type: 'ADDRESS_CREATE'
        },
        ADDRESS_EDIT: {
            statusCode: 200,
            customMessage: 'Address updated successfully.',
            type: 'ADDRESS_EDIT'
        },
        UPLOAD_IMAGE: {
            statusCode: 200,
            customMessage: 'Image uploaded successfully.',
            type: 'UPLOAD_IMAGE'
        },
        CARD_DELETED: {
            statusCode: 200,
            customMessage: 'Card deleted successfully',
            type: 'CARD_DELETED'
        },
        APPOINTMENT_CANCELLED: {
            statusCode: 200,
            customMessage: 'Appointment cancelled.',
            type: 'APPOINTMENT_CANCELLED'
        },
        DOCTOR_REASSIGNED: {
            statusCode: 200,
            customMessage: 'Doctor reassigned.',
            type: 'DOCTOR_REASSIGNED'
        },
        VERIFY_COMPLETE: {
            statusCode: 200,
            customMessage: 'One Time Password verification is completed.',
            type: 'VERIFY_SENT'
        },
        PASSWORD_RESET: {
            statusCode: 200,
            customMessage: 'Password Reset Successfully',
            type: 'PASSWORD_RESET'
        },
        FORGOT_PASSWORD: {
            statusCode: 200,
            customMessage: 'A mail has been sent to your email for password reset.',
            type: 'FORGOT_PASSWORD'
        },
        PASSWORD_CHANGE: {
            statusCode: 200,
            customMessage: 'Password Change Successfully',
            type: 'PASSWORD_CHANGE'
        },
        VERIFY_SENT: {
            statusCode: 200,
            customMessage: 'One Time Password has been sent to your mobile number.',
            type: 'VERIFY_SENT'
        },
        VERIFY_RESENT: {
            statusCode: 200,
            customMessage: 'One Time Password has been resent to your mobile number.',
            type: 'VERIFY_RESENT'
        },
        CREATED: {
            statusCode: 201,
            customMessage: 'Created Successfully',
            type: 'CREATED'
        },
        STARTED: {
            statusCode: 200,
            customMessage: 'Service Started Successfully',
            type: 'STARTED'
        },
        COMPLETED: {
            statusCode: 200,
            customMessage: 'Service Completed Successfully',
            type: 'COMPLETED'
        },
        COLLECTED: {
            statusCode: 200,
            customMessage: 'Sample collected',
            type: 'COLLECTED'
        },
        FETCHED: {
            statusCode: 200,
            customMessage: 'Fetched Successfully',
            type: 'FETCHED'
        },
        DEFAULT: {
            statusCode: 200,
            customMessage: 'Success',
            type: 'DEFAULT'
        },
        UPDATED: {
            statusCode: 200,
            customMessage: 'Updated Successfully',
            type: 'UPDATED'
        },
        LOGOUT: {
            statusCode: 200,
            customMessage: 'Logged Out Successfully',
            type: 'LOGOUT'
        },
        DELETED: {
            statusCode: 200,
            customMessage: 'Deleted Successfully',
            type: 'DELETED'
        },
        ADDRESS_NOT_FOUND: {
            statusCode: 200,
            customMessage: 'Address not found.',
            type: 'ADDRESS_NOT_FOUND'
        },
        PAYMENT_SUCCESS: {
            statusCode: 200,
            customMessage: 'Payment Successfully',
            type: 'PAYMENT_SUCCESS'
        },
        FEEDBACK_SUCCESS: {
            statusCode: 200,
            customMessage: 'Feedback Successfully',
            type: 'FEEDBACK_SUCCESS'
        }
    }
};


var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];
var TIME_UNITS = {
    MONTHS: 'months',
    HOURS: 'hours',
    MINUTES: 'minutes',
    SECONDS: 'seconds',
    WEEKS: 'weeks',
    DAYS: 'days'
};

var SCREEN_TO_SHOW = {
    HOMEPAGE: 'HOMEPAGE',
    TRACKING: 'TRACKING',
    FEEDBACK: 'FEEDBACK'
};

var notificationMessages = {
    USER: {
        JUMIO_NOT_VERIFIED:"Please verify yourself again. You are not verified by Jumio.",
        JUMIO_APPROVED:'You are successfully verified by Jumio.',
        SERVICE_STARTED: 'Therapist started your booking',
        APPOINTMENT_ACCEPT: "Your Job request has been claimed by ",
        APPOINTMENT_COMPLETE: "has completed your service.",
        APPOINTMENT_CANCEL: "PloMo cancelled your Booking.",
        APPOINTMENT_EXPIRED: "Sorry, there isn't anyone to service your request at this time.",
        PRICE_EDIT: "Booking price updated by admin.",
        FAIL_PRICE_EDIT: "Price editing failed due to insufficient amount in user's payment card",
        SERVICE_COMPLETE: "Therapist has completed your service.",
    },
    THERAPIST: {
        SERVICE_COMPLETE: "You have completed your service.",
        SPECIAL_REQUEST: "You've been requested",
        APPOINTMENT_REQUEST: "You have a new booking request!",
        APPROVED_AGENT: "You are now a PloMo service provider.",
        REJECT_AGENT: "You are rejected as a PloMo service provider.",
        PRICE_EDIT: "Booking price updated by admin.",
        FAIL_PRICE_EDIT: "Price editing failed due to insufficient amount in user's payment card"
    },
    bookingNotify:"Dear Customer,Your booking Details.",
    giftCard:"Dear Customer, You receive a gift of {{amount}}$ from your friend.",
    bookingNotConfirmedMessage:"Dear Customer,\n Unfortunately we were unable to fullfill your request for a massage at this time due to higher demand than normal. Your credit card will not be charged. Please try again soon.",
    bookingMessage:"Your appointment has been booked.Check your email for confirmation and reservation details.Enjoy your massage!",
    registrationMsg:"Thank you for registering with Massago.Check your email for information about setting up an interview and providing additional information to complete your application.",
    verificationCodeMsg: 'Your 4 digit verification code for On Demand Trucking is {{four_digit_verification_code}}',
    blockedMsg: 'You are blocked by admin',
    sendMessageToAdmin:'Hi,\n On Demand Trucking added company, your password is {{password}} and userName is {{userName}}.',
    sendMessageToClient:'Hi {{userName}},\n On Demand Trucking give you admin access, your password is {{password}} and userName is {{userName}}',
    unblockedMsg: "You are unblocked by admin.",
    forgetPasswordMsg: 'Your new password for Massago is {{four_digit_verification_code}}',
    stripeAccount:"Your stripe account is successfully added with Masssago",
    registrationEmail: {
        emailMessage: "Hi {{user_name}}, <br> Thank you for registering with Massago. <br> <br>When you're ready to enjoy your first on-demand massage, just return  to the Massago app on your mobile phone and tell us the time and place. We’ll send one of our Registered Massage Therapists to your home, office or hotel room – anywhere in Toronto, in as little as an hour. <br> " +
        "<br>Enter promo code (XXXX) and you'll receive a (xx%) discount on your first massage.<br><br> We’ll apply the discount on the spot. Your payment, as always, will be made through the app. So you never need to have cash on hand, not even for the tip.<br>" +
        "<br>You can relax knowing that our massage therapists are the best in the industry. All are licensed, background checked and fully vetted. You can call on them any day of the week, 8:00 a.m. – 10:00 p.m.<br>" +
        "<br>All of our therapists are RMTs. So if massage is covered by your health insurance plan you can submit a claim using the details provided on your receipt, which will be provided to you by your therapist after your treatment.<br>" +
        "<br>If you have questions, we suggest you read our <a href ='{{FAQ}}'>FAQs.</a> If you can’t find the answers there, give us a call at 1-888-888-8888. <br>" +
        "<br> <a href='http://www.massago.ca'>www.massago.ca</a>  <a href='http://www.google.com'> Terms and Conditions</a>   <a href='http://www.massago.ca'> Privacy Policy</a> <br> " +
        "<br>Do you know a massage therapist? Why not <a href='http://www.massago.ca'> tell them about us</a>? We’re always looking for qualified RMTs to join our team.",
        emailSubject: "Welcome to Massago"
    },
    BookingConfrmedForm: {
        emailMessage: "Hi {{userName}},<br> <br>Here are the reservation details for your upcoming massage appointment: <br>" +
        "<br> Date and time: {{dateTime}} <br>" +
        "<br> Session type : {{sessionType}} <br>" +
        "<br> Massage type : {{massageType}} <br>" +
        "<br> Duration : {{duration}} <br>" +
        "<br> If this is your first Massage with us, please complete the secure online intake form prior to your therapist’s arrival which can be accessed here >> www.test.com <br>" +
        "<br> Please have a towel, 2 sheets and a pillowcase ready for your appointment.  Your therapist may arrive a few minutes early to set up. <br>" +
        "<br> If you need to cancel, you can let us know through the app. Charges may apply, so please review our <a href='http://www.google.com'> cancellation policy.</a> <br>" +
        "<br> Your credit card will be charged immediately after the service is complete.<br>" +
        "<br> If you need anything, please contact us at info@massago.ca <br>" +
        "<br> Enjoy your massage!<br>"
        ,
        emailSubject: "Your massage has been booked"
    },
    bookingNotConfirm: {
        emailMessage: "Hi {{name}},<br> <br> Thank you for choosing Massago. Unfortunately we were unable to fulfill your request for an appointment at the time requested due to higher demand than normal. Why not return to the app and choose a different date or time.<br>" +
        "<br> Please be assured that your credit card will not be charged. We hope you'll try again soon.<br>" +
        "If you need to contact us please email info@massago.ca",
        emailSubject: "We were unable to book your massage"
    },
    bookingComplete: {
        emailMessage: "Hi {{name}},<br> <br> We hope you enjoyed your on-demand massage with Massago!<br>" +
        "<br>If you’re submitting a claim to your insurance provider they will require the signed receipt provided to you by your therapist.Here is your summary for the service provided:<br>" +
        "<br> Date and time: {{dateTime}} <br>" +
        "<br> Session type : {{sessionType}} <br>" +
        "<br> Massage type : {{massageType}} <br>" +
        "<br> Duration : {{duration}} <br>" +
        "<br> Price:{{price}}<br>" +
        "<br> Gratuity:{{gratuity}}<br>" +
        "<br> Tax:{{tax}}<br>" +
        "<br>Total:{{total}}<br>" +
        "<br>Promo:{{promo}}<br>" +
        "<br>GiftCard:{{giftCard}}<br>",
        emailSubject: "Receipt for your recent massage"
    },
    forgotPassword: {
        emailMessage: "Dear {{user_name}}, <br><br>  Your reset password token is <strong>{{password_reset_token}}</strong> , <a href='{{password_reset_link}}'> Click Here </a> To Reset Your Password",
        emailSubject: "Password Reset Notification For Seed Project"
    }
};

var languageSpecificMessages = {
    verificationCodeMsg: {
        EN: 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}',
        ES_MX: 'Your 4 digit verification code for Seed Project is {{four_digit_verification_code}}'
    }
};

var PROMOCODE_LENGTH = 4;

var APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    SCREEN_TO_SHOW: SCREEN_TO_SHOW,
    STATUS_MSG: STATUS_MSG,
    notificationMessages: notificationMessages,
    languageSpecificMessages: languageSpecificMessages,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,
    TIME_UNITS: TIME_UNITS,
    PROMOCODE_LENGTH: PROMOCODE_LENGTH
};

module.exports = APP_CONSTANTS;
