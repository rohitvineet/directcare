'use strict';

const local = "mongodb://localhost/directcare";
//const local = process.env.MONGO_URI || "mongodb://"+process.env.MONGO_USER+":"+process.env.MONGO_PASS+"@"+'localhost'+"/"+process.env.MONGO_DBNAME_DEV;
//const local = process.env.MONGO_URI || "mongodb://"+process.env.MONGO_USER+":"+process.env.MONGO_PASS+"@"+'localhost'+"/"+process.env.MONGO_DBNAME_TEST;

const mongodbURI = {
    local: local
};

module.exports = {
    mongodbURI: mongodbURI
};
