'use strict';

const SORT_ALGO = {
    QUICK: "QUICK",
    MERGE: "MERGE"
};

const GEO_JSON_TYPES = {
    "Point": "Point"
};

const STATUS_CODE = {
    OK: 200,
    CREATED: 201,
    DO_NOT_PROCESS: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    PAYMENT_FAILURE: 402,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    ALREADY_EXISTS_CONFLICT: 409,
    UNSUPPORTED_MEDIA_TYPE: 415,
    SERVER_ERROR: 500
};

const PAYMENT_STATUS = {
    PENDING : 'Pending',
    COMPLETED : "Completed"
};

const CREDITS_DEDUCTED_ON_CANCELLATION = {
    AMOUNT : 20
};

const PAYMENT_METHOD = {
    CARD : "Card",
    PROMO : "PromoCode",
    BOTH: "Card-cum-PromoCode",
    SUBSCRIPTION: "Subscription"
};

const APPLICATION_STATUS = {
    COMPLETED : "Completed",
    CONFIRMED : "Confirmed",
    CANCELLED : "Cancelled",
    STARTED : "Started",
    SAMPLE_COLLECTED : "Sample Collected"
};

const CANCEL_REASON = {
    DOCTOR : "Cancelled by Doctor",
    PATIENT : "Cancelled by Patient",
    NOT_CANCELLED : "Not Cancelled"
};

const MEAL = {
    WITHOUT : "Without",
    AFTER : "After"
};

const REGEX = {
    EMAIL : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
};

module.exports = {
    SORT_ALGO: SORT_ALGO,
    STATUS_CODE: STATUS_CODE,
    GEO_JSON_TYPES: GEO_JSON_TYPES,
    PAYMENT_STATUS:PAYMENT_STATUS,
    PAYMENT_METHOD: PAYMENT_METHOD,
    CANCEL_REASON: CANCEL_REASON,
    APPLICATION_STATUS: APPLICATION_STATUS,
    MEAL: MEAL,
    REGEX : REGEX,
    CREDITS_DEDUCTED_ON_CANCELLATION: CREDITS_DEDUCTED_ON_CANCELLATION
};
