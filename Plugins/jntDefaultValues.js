
var jntDefaultValues = {
    maxUseCount : 50,
    promoDescriptionTemplate : "This code will give [Discount_percent_or_amount_statement] on your next service"
};

module.exports = jntDefaultValues;