
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api', 'doctor'];

// Register doctor
var register =
{
    method: 'POST',
    path: '/api/doctor/register',
    handler: function (request, reply) {
        var payloadData = request.payload;
        if (request.payload.email && (UniversalFunctions.validateString(request.payload.email, /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/) == null)) {
            reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_DOMAIN_ERROR));
        }
        else {
            Controller.doctorBaseController.registerDoctor(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        }

    },
    config: {
        description: 'Register a new doctor || WARNING : Will not work from documentation, use postman instead',
        tags: TAGS,
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).max(10).required(),
                email: Joi.string().email().required(),
                password: Joi.string().required().min(4),
                geoFencingId: Joi.string().required(),
                domainOfServiceKey: Joi.array().items(Joi.number()).required(),
                homeAddLatitude: Joi.number().required().min(-90).max(90),
                homeAddLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string(),
                profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .allow('')
                        .description('image file'),

                MDCN_certificate: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('pdf file')
                    .required(),

                College_certificate: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('pdf file')
                    .required()
                },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


//Login doctor

var login = {
    method: 'POST',
    path: '/api/doctor/login',
    config: {
        description: 'Log in doctor',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.doctorBaseController.loginDoctor(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGGEDIN, success));
                }
            });
        },
        validate: {
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required(),
                password: Joi.string().trim().required()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

//logout doctor

var logout = {
    method: 'PUT',
    path: '/api/doctor/logout',
    config: {
        description: 'Logout doctor',
        auth: 'UserAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DOCTOR) {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            } else {
                Controller.doctorBaseController.logoutDoctor(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT));
                    }
                });

            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }

    }
};


var accessTokenLogin =
{
    /* *****************access token login****************** */
    method: 'POST',
    path: '/api/doctor/accessTokenLogin',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.doctorBaseController.accessTokenLogin(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Doctor access token login',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var verifyAccount =
{
    /* *****************verifyEmail****************** */
    method: 'POST',
    path: '/api/doctor/verifyAccount',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.doctorBaseController.verifyAccount(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Verify account of doctor',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var changePassword =
{
    /* *****************change Password****************** */
    method: 'PUT',
    path: '/api/doctor/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.doctorBaseController.changePassword(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_CHANGE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Doctor change password',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                oldPassword: Joi.string().required().min(4),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var resetPassword =
{
    /* *****************reset Password****************** */
    method: 'PUT',
    path: '/api/doctor/resetPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.doctorBaseController.resetPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_RESET));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Doctor reset password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                hash: Joi.string().required(),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var forgotPassword =
{
    /* *****************forgot Password****************** */
    method: 'PUT',
    path: '/api/doctor/forgotPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.doctorBaseController.forgotPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FORGOT_PASSWORD));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Forgot password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getProfile =
{
    /* *****************get profile****************** */
    method: 'GET',
    path: '/api/doctor/getProfile',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.doctorBaseController.getProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Get profile of doctor',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var editProfile =
{
    /* ***************** editProfile ****************** */
    method: 'PUT',
    path: '/api/doctor/editProfile',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.doctorBaseController.editProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Doctor edit profile',
        tags: TAGS,
        auth: 'UserAuth',
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).max(10).required(),
                geoFencingId: Joi.string().required(),
                domainOfServiceKey: Joi.array().items(Joi.number()).required(),
                homeAddLatitude: Joi.number().required().min(-90).max(90),
                homeAddLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .allow('')
                    .description('image file')
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var doctorRoute =
    [
        register, login, logout, accessTokenLogin, verifyAccount, changePassword, resetPassword, getProfile, editProfile, forgotPassword
    ]
module.exports = doctorRoute;
