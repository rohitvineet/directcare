'use strict';

module.exports = [
    require('./Admin/baseRoutes'),
    require('./Admin/bandsRoutes'),
    require('./Patient/baseRoutes'),
    require('./Doctor/baseRoutes'),
    require('./Diagnostic/baseRoutes'),
    require('./Patient/appointmentRoutes'),
    require('./Patient/paymentRoutes'),
    require('./Doctor/appointmentRoutes'),
    require('./Diagnostic/appointmentRoutes'),
    require('./Patient/videoConsultationRoutes'),
    require('./Admin/appointmentRoutes')
];
