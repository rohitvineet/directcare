

'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api', 'patient'];

// Setup video consultation
var publishVideoConsultation = {
    method: 'POST',
    path: '/api/patient/appointment/publishVideoConsultation',
    config: {
        description: 'Publish video consultation',
        tags: TAGS,
        auth: 'UserAuth',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.videoConsultationController.publishVideoConsultation(userData, data, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(201);
                }
            });
        },
        validate: {
            payload:{
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var PublishRoute =
    [
        publishVideoConsultation
    ]
module.exports = PublishRoute;
