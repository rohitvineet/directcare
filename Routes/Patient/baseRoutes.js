
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api' , 'patient'];
// Register patient
var register =
{
    method: 'POST',
    path: '/api/patient/register',
    config: {
    description: 'Register a new patient || WARNING : Will not work from documentation, use postman instead',
        tags: TAGS,
         payload: {
         maxBytes: 20715200,
         output: 'stream',
         parse: true,
         allow: 'multipart/form-data'
         },
        validate: {
        payload: {
            firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                email: Joi.string().email().required(),
                password: Joi.string().required().min(4),
                 profilePic: Joi.any()
                 .meta({swaggerType: 'file'})
                 .optional()
                 .allow('')
                 .description('image file')
    
                 },
        failAction: UniversalFunctions.failActionFunction
    },
    plugins: {
        'hapi-swagger': {
            payloadType: 'form',
            responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
    }
},
    handler: function (request, reply) {
        var payloadData = request.payload;
        if (request.payload.email && (UniversalFunctions.validateString(request.payload.email, /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/) == null)) {
            reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_DOMAIN_ERROR));
        }
        else {
            Controller.patientBaseController.registerPatient(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        }

    }
};


//Login admin

var login = {
    method: 'POST',
    path: '/api/patient/login',
    config: {
        description: 'Log in patient',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.patientBaseController.loginPatient(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGGEDIN, success));
                }
            });
        },
        validate: {
            payload: {
                email: Joi.string().trim().required(),
                password: Joi.string().trim().required()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


//logout patient

var logout = {
    method: 'PUT',
    path: '/api/patient/logout',
    config: {
        description: 'Logout patient',
        auth: 'UserAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT) {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            } else {
                Controller.patientBaseController.logoutPatient(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT));
                    }
                });

            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }

    }
};


var accessTokenLogin =
{
    /* *****************access token login****************** */
    method: 'POST',
    path: '/api/patient/accessTokenLogin',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.patientBaseController.accessTokenLogin(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Patient access token login',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var verifyAccount =
{
    /* *****************verifyEmail****************** */
    method: 'POST',
    path: '/api/patient/verifyAccount',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.patientBaseController.verifyAccount(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Verify account of patient',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var changePassword =
{
    /* *****************change Password****************** */
    method: 'PUT',
    path: '/api/patient/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.patientBaseController.changePassword(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_CHANGE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Patient change password',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                oldPassword: Joi.string().required().min(4),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};
var resetPassword =
{
    /* *****************reset Password****************** */
    method: 'PUT',
    path: '/api/patient/resetPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.patientBaseController.resetPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_RESET));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Patient reset password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                hash: Joi.string().required(),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var forgotPassword =
{
    /* *****************forgot Password****************** */
    method: 'PUT',
    path: '/api/patient/forgotPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.patientBaseController.forgetPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FORGOT_PASSWORD));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Forgot password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getProfile =
{
    /* *****************get profile****************** */
    method: 'GET',
    path: '/api/patient/getProfile',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.patientBaseController.getProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Get profile of patient',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var editProfile =
{
    /* ***************** editProfile ****************** */
    method: 'PUT',
    path: '/api/patient/editProfile',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientBaseController.editProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data==null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Patient update profile',
        tags: TAGS,
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                firstName: Joi.string().required(),
                lastName: Joi.string(),
                countryCode: Joi.string().required(),
                mobileNo: Joi.number().required(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .allow('')
                    .description('image file'),
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var addAddress =
{
    /* ***************** addAddress ****************** */
    method: 'PUT',
    path: '/api/patient/addAddress',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientBaseController.addAddress(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data==null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.ADDRESS_CREATE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Add address',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                label: Joi.string(),
                addressLatitude: Joi.number().required().min(-90).max(90),
                addressLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var delAddress =
{
    /* ***************** delete Address ****************** */
    method: 'DELETE',
    path: '/api/patient/deleteAddress',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientBaseController.delAddress(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data==null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.ADDRESS_DELETE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Delete address',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                id: Joi.string().required(),
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getAddress =
{
    /* ***************** get Address ****************** */
    method: 'GET',
    path: '/api/patient/getAddress',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientBaseController.getAddress(userData, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Get All address',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var updateAddress =
{
    /* ***************** get Address ****************** */
    method: 'PUT',
    path: '/api/patient/updateAddress',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientBaseController.updateAddress(userData, request.payload, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Update address',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                id: Joi.string().required(),
                label: Joi.string(),
                addressLatitude: Joi.number().required().min(-90).max(90),
                addressLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }

    }
};

var patientRoute =
    [
        register, login, logout, accessTokenLogin, verifyAccount, changePassword, resetPassword, forgotPassword, getProfile, editProfile, addAddress, delAddress, getAddress, updateAddress
    ]
module.exports = patientRoute;
