
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api', 'patient'];

// get Payment Card
var getPaymentCardDetails = {
    method: 'POST',
    path: '/api/patient/getPaymentCardDetails',
    config: {
        description: 'Get payment card details',
        tags: TAGS,
        auth: 'UserAuth',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.patientPaymentController.getPaymentCardDetails(data, userData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(201);
                }
            });
        },
        validate: {
            payload: {
                cardToken: Joi.string().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// get all payment Card
var getAllCards = {
    method: 'GET',
    path: '/api/patient/getAllCard',
    config: {
        description: 'Get all payment card',
        tags: TAGS,
        auth: 'UserAuth',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.patientPaymentController.getAllCards(userData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(201);
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

//add Payment Card

var addPaymentCard =
{
    /* ***************** add Payment Card ****************** */
    method: 'PUT',
    path: '/api/patient/addPaymentCard',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientPaymentController.addPaymentCard(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CARD));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Add Payment Card',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                type: Joi.string().required(),
                number: Joi.number().required(),
                expire_month: Joi.number().required(),
                expire_year: Joi.number().required(),
                cvv2: Joi.number().required(),
                first_name: Joi.string().required(),
                last_name: Joi.string(),
                payer_id: Joi.string().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// delete Payment Card
var deleteCard = {
    method: 'DELETE',
    path: '/api/patient/deleteCard',
    config: {
        description: 'Delete payment card',
        tags: TAGS,
        auth: 'UserAuth',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.patientPaymentController.deleteCard(data, userData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.CARD_DELETED, success)).code(201);
                }
            });
        },
        validate: {
            payload: {
                cardToken: Joi.string().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


//set Payment Card

var setCardDefault =
{
    /* ***************** set Payment Card as default****************** */
    method: 'PUT',
    path: '/api/patient/setCardDefault',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientPaymentController.setCardDefault(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Set Payment Card as default',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                cardToken: Joi.string().required(),
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

//make Payment Card

var makePayment =
{
    /* ***************** make Payment****************** */
    method: 'PUT',
    path: '/api/patient/makePayment',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.patientPaymentController.makePayment(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PAYMENT_SUCCESS));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Make payment',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                cardToken: Joi.string().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var PaymentRoute =
    [
        getPaymentCardDetails, addPaymentCard, deleteCard, getAllCards, setCardDefault, makePayment
    ]
module.exports = PaymentRoute;
