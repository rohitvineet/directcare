
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var Plugins = require('../../Plugins');
//var jwtDecode = require('jwt-decode');

// Add create booking
var createBooking = {
    method: 'POST',
    path: '/api/patient/appointment/bookingCreation',
    config: {
        description: "Create new appointment booking",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.bookAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                domainOfServiceKey: Joi.number(),
                typeOfServiceKey: Joi.number(),
                diagnosticTypeOfServiceKey: Joi.number(),
                addLatitude: Joi.number().required(),
                addLongitude: Joi.number().required(),
                streetAddress: Joi.string().required(),
                scheduleDate: Joi.date(),
                symptoms:Joi.string().optional().allow(''),
                allergies:Joi.string().optional().allow(''),
                medication:Joi.string().optional().allow(''),
                specialInstruction:Joi.string().optional().allow(''),
                totalCost: Joi.number().required(),
                discount: Joi.number(),
                paymentCardId:Joi.string(),
                promoCode:Joi.string().optional().allow(''),
                subscriptionId: Joi.string().optional().allow(''),
                paymentMethod: Joi.string().required(),
                attachedImages: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .allow('')
                    .description('image file')
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// View one booking
var viewBooking = {
    method: 'POST',
    path: '/api/patient/appointment/view',
    config: {
        description: "View appointment",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.viewAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                appointmentId:  Joi.string().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// View A booking
var appointmentHistory = {
    method: 'GET',
    path: '/api/patient/appointment/history',
    config: {
        description: "Appointment history",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.appointmentHistory(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var submitFeedback = {
    method: 'POST',
    path: '/api/patient/appointment/submitFeedback',
    config: {
        description: "Submit Feedback",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.submitFeedback(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FEEDBACK_SUCCESS))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required(),
                feedback: Joi.string(),
                rating: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var buySubscription = {
    method: 'PUT',
    path: '/api/patient/buySubscription',
    config: {
        description: "Buy Subscription",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.buySubscription(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.SUBSCRIPTION_BOUGHT))
                }
            });
        },
        validate: {
            payload: {
                subscriptionId: Joi.string().required(),
                currentTime: Joi.date()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getBoughtSubscription = {
    method: 'GET',
    path: '/api/patient/getBoughtSubscription',
    config: {
        description: "Get Bought Subscription",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.patientAppointmentController.getBoughtSubscription(userData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED,success))
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var applyPromoCode = {
    method: 'POST',
    path: '/api/patient/applyPromoCode',
    config: {
        description: "Apply PromoCode",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.applyPromoCode(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success))
                }
            });
        },
        validate: {
            payload: {
                promoCode: Joi.string().required(),
                price: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var cancelAppointment = {
    method: 'POST',
    path: '/api/patient/appointment/cancel',
    config: {
        description: "Cancel Appointment",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.patientAppointmentController.cancelAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var AdminRoute =
    [
        createBooking, viewBooking, appointmentHistory, submitFeedback, buySubscription, getBoughtSubscription, applyPromoCode, cancelAppointment
    ]
module.exports = AdminRoute;
