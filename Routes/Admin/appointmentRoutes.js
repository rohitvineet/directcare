
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api' , 'admin'];



//cancel appointment

var cancelAppointment = {
    method: 'POST',
    path: '/api/admin/appointment/cancel',
    config: {
        description: 'Cancel Appointment',
        tags: TAGS,
        auth: 'AdminAuth',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.adminAppointmentController.cancelAppointment(userData, request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.APPOINTMENT_CANCELLED, success));
                }
            });
        },
        
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                appointmentId: Joi.string().required(),
            }
        },

        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

//reassignment

var reassignment = {
    method: 'POST',
    path: '/api/admin/appointment/reassignment',
    config: {
        description: 'Reassignment',
        tags: TAGS,
        auth: 'AdminAuth',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.adminAppointmentController.reassignment(userData, request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DOCTOR_REASSIGNED));
                }
            });
        },

        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                doctorId: Joi.string().required(),
                previousAppointmentId: Joi.string().required(),
            }
        },

        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var AdminRoute =
    [
        cancelAppointment, reassignment
    ]
module.exports = AdminRoute;