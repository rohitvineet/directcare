
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var Plugins = require('../../Plugins');
const TAGS = ['api', 'service'];

// Add domainOfService
var addDomainOfService = {
    method: 'POST',
    path: '/api/admin/add/domainOfService',
    config: {
        description: "Add domain of service",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.addDomainOfService(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                name: Joi.string().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All domainOfService
var getAllDomainOfService = {
    method: 'GET',
    path: '/api/admin/get/domainOfService',
    config: {
        description: "List all domain of service",
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllDomainOfService(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All geoFencing
var getAllGeoFencing = {
    method: 'GET',
    path: '/api/admin/get/geoFencing',
    config: {
        description: "List all geoFencing area",
        tags: TAGS,
        handler: function (request, reply) {
            Controller.adminBandsController.getAllGeoFence(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


// Get All typeOfService
var getAllTypeOfService = {
    method: 'GET',
    path: '/api/admin/get/typeOfService',
    config: {
        description: "List all type of service",
        tags: TAGS,
        handler: function (request, reply) {
            Controller.adminBandsController.getAllTypeOfService(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All Price List
var getAllPriceList = {
    method: 'GET',
    path: '/api/admin/get/pricingTable',
    config: {
        description: "List all Price List",
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllPriceList(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var addGeoFencing = {
        method: 'POST',
        path: '/api/admin/add/geoFencing',
        config: {
            description: "Add geoFencing Area",
            tags: TAGS,
            auth: 'AdminAuth',
            handler: function (request, reply) {
                var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
                Controller.adminBandsController.addGeoFence(request.payload, function (error, success) {
                    if (error) {
                        reply(UniversalFunctions.sendError(error));
                    } else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                    }
                });
            },
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                payload: {
                    locationName: Joi.string().required(),
                    coordinates: Joi.array().items(Joi.array().items(Joi.number().precision(8).required()).required()).min(4).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
};


// Get All Price List
var getAllDiagnosticTypeOfService = {
    method: 'GET',
    path: '/api/admin/get/diagnosticTypeOfService',
    config: {
        description: "List all diagnostic type of service",
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllDiagnosticTypeOfService(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All Patients
var getAllPatients = {
    method: 'GET',
    path: '/api/admin/get/patients',
    config: {
        description: "List all patients",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllPatients(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All Doctors
var getAllDoctors = {
    method: 'GET',
    path: '/api/admin/get/doctors',
    config: {
        description: "List all doctor",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllDoctors(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All Diagnostician
var getAllDiagnostic = {
    method: 'GET',
    path: '/api/admin/get/diagnostician',
    config: {
        description: "List all diagnostician",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.getAllDiagnostic(function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Add typeOfService
var addTypeOfService = {
    method: 'POST',
    path: '/api/admin/add/typeOfService',
    config: {
        description: "Add type of service",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.addTypeOfService(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                name:Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Add pricing table
var addPricingtable = {
    method: 'POST',
    path: '/api/admin/add/pricingTable',
    config: {
        description: "Add pricing table",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.addPricingTable(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                domainOfServiceId: Joi.number().required(),
                typeOfServiceId: Joi.number().required(),
                cost: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Add diagnosticTypeOfService
var addDiagnosticTypeOfService = {
    method: 'POST',
    path: '/api/admin/add/diagnosticTypeOfService',
    config: {
        description: "Add diagnostic of service",
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.addDiagnosticType(request.payload, function (error, success) {
                    if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                name: Joi.string().required(),
                subType: Joi.string(),
                cost: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var validateLocation =
{
    /* *****************Validate Location based on geofencing****************** */
    method: 'POST',
    path: '/api/admin/validateLocation',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.adminBandsController.validateLocation(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_LOCATION));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Validate location based on geofencing',
        tags: TAGS,
        validate: {
            payload: {
                longitude: Joi.number().min(-180).max(180).required(),
                latitude: Joi.number().min(-90).max(90).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var addSubscription =
{
    /* *****************Add Subscription****************** */
    method: 'POST',
    path: '/api/admin/addSubscription',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        Controller.adminBandsController.addSubscription(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Add Subscription',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            payload: {
                name: Joi.string().required(),
                numberOfServices: Joi.number().required(),
                cost: Joi.number().required(),
                duration: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getAllSubscription =
{
    /* *****************Get All Subscription****************** */
    method: 'GET',
    path: '/api/admin/getAllSubscription',
    handler: function (request, reply) {
        Controller.adminBandsController.getAllSubscription('', function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Get All Subscription',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var blockSubscription =
{
    /* *****************Block Subscription****************** */
    method: 'POST',
    path: '/api/admin/blockSubscription',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        Controller.adminBandsController.blockSubscription(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Block Subscription',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            payload: {
                subscriptionId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var blockUser =
{
    /* *****************Block User****************** */
    method: 'POST',
    path: '/api/admin/blockUser',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        Controller.adminBandsController.blockUser(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Block User',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            payload: {
                id: Joi.string().required(),
                userType: Joi.string().required().valid([
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT,
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DOCTOR,
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DIAGNOSTIC
                ]),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var setPromoCode =
{
    /* *****************Set Promo Code****************** */
    method: 'POST',
    path: '/api/admin/setPromoCode',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
        Controller.adminBandsController.setPromoCode(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Generate Promo Code',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            payload: {
                validUpto: Joi.date().required(),
                type: Joi.string().required().valid([
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.PROMO_TYPE.PERCENT,
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.PROMO_TYPE.AMOUNT]),
                value: Joi.number().required(),
                maxUsageCountLimit: Joi.number().required(),
           //     validForServiceType: Joi.array().items(Joi.string()).required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Get All Booking
var getAllBooking = {
    method: 'GET',
    path: '/api/admin/get/monitorBooking',
    config: {
        description: "List all booking",
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.adminBandsController.allBookings(userData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var AdminRoute =
    [
        addDomainOfService, addTypeOfService, getAllDomainOfService, addPricingtable, getAllTypeOfService, getAllPriceList, addDiagnosticTypeOfService, getAllDiagnosticTypeOfService, addGeoFencing, getAllGeoFencing, getAllPatients, getAllDoctors,
        getAllDiagnostic, validateLocation, addSubscription, getAllSubscription, blockSubscription, setPromoCode, blockUser, getAllBooking
    ]
module.exports = AdminRoute;
