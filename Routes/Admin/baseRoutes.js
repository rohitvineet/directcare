
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api', 'admin'];

// Register admin
var register = {
    method: 'POST',
    path: '/api/admin/register',
    config: {
        description: 'Register a new admin',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.adminBaseController.registerAdmin(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(201);
                }
            });
        },
        validate: {
            payload: {
                email:Joi.string().email().required(),
                password:Joi.string().required().min(4),
                name:Joi.string().required(),
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Add Sub admin
var addSubAdmin = {
    method: 'POST',
    path: '/api/admin/addSubAdmin',
    config: {
        description: 'Add Sub-Admin',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.adminBaseController.addSubAdmin(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, success)).code(201);
                }
            });
        },
        validate: {
            payload: {
                email:Joi.string().email().required(),
                password:Joi.string().required().min(4),
                name:Joi.string().required(),
                adminType: Joi.string().required().valid([
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.ADMIN_TYPE.SUPER_ADMIN,
                    UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.ADMIN_TYPE.SUB_ADMIN])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

//Login admin

var login = {
    method: 'POST',
    path: '/api/admin/login',
    config: {
        description: 'Log in admin',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.adminBaseController.loginAdmin(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGGEDIN, success));
                }
            });
        },
        validate: {
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required(),
                password: Joi.string().trim().required()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// logout admin


var logout = {
    method: 'PUT',
    path: '/api/admin/logout',
    config: {
        description: 'Logout admin',
        auth: 'AdminAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN) {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            } else {
                Controller.adminBaseController.logoutAdmin(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT));
                    }
                });
            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }

    }
};


var accessTokenLogin =
{
    /* *****************access token login****************** */
    method: 'POST',
    path: '/api/admin/accessTokenLogin',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.adminBaseController.accessTokenLogin(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Admin access token login',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var changePassword =
{
    /* *****************change Password****************** */
    method: 'PUT',
    path: '/api/admin/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.adminBaseController.changePassword(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_CHANGE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Admin change password',
        tags: TAGS,
        auth: 'AdminAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                oldPassword: Joi.string().required().min(4),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};
var resetPassword =
{
    /* *****************reset Password****************** */
    method: 'PUT',
    path: '/api/admin/resetPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.adminBaseController.resetPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_RESET));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Admin reset password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                hash: Joi.string().required(),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var forgotPassword =
{
    /* *****************forgot Password****************** */
    method: 'PUT',
    path: '/api/admin/forgotPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.adminBaseController.forgotPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FORGOT_PASSWORD));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Forgot password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var AdminRoute =
    [
        register, login, logout, accessTokenLogin, changePassword, resetPassword, forgotPassword, addSubAdmin
    ]
module.exports = AdminRoute;
