
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var Plugins = require('../../Plugins');

// View one booking
var viewBooking = {
    method: 'POST',
    path: '/api/diagnostician/appointment/view',
    config: {
        description: "View appointment",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.viewAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            payload: {
                appointmentId:  Joi.string().required(),
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// View booking
var appointmentHistory = {
    method: 'GET',
    path: '/api/diagnostician/appointment/history',
    config: {
        description: "Appointment history",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.appointmentHistory(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(200)
                }
            });
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


// Start booking
var startAppointment = {
    method: 'POST',
    path: '/api/diagnostician/appointment/start',
    config: {
        description: "Start appointment",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.startAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.STARTED))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// End booking
var endAppointment = {
    method: 'POST',
    path: '/api/diagnostician/appointment/end',
    config: {
        description: "End appointment",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.endAppointment(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.COMPLETED))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

// Collect Sample
var collectSample = {
    method: 'POST',
    path: '/api/diagnostician/appointment/collectSample',
    config: {
        description: "Collect Sample",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.collectSample(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.COLLECTED))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var submitFeedback = {
    method: 'POST',
    path: '/api/diagnostician/appointment/submitFeedback',
    config: {
        description: "Submit Feedback",
        auth: 'UserAuth',
        tags: ['api', 'Booking'],
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            var payloadData = request.payload;
            Controller.diagnosticAppointmentController.submitFeedback(userData, payloadData, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FEEDBACK_SUCCESS))
                }
            });
        },
        validate: {
            payload: {
                appointmentId: Joi.string().required(),
                feedback: Joi.string(),
                rating: Joi.number().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


// Setup video consultation
var subscribeVideoConsultation = {
    method: 'POST',
    path: '/api/diagnostician/appointment/subscribeVideoConsultation',
    config: {
        description: 'Subscribe video consultation',
        tags:  ['api', 'Booking'],
        auth: 'UserAuth',
        handler: function (request, reply) {
            var data = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.diagnosticAppointmentController.subscribeVideoConsultation(userData, data, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.FETCHED, success)).code(201);
                }
            });
        },
        validate: {
            payload:{
                appointmentId: Joi.string().required()
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var AdminRoute =
    [
        viewBooking, appointmentHistory, startAppointment, endAppointment, collectSample, submitFeedback, subscribeVideoConsultation
    ]
module.exports = AdminRoute;
