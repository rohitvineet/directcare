
'use strict'

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
const TAGS = ['api', 'diagnostician'];
// Register diagnostic
var register =
{
    method: 'POST',
    path: '/api/diagnostician/register',
    handler: function (request, reply) {
        var payloadData = request.payload;
        if (request.payload.email && (UniversalFunctions.validateString(request.payload.email, /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i) == null)) {
            reply(UniversalFunctions.sendError(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_DOMAIN_ERROR));
        }
        else {
            Controller.diagnosticBaseController.registerDiagnostic(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        }

    },
    config: {
        description: 'Register a new diagnostic || WARNING : Will not work from documentation, use postman instead',
        tags: TAGS,
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                email: Joi.string().email().required(),
                password: Joi.string().required().min(4),
                homeAddLatitude: Joi.number().required().min(-90).max(90),
                homeAddLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string(),
                geoFencingId: Joi.string().required(),
                diagnosticTypeOfServiceKey: Joi.array().items(Joi.number()).required(),
                companyName: Joi.string(),
                profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .allow('')
                        .description('image file'),

                Diagnostic_lab_registry: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('pdf file')
                    .required(),

                Identity_certificate: Joi.any()
                    .meta({swaggerType: 'file'})
                    .description('pdf file')
                    .required()
            },
            failAction: UniversalFunctions.failActionFunction
        },


        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var editProfile =
{
    /* ***************** editProfile ****************** */
    method: 'PUT',
    path: '/api/doctor/editProfile',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.doctorBaseController.editProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED));
            }
            else {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
        });
    },
    config: {
        description: 'Doctor edit profile',
        tags: TAGS,
        auth: 'UserAuth',
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).max(10).required(),
                geoFencingId: Joi.string().required(),
                domainOfServiceKey: Joi.array().items(Joi.number()).required(),
                homeAddLatitude: Joi.number().required().min(-90).max(90),
                homeAddLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .allow('')
                    .description('image file')
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


//Login diagnostic

var login = {
    method: 'POST',
    path: '/api/diagnostician/login',
    config: {
        description: 'Log in diagnostic',
        tags: TAGS,
        handler: function (request, reply) {
            Controller.diagnosticBaseController.loginDiagnostic(request.payload, function (error, success) {
                if (error) {
                    reply(UniversalFunctions.sendError(error));
                } else {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGGEDIN, success));
                }
            });
        },
        validate: {
            payload: {
                email: Joi.string().trim().required(),
                password: Joi.string().trim().required()

            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


//logout diagnostrican

var logout = {
    method: 'PUT',
    path: '/api/diagnostician/logout',
    config: {
        description: 'Logout diagnostican',
        auth: 'UserAuth',
        tags: TAGS,
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DIAGNOSTIC) {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            } else {
                Controller.diagnosticBaseController.logoutDiagnostic(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGOUT));
                    }
                });

            }
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }

    }
};

var accessTokenLogin =
{
    /* *****************access token login****************** */
    method: 'POST',
    path: '/api/diagnostician/accessTokenLogin',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.diagnosticBaseController.accessTokenLogin(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Diagnostic access token login',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var changePassword =
{
    /* *****************change Password****************** */
    method: 'PUT',
    path: '/api/diagnostician/changePassword',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.diagnosticBaseController.changePassword(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_CHANGE));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Change password of diagnostician',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                oldPassword: Joi.string().required().min(4),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var verifyAccount =
{
    /* *****************verifyEmail****************** */
    method: 'POST',
    path: '/api/diagnostician/verifyAccount',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.diagnosticBaseController.verifyAccount(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Verify account of diagnostician',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var resetPassword =
{
    /* *****************reset Password****************** */
    method: 'PUT',
    path: '/api/diagnostician/resetPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.diagnosticBaseController.resetPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.PASSWORD_RESET));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Diagnostician reset password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                hash: Joi.string().required(),
                newPassword: Joi.string().required().min(4)
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var forgotPassword =
{
    /* *****************forgot Password****************** */
    method: 'PUT',
    path: '/api/diagnostician/forgotPassword',
    handler: function (request, reply) {
        var data = request.payload;
        Controller.diagnosticBaseController.forgotPassword(data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.FORGOT_PASSWORD));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Forgot password',
        tags: TAGS,
        validate: {
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                email: Joi.string().regex(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9_.+-])+\.)+([a-zA-Z0-9_.+-]+)+$/).trim().required()
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};

var getProfile =
{
    /* *****************get profile****************** */
    method: 'GET',
    path: '/api/diagnostician/getProfile',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        var data = request.payload;
        Controller.diagnosticBaseController.getProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null && data == null) {
                return reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_ACCESSTOKEN));
            }
            if (!err) {
                return reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Get profile of diagnostician',
        tags: TAGS,
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var editProfile =
{
    /* ***************** editProfile ****************** */
    method: 'PUT',
    path: '/api/diagnostician/editProfile',
    handler: function (request, reply) {
        var data = request.payload;
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controller.diagnosticBaseController.editProfile(userData, data, function (err, data) {
            console.log('%%%%%%%%%%%%%%%', err, data)
            if (err == null) {
                return reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED));
            }
            else {
                return reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Diagnostic edit profile',
        tags: TAGS,
        auth: 'UserAuth',
        payload: {
            maxBytes: 20715200,
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction,
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                lastName:Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).optional().trim().allow(''),
                countryCode: Joi.string().max(4).required().trim(),
                mobileNo: Joi.string().regex(/^[0-9]+$/).max(10).required(),
                geoFencingId: Joi.string().required(),
                diagnosticTypeOfServiceKey: Joi.array().items(Joi.number()).required(),
                homeAddLatitude: Joi.number().required().min(-90).max(90),
                homeAddLongitude: Joi.number().required().min(-180).max(180),
                streetAddress: Joi.string(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .allow('')
                    .description('image file')
            }
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
};


var diagnosticRoute =
    [
        register, login, logout, accessTokenLogin, verifyAccount, changePassword,forgotPassword, resetPassword, getProfile, editProfile
    ]
module.exports = diagnosticRoute;
