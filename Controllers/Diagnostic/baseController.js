'use strict';

var Service = require('../../Services');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var async = require('async');
var moment = require('moment');
var TokenManager = require('../../lib/TokenManager');
var bcrypt = require('bcrypt');
var jwtDecode = require('jwt-decode');
var CodeGenerator = require('../../lib/CodeGenerator');
var UploadManager = require('../../lib/UploadManager');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var SUCCESS = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS;

var registerDiagnostic = function (payloadData, callback) {
    payloadData.email = payloadData.email.toLowerCase();
    var accessToken = null;
    var uniqueCode = null;
    var upload_file1 = null;
    var upload_file2 = null;
    var dataToSave = payloadData;
    //if (dataToSave.password) dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    var diagnosticData = null;
    var dataToUpdate = {};
    if (payloadData.hasOwnProperty("profilePic") && payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }
    async.series([
        function (cb) {
                var query = {
                    $or: [{email: payloadData.email}, {mobileNo: payloadData.mobileNo}]
                };
                Service.diagnostic.getDiagnostic(query, {}, {lean: true}, function (error, data) {
                    if (error) {
                        cb(error);
                    } else {
                        if (data && data.length > 0) {
                            if (data[0].isVerified == true) {
                                cb(ERROR.USER_ALREADY_REGISTERED)
                            }
                            else {
                                Service.diagnostic.deleteDiagnostic({_id: data[0]._id}, function (err, updatedData) {
                                    if (err) cb(err)
                                    else cb(null);
                                })
                            }
                        } else {
                            cb(null);
                        }
                    }
                });

        },
        function(cb){
            if(dataToSave.password)
            {
                const SALT_WORK_FACTOR = 10;

                bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                    if (err)
                        cb(err);
                    bcrypt.hash(dataToSave.password, salt, function (err, hash) {
                        if (err)
                            cb(err);
                        else {
                            dataToSave.password = hash;
                            cb(null);
                        }
                    });
                });
            }
        },
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            if (dataToSave.mobileNo && dataToSave.mobileNo.split('')[0] == 0) {
                cb(ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            //Insert Into DB
            dataToSave.mobileNo = payloadData.mobileNo;
            dataToSave.countryCode = payloadData.countryCode;
            dataToSave.firstName = payloadData.firstName;
            dataToSave.lastName = payloadData.lastName;
            dataToSave.companyName = payloadData.companyName;
            dataToSave.geoFencingId = payloadData.geoFencingId;
            dataToSave.diagnosticTypeOfServiceId = [];
            var i,len=payloadData.diagnosticTypeOfServiceKey.length;
            for(i=0;i<len;i++)
                dataToSave.diagnosticTypeOfServiceId.push(payloadData.diagnosticTypeOfServiceKey[i]);
            dataToSave.homeAddress = {};;
            dataToSave.homeAddress.streetAddress = payloadData.streetAddress;
            dataToSave.homeAddress.customerLocation = {}
            dataToSave.homeAddress.customerLocation.coordinates = [];
            dataToSave.homeAddress.customerLocation.coordinates[1]=payloadData.homeAddLatitude;
            dataToSave.homeAddress.customerLocation.coordinates[0]=payloadData.homeAddLongitude;
//            var offset = payloadData.timezoneOffset;
            Service.diagnostic.createDiagnostic(dataToSave, function (err, diagnosticDataFromDB) {
                console.log('hello', err, diagnosticDataFromDB)
                if (err) {
                        cb(err);
                } else {
                    diagnosticData = diagnosticDataFromDB;
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            var profilePicURL = {};
            dataToUpdate.profilePicURL = {
                original: UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.agentDefaultPicUrl,
                thumbnail: UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.agentDefaultPicUrl
            };
            var profilePicture = payloadData.profilePic;
                if (payloadData.hasOwnProperty("profilePic") && payloadData.profilePic && profilePicture.hapi.filename) {
                    UploadManager.uploadProfilePicture(profilePicture, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.diagnostician, (payloadData.mobileNo).replace('+', '0'), function (err, uploadedInfo) {
                        if (err) {
                            cb(err)
                        } else {
                            dataToUpdate.profilePicURL = {
                                original: uploadedInfo.profilePicture,
                                thumbnail: uploadedInfo.profilePictureThumb
                            }
                          //  console.log(dataToUpdate.profilePicURL);
                            cb();
                        }
                    });
            }
            else {
                //console.log('urls');
                //console.log(profilePicURL);
                cb();
            }
        },
        function (cb) {
            var file1 = payloadData.Diagnostic_lab_registry;
            if (payloadData.hasOwnProperty("Diagnostic_lab_registry") && file1 && file1.hapi.filename) {
                UploadManager.uploadFileDocuments(file1, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.diagnostician, (payloadData.mobileNo).replace('+', '0') + '_1', function (err, uploadedInfo) {
                    console.log('Uploaded Info: Diagnostic Lab Registry', err, uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        upload_file1 = uploadedInfo.filename;
                        cb();
                    }
                });
            }
            else {
                //console.log('urls');
                //console.log(uploadedDocumentURL);
                cb();
            }
        },
        function(cb){
            var file2 = payloadData.Identity_certificate;
            if (payloadData.hasOwnProperty("Identity_certificate") && file2 && file2.hapi.filename) {
                UploadManager.uploadFileDocuments(file2, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.diagnostician, (payloadData.mobileNo).replace('+', '0')+'_2', function (err, uploadedInfo) {
                    console.log('Uploaded Info: Identity_certificate', err, uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        upload_file2= uploadedInfo.filename;
                        cb();
                    }
                });
            }
            else {
                //console.log('urls');
               // console.log(uploadedDocumentURL);
                cb();
            }
        },
        function (cb) {
            if (diagnosticData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                dataToUpdate.uploadedDocumentURL = {
                    Diagnostic_lab_registry: upload_file1,
                    Identity_certificate: upload_file2
                }
                //Update User
                var criteria = {
                    _id: diagnosticData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.diagnostic.updateDiagnostic(criteria, setQuery, {new: true}, function (err, updatedData) {
                    diagnosticData = updatedData;
                    cb(err, updatedData)
                })
            } else {
                if (diagnosticData && diagnosticData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original) {
                    var criteria = {
                        _id: diagnosticData._id
                    };
                    Service.diagnostic.deleteDiagnostic(criteria, function (err, updatedData) {
                        cb(ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                } else {
                    cb();
                }
            }
        },
        function (cb) {
            //Set Access Token
            if (diagnosticData) {
                var tokenData = {
                    id: diagnosticData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DIAGNOSTIC
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        },

    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(diagnosticData)
            });
        }
    });
};

var loginDiagnostic = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var updatedUserDetails = null;
    var password = '';
    payloadData.email = payloadData.email.toLowerCase();
    var uniqueCode;
        async.series([
            function (callback) {
                var query = {
                    email: payloadData.email
                };
                Service.diagnostic.getDiagnostic(query, {password: 1, firstName: 1, lastName: 1, isVerified: 1}, {lean: true}, function (err, result) {
                    console.log("err,result", err, result)
                    if (err) {
                        callback(err);
                    }
                    else
                    {
                        if (result && result.length > 0 && result[0].isVerified==true) {
                            userFound = result && result[0] || null;
                            password = result[0].password;
                            callback(null);
                        }
                        else {
                            if(result.length > 0 && result[0].isVerified !=true)
                                callback(ERROR.EMAIL_NOT_VERIFIED);
                            else
                                callback(ERROR.INCORRECT_EMAIL);
                        }
                    }
                });
            },
            function (callback){
                bcrypt.compare(payloadData.password,password,function(err,isMatch){
                    if(err)
                        callback(err);
                    else {
                        if(isMatch) {
                            successLogin=true;
                            callback(null);
                        }
                        else {
                            callback(ERROR.INVALID_USER_PASS);
                        }
                    }
                });
            },

        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DIAGNOSTIC
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(userFound)

            });
        }
    });
};

var logoutDiagnostic = function (userData, callbackRoute) {
    async.series([
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$unset: {accessToken: 1}};
                Service.diagnostic.updateDiagnostic(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var changePassword = function (userData, data, callbackRoute) {
    var userdata = null;
    async.series([
            function(cb) {
                Service.diagnostic.getDiagnostic({_id: userData.id}, {password: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (callback){
                bcrypt.compare(data.oldPassword,userdata[0].password,function(err,isMatch){
                    if(err)
                        callback(err);
                    else {
                        if(isMatch) {
                            callback(null);
                        }
                        else {
                            callback(ERROR.INCORRECT_OLD_PASS);
                        }
                    }
                });
            },
            function(cb){
                if(data.newPassword != data.oldPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
                else
                    cb(ERROR.SAME_PASSWORD);
            },
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$set: {password: data.newPassword}};
                Service.diagnostic.updateDiagnostic(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var accessTokenLogin = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1,
                    firstName: 1,
                    lastName: 1
                };

                Service.diagnostic.getDiagnostic(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }

                })
            },
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                };

                var setQuery = {
                };

                Service.diagnostic.updateDiagnostic(criteria, setQuery, {new: true}, function (err, data) {
                    cb(err, data);
                });

            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};

var verifyAccount = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1
                };

                Service.diagnostic.getDiagnostic(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }

                })
            },
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                };

                var setQuery = {
                    isVerified: true
                };

                Service.diagnostic.updateDiagnostic(criteria, setQuery, {new: true}, function (err, data) {
                    cb(err, data);
                });

            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};


var resetPassword = function (data, callbackRoute) {
    var decodedToken = null;
    var userdata = null;
    async.series([
            function(cb){
                decodedToken = jwtDecode(data.hash);
                Service.diagnostic.getDiagnostic({email: decodedToken.email}, {resetToken: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_RESET_TOKEN);
                        else {
                            if(dataFromDB[0].resetToken != "") {
                                userdata = jwtDecode(dataFromDB[0].resetToken);
                                cb(null);
                            }
                            else
                                cb(ERROR.INCORRECT_RESET_TOKEN);
                        }
                    }
                });
            },
            function(cb){
                if(decodedToken.hash === userdata.hash)
                    cb(null);
                else
                    cb(ERROR.INCORRECT_RESET_TOKEN);
            },
            function(cb){
                if(data.newPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
            },
            function (callback) {
                var condition = {email: decodedToken.email};
                var dataToUpdate = {$set: {password: data.newPassword, accessToken: "", resetToken: ""}};
                Service.diagnostic.updateDiagnostic(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};


var forgotPassword = function (data, callback) {
    var resetToken = null;
    var userdata = null;
    async.series([
            function(cb) {
                Service.diagnostic.getDiagnostic({email: data.email}, {}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_EMAIL);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (cb) {
                var rand = Math.floor(Math.random() * 90000) + 10000;
                var resetTokenData = {
                    email: data.email,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DIAGNOSTIC,
                    hash: rand
                };
                TokenManager.resetToken(resetTokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            resetToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                });
            }],
        function (error, result) {
            if (error) {
                return callback(error);
            } else {
                return callback(null);
            }
        });
};

var getProfile = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    _id: 0
                };

                Service.diagnostic.getDiagnostic(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }

                })
            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};


var editProfile = function (tokenData, payload, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    var setQuery = {};
    var c = 0;

    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1,
                    firstName: 1,
                    lastName: 1,
                    countryCode: 1,
                    mobileNo: 1,
                    diagnosticTypeOfServiceId: 1,
                    homeAddress: 1
                };

                Service.diagnostic.getDiagnostic(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var criteria = {
                    mobileNo: payload.mobileNo,
                    _id: {$ne : tokenData._id}
                }
                Service.diagnostic.getDiagnostic(criteria, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length > 0)
                            cb(ERROR.MOBILE_NO_ALREADY_USED)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function (cb) {
                console.log(payload.profilePic)
                var profilePicture = payload.profilePic;
                if (typeof(payload.profilePic)=="object" && payload.hasOwnProperty("profilePic") && payload.profilePic && profilePicture.hapi.filename) {

                    UploadManager.uploadProfilePicture(profilePicture, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.diagnostician, (payload.mobileNo).replace('+', '0'), function (err, uploadedInfo) {

                        if (err) {
                            cb(err)
                        } else {
                            setQuery.profilePicURL = {
                                original: uploadedInfo.profilePicture,
                                thumbnail: uploadedInfo.profilePictureThumb
                            }

                            cb();
                        }
                    });
                }
                else {

                    cb();
                }
            },
            function (cb) {
                var setQuery1 = {},c1 =0;
                var criteria = {
                    _id: tokenData.id
                };


                if(payload.firstName != userdata[0].firstName) {
                    setQuery.firstName = payload.firstName;
                    c++;
                }

                if(payload.lastName != userdata[0].lastName) {
                    setQuery.lastName = payload.lastName;
                    c++;
                }

                if(payload.mobileNo != userdata[0].mobileNo ) {
                    setQuery.mobileNo = payload.mobileNo;
                    c++;
                }

                if(payload.countryCode != userdata[0].countryCode){
                    setQuery.countryCode = payload.countryCode;
                    c++;
                }

                if((payload.homeAddLongitude != userdata[0].homeAddress.customerLocation.coordinates[0]) || (payload.homeAddLatitude != userdata[0].homeAddress.customerLocation.coordinates[1])){
                    setQuery.homeAddress = {};
                    var homeAddress = {
                        isDeleted : false,
                        customerLocation:{
                            coordinates : [payload.homeAddLongitude,payload.homeAddLatitude],
                            type:"Point"
                        }
                    }
                    setQuery.homeAddress = homeAddress;
                    c++;
                }

                var i,len1=payload.diagnosticTypeOfServiceKey.length,len2=userdata[0].diagnosticTypeOfServiceId.length,flag=false;
                if(len1==len2) {
                    for(i = 0; i < len1; i++) {
                        if (userdata[0].diagnosticTypeOfServiceId[i] != payload.diagnosticTypeOfServiceKey[i])
                            flag = true;
                        break;
                    }
                }
                else
                    flag=true;

                if(flag){
                    setQuery.diagnosticTypeOfServiceId=[];
                    for(i=0;i<len1;i++) {
                        setQuery.diagnosticTypeOfServiceId.push(payload.diagnosticTypeOfServiceKey[i]);
                    }
                    c++;
                }

                console.log(setQuery)

                if(c!=0) {
                    Service.diagnostic.updateDiagnostic(criteria, {$set: setQuery}, {new: true, multi: true}, function (err, data) {
                        userdata = data;
                        cb(null);
                    });
                }
                else{
                    cb(ERROR.NOTHING_TO_UPDATE)
                }

            }
        ],
        function (err, data)
        {
            if (!err)
                callback(null, {data : userdata});
            else
                callback(err);
        }
    );
};

module.exports = {
    registerDiagnostic: registerDiagnostic,
    loginDiagnostic: loginDiagnostic,
    logoutDiagnostic: logoutDiagnostic,
    accessTokenLogin: accessTokenLogin,
    verifyAccount: verifyAccount,
    changePassword: changePassword,
    resetPassword: resetPassword,
    forgotPassword: forgotPassword,
    getProfile: getProfile,
    editProfile: editProfile
  };
