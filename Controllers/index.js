
'use strict';

module.exports = {
    adminBaseController: require('./Admin/baseController'),
    adminBandsController: require('./Admin/bandsController'),
    patientBaseController: require('./Patient/baseController'),
    doctorBaseController: require('./Doctor/baseController'),
    diagnosticBaseController: require('./Diagnostic/baseController'),
    patientAppointmentController: require('./Patient/appointmentController'),
    patientPaymentController: require('./Patient/paymentController'),
    doctorAppointmentController: require('./Doctor/appointmentController'),
    diagnosticAppointmentController: require('./Diagnostic/appointmentController'),
    videoConsultationController: require('./Patient/videoConsultationController'),
    adminAppointmentController: require('./Admin/appointmentController')
  };
