'use strict';

var Service = require('../../Services');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var async = require('async');
var moment = require('moment');
var TokenManager = require('../../lib/TokenManager');
var bcrypt = require('bcrypt');
var jwtDecode = require('jwt-decode');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var SUCCESS = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS;

var registerAdmin = function (payload, callbackRoute) {
    var returnedData = {};
    var password = '';
    var email = payload.email.toLowerCase();
    async.series([
            function (callback) {

                Service.admin.getAdmin({email: email}, {}, {}, function (err, data) {
                    if (!err) {
                        if (data.length > 0) {
                            callback(ERROR.ADMIN_EMAIL);
                        }
                        else {
                            callback(null);
                        }

                    }
                    else
                      callback(err);
                })

            },
            function (cb) {
                //verify email address
                if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
                    cb(ERROR.INVALID_EMAIL);
                } else {
                    cb();
                }
            },
            function(callback){
                const SALT_WORK_FACTOR=10;

                bcrypt.genSalt(SALT_WORK_FACTOR,function(err,salt){
                    if(err)
                      callback(err);
                    bcrypt.hash(payload.password,salt,function(err,hash){
                        if(err)
                          callback(err);
                        else
                          {
                            password=hash;
                            callback(null);
                          }
                    });
                });
            },
            function (callback) {
                var str = payload.email;
                var dataToSet = {
                    email: str.toLowerCase(),
                    password: password,
                    name: payload.name,
                    adminType: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.ADMIN_TYPE.SUPER_ADMIN,
                    mobile: payload.mobile
                };
                Service.admin.createAdmin(dataToSet, function (err, data) {
                    if (err) {
                        callback(err)

                    } else {
                        returnedData = data;
                        console.log("\n ------returned data-----" + JSON.stringify(returnedData) + 'admin register Ist Function \n');
                        callback(null);
                    }

                });
            }
        ], function (error, results) {
            if (error) {
                return callbackRoute(error);
            } else {
                var data = {
                    adminDetails: returnedData,
                };
                console.log(" ------contoller -----data" + data);
                return callbackRoute(null, data);
            }
        }
    );
};

var loginAdmin = function (payload, callbackRoute) {
    var accessToken = "";
    var adminData = {};
    var password = '';
    var email = payload.email.toLowerCase();
    async.series([
            function (callback) {
                var query = {
                    email: email
                };
                Service.admin.getAdmin(query, {password: 1, adminType: 1}, {lean: true}, function (err, result) {
                    console.log("err,result", err, result)
                    if (err) {
                        callback(err);
                    }
                    else {
                        if (result && result.length > 0) {
                            adminData = result[0];
                            password = result[0].password;
                            callback(null);
                        }
                        else {
                            callback(ERROR.INCORRECT_EMAIL);
                        }
                    }
                });
            },
            function (callback){
              bcrypt.compare(payload.password,password,function(err,isMatch){
                  if(err)
                    callback(err);
                  else {
                      if(isMatch)
                        callback(null);
                      else {
                        callback(ERROR.INVALID_USER_PASS);
                    }
                  }
               });
            },
            function (callback) {
                var timestamp = UniversalFunctions.getTimestamp();
                var tokenData = {
                    id: adminData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN
                };
                TokenManager.setToken(tokenData, function (err, outputData) {
                    if (err) {
                        callback(err);
                    } else {
                        if (outputData && outputData.accessToken) {
                            accessToken = outputData && outputData.accessToken;
                            var dataToUpdate = {
                                lastLogin: timestamp,
                                accessToken: accessToken
                            };
                            var query = {
                                email: payload.email
                            };
                            Service.admin.updateAdmin(query, dataToUpdate, {}, function (err, data) {
                                if (err) {
                                    callback(err);
                                } else {
                                    callback();
                                }

                            });
                        }
                        else {
                            callback(ERROR.IMP_ERROR)
                        }
                    }
                });

            }
        ], function (error, results) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null, {
                    accessToken: accessToken,
                    adminData: adminData
                });
            }
        }
    );
};


var logoutAdmin = function (userData, callbackRoute) {
    async.series([
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$unset: {accessToken: 1}};
                Service.admin.updateAdmin(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var accessTokenLogin = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
        function (cb) {
            var criteria = {
                _id: tokenData.id
            }

            var projection = {
                accessToken: 1,
                name: 1,
                adminType: 1
            };

            Service.admin.getAdmin(criteria, projection, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        userdata = data;
                        cb(null)
                    }
                }

            })
        },
        function (cb) {
            var criteria = {
                _id: tokenData.id
            };

            var setQuery = {
            };

            Service.admin.updateAdmin(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });

        }],
        function (err, data)
        {
        if (!err)
            callback(null, {    data : userdata    });
        else
            callback(err);

        }
    );
};


var changePassword = function (userData, data, callbackRoute) {
    var userdata = null;
    async.series([
            function(cb) {
                Service.admin.getAdmin({_id: userData.id}, {password: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (callback){
                bcrypt.compare(data.oldPassword,userdata[0].password,function(err,isMatch){
                    if(err)
                        callback(err);
                    else {
                        if(isMatch) {
                            callback(null);
                        }
                        else {
                            callback(ERROR.INCORRECT_OLD_PASS);
                        }
                    }
                });
            },
            function(cb){
                if(data.newPassword != data.oldPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
                else
                    cb(ERROR.SAME_PASSWORD);
            },
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$set: {password: data.newPassword}};
                Service.admin.updateAdmin(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var resetPassword = function (data, callbackRoute) {
    var decodedToken = null;
    var userdata = null;
    async.series([
            function(cb){
                decodedToken = jwtDecode(data.hash);
                Service.admin.getAdmin({email: decodedToken.email}, {resetToken: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_RESET_TOKEN);
                        else {
                            if(dataFromDB[0].resetToken != "") {
                                userdata = jwtDecode(dataFromDB[0].resetToken);
                                cb(null);
                            }
                            else
                                cb(ERROR.INCORRECT_RESET_TOKEN);
                        }
                    }
                });
            },
            function(cb){
                if(decodedToken.hash === userdata.hash)
                    cb(null);
                else
                    cb(ERROR.INCORRECT_RESET_TOKEN);
            },
            function(cb){
                if(data.newPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
            },
            function (callback) {
                var condition = {email: decodedToken.email};
                var dataToUpdate = {$set: {password: data.newPassword, accessToken: "", resetToken: ""}};
                Service.admin.updateAdmin(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};


var forgotPassword = function (data, callback) {
    var resetToken = null;
    var userdata = null;
    async.series([
            function(cb) {
                Service.admin.getAdmin({email: data.email}, {}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_EMAIL);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (cb) {
                var rand = Math.floor(Math.random() * 90000) + 10000;
                var resetTokenData = {
                    email: data.email,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.ADMIN,
                    hash: rand
                };
                TokenManager.resetToken(resetTokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            resetToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                });
            }],
        function (error, result) {
            if (error) {
                return callback(error);
            } else {
                return callback(null);
            }
        });
};

var addSubAdmin = function(payloadData, callback){

    var returnedData= {};

    var password = '';

    var dataToSave = {};

    async.series([
        function(cb){

            var query = {
                email : payloadData.email
            };

            var projection = {};

            var options = {
                lean : true
            };

            Service.admin.getAdmin(query,projection,options,function(err,data){
                if(err) {
                    cb(err);
                }else{
                    if(data && data.length>0){
                        cb(ERROR.ADMIN_EMAIL);
                    }else{
                        cb(null);
                    }
                }
            });
        },
        function(cb){
            const SALT_WORK_FACTOR=10;

            bcrypt.genSalt(SALT_WORK_FACTOR,function(err,salt){
                if(err)
                    cb(err);
                bcrypt.hash(payloadData.password,salt,function(err,hash){
                    if(err)
                        cb(err);
                    else
                    {
                        password=hash;
                        cb(null);
                    }
                });
            });
        },
        function(cb){

            dataToSave.email =payloadData.email;
            dataToSave.name =payloadData.name;
            dataToSave.adminType = payloadData.adminType;
            dataToSave.password = password;

            Service.admin.createAdmin(dataToSave,function(err,data){
                if(err)
                    cb(err);
                else{
                    console.log(dataToSave);
                    returnedData = data;
                    cb();
                }
            });
        }
    ],function(err,result){
        if(err)
            return callback(err);
        else{
            var data = {
                adminDetails : returnedData,
            };

            return callback(null,data);
        }
    });
};

module.exports = {
    registerAdmin: registerAdmin,
    loginAdmin: loginAdmin,
    logoutAdmin: logoutAdmin,
    accessTokenLogin: accessTokenLogin,
    changePassword: changePassword,
    forgotPassword: forgotPassword,
    resetPassword: resetPassword,
    addSubAdmin: addSubAdmin
  };
