'use strict';

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var async = require('async');
var Service = require('../../Services');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var TokenManager = require('../../lib/TokenManager');
var UploadManager = require('../../lib/UploadManager');
var moment = require('moment')
var timezoner = require('timezoner');
var distance = require('google-distance');
distance = require('google-distance-matrix');
var CONST = require('../../Config/constants');
distance.apiKey = Config.serverConfig.GOOGLE_API_KEY;
var GeoPoint = require('geopoint');
console.log("key"+distance.apiKey)

var cancelAppointment = function (tokenData, payload, callback) {
    var dataToSave = [];
    var userdata = null;
    var appointmentData = {};
    var timezoneOffset = null;
    var data1 = null;
    var flag1 = false;
    var data2 = [], time = null, data3 = null;
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.admin.getAdmin(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })

            },
            function (cb) {
                var query = {
                    _id: payload.appointmentId
                };
                Service.appointment.getAppointment(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0) {
                            cb(ERROR.INVALID_APPOINTMENT)
                        }
                        else {
                            userdata =data[0]
                            console.log(userdata)
                            cb(null)
                        }
                    }
                });
            },
            function (cb) {
                var query = {
                    _id: payload.appointmentId
                };
                Service.appointment.updateAppointment(query, {$set: {applicationStatus: Config.constants.APPLICATION_STATUS.CANCELLED}}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        cb(null)
                    }
                });
            },
            function(cb){
                var query,projection;
                if(userdata.domainOfServiceId!=0) {
                    query = {
                        domainOfServiceId: {$in: [userdata.domainOfServiceId]},
                        userBlock: false,
                        isVerified: true,
                        //isApprovedByAdmin: true
                    };
                    projection = {
                        firstName: 1,
                        lastName: 1,
                        homeAddress: 1,
                        _id: 1
                    };
                    Service.doctor.getDoctor(query, projection, {}, function (err, data) {
                        console.log(data1)
                        if (err)
                            cb(err)
                        else {
                            if (data && data.length > 0) {
                                data1 = data;
                            }
                            cb(null);
                        }
                    });
                }
                else {
                    query = {
                        diagnosticTypeOfServiceId: {$in: [userdata.diagnosticTypeOfServiceId]},
                        userBlock: false,
                        isVerified: true,
                        //                   isApprovedByAdmin: true
                    };
                    projection = {
                        firstName: 1,
                        lastName: 1,
                        homeAddress: 1,
                        _id: 1
                    };
                    Service.diagnostic.getDiagnostic(query, projection, {}, function (err, data) {
                        if (err)
                            cb(err)
                        else {
                            if (data.length > 0){
                                data1 = data;
                            }
                            cb(null);
                        }
                    });
                }
            },
            function(cb) {
                var query, projection, Job = data1, c = 0, dist, flag = false, endTime, endTime1, nextTime = null, prevTime = null, schDate = new Date(userdata.dates.scheduleDate).toISOString(), lat, long, temp = {}, prev_lat, prev_long, next_lat, next_long, prevTime1;
                endTime = moment(schDate).add(1, 'hours');
                endTime = new Date(endTime).toISOString();
                if (Job && Job.length != 0) {
                    var taskInImages = [];
                    for (var key in Job)
                    {
                        (function (key) {
                            taskInImages.push((function (key) {
                                return function (embeddedCb) {
                                    if(userdata.domainOfServiceId)
                                    {
                                        query = {
                                            doctorId: Job[key]._id,
                                            scheduleDate: userdata.scheduleDate,
                                            applicationStatus: CONST.APPLICATION_STATUS.CONFIRMED
                                        };
                                        projection = {
                                            dates: 1,
                                            doctorId: 1,
                                            address: 1,
                                            _id: 0
                                        };
                                    }
                                    else
                                    {
                                        query = {
                                            diagnosticId: Job[key]._id,
                                            scheduleDate: userdata.scheduleDate,
                                            applicationStatus: CONST.APPLICATION_STATUS.CONFIRMED
                                        };
                                        projection = {
                                            dates: 1,
                                            diagnosticId: 1,
                                            address: 1,
                                            _id: 0
                                        };
                                    }
                                    Service.appointment.getAppointment(query, projection, {sort: {"dates.scheduleDate": 1}}, function (err, data) {
                                        console.log(data)
                                        if (err)
                                            embeddedCb(err);
                                        else {
                                            if (data && data.length > 0)
                                            {
                                                var origins, destinations, prev_add, next_add, date1, secondsDiff1, secondsDiff2;
                                                date1=null, endTime1=null, prevTime = null, prevTime1 = null, nextTime = null, prev_lat = null, prev_long = null, next_lat = null, next_long = null, flag = false, dist = null, temp= {};

                                                for (var i = 0; i < data.length; i++)
                                                {
                                                    date1 = new Date(data[i].dates.scheduleDate).toISOString()
                                                    console.log("date1 :"+date1)
                                                    endTime1 = moment(date1).add(1, 'hours');
                                                    endTime1 = new Date(endTime1).toISOString();
                                                    console.log("date1 = "+date1 +"\t"+"endTime1 = "+endTime1 +"\t"+"schDate = "+schDate+"\t"+"endtime = "+endTime)
                                                    if((schDate >=date1  && schDate <= endTime1) || (endTime==date1)) {
                                                        console.log("busy")
                                                        flag = true;
                                                        break;
                                                    }
                                                    else if (date1 > schDate) {
                                                        prev_lat = dataToSave.address.customerLocation.coordinates[1]
                                                        prev_long = dataToSave.address.customerLocation.coordinates[0]
                                                        prevTime = endTime;
                                                    }
                                                    else if (schDate > endTime1) {
                                                        prev_lat = data[i].address.customerLocation.coordinates[1]
                                                        prev_long = data[i].address.customerLocation.coordinates[0]
                                                        prevTime1 = endTime1;
                                                    }
                                                    else{
                                                        next_lat = data[i].address.customerLocation.coordinates[1]
                                                        next_long = data[i].address.customerLocation.coordinates[0]
                                                        nextTime = endTime1;
                                                        break;
                                                    }
                                                }

                                                console.log("prevTime: "+prevTime)
                                                console.log("nextTime: "+nextTime)

                                                if (flag != true)
                                                {
                                                    var endTimeComputed = moment(schDate, 'YYYY-MM-DD HH:mm:ss')
                                                    var schTimeComputed = moment(date1, 'YYYY-MM-DD HH:mm:ss')

                                                    if (prevTime) {
                                                        var prevTimeComputed = moment(prevTime, 'YYYY-MM-DD HH:mm:ss')
                                                        secondsDiff1 = Math.abs(schTimeComputed.diff(prevTimeComputed, 'seconds'))
                                                    }

                                                    if (nextTime) {
                                                        var nextTimeComputed = moment(nextTime, 'YYYY-MM-DD HH:mm:ss')
                                                        secondsDiff2 = Math.abs(endTimeComputed.diff(nextTimeComputed, 'seconds'))
                                                    }

                                                    if (prevTime1) {
                                                        var prevTimeComputed1 = moment(prevTime1, 'YYYY-MM-DD HH:mm:ss')
                                                        secondsDiff1 = Math.abs(endTimeComputed.diff(prevTimeComputed1, 'seconds'))
                                                    }

                                                    console.log("prevAddress Diff: "+secondsDiff1);
                                                    console.log("nextAddress Diff: "+secondsDiff2);

                                                    if (prevTime)
                                                        prev_add = prev_lat + ',' + prev_long;

                                                    if (nextTime)
                                                        next_add = next_lat + ',' + next_long;

                                                    if (prevTime1)
                                                        prev_add = prev_lat + ',' + prev_long;

                                                    console.log("prev_add "+prev_add)
                                                    console.log("next_add "+next_add)

                                                    var curr_add = userdata.address.customerLocation.coordinates[1] + ',' + userdata.address.customerLocation.coordinates[0];
                                                    console.log("curr_add "+curr_add)

                                                    origins = [curr_add];

                                                    if (prevTime && nextTime)
                                                        destinations = [prev_add, next_add];
                                                    else if (prevTime || prevTime1)
                                                        destinations = [prev_add];
                                                    else
                                                        destinations = [next_add];

                                                    console.log("origins:  "+origins)
                                                    console.log("destinations "+destinations)

                                                    if (prevTime!=null || nextTime!=null || prevTime1!=null)
                                                    {
                                                        calculateDistanceViaGoogleDistanceMatrix(origins, destinations, function (err, data) {
                                                            console.log(">>>>>>>>",JSON.stringify(data.rows))
                                                            if(data.rows[0])
                                                                console.log("google data prev eta time = "+data.rows[0].elements[0].duration.value)
                                                            if(data.rows[1])
                                                                console.log("google data next eta time = "+data.rows[1].elements[0].duration.value)
                                                            if (!err) {
                                                                if ((prevTime || prevTime1) && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff1 && nextTime && data.rows[1] && data.rows[1].elements[0].duration.value <= secondsDiff2) {
                                                                    dist = UniversalFunctions.getDistanceBetweenPoints({lat:prev_lat, long:prev_long}, {lat: userdata.address.customerLocation.coordinates[1], long: userdata.address.customerLocation.coordinates[0]});
                                                                    temp = {
                                                                        id: Job[key]._id,
                                                                        name: Job[key].firstName+ " "+Job[key].lastName,
                                                                        long: prev_long,
                                                                        lat: prev_lat,
                                                                        distance: dist
                                                                    };
                                                                    console.log("prev_lat "+ userdata.addLatitude + "prev_long " + prev_long)
                                                                    console.log("temp = "+temp)
                                                                    embeddedCb(null,temp);
                                                                }
                                                                else if ((prevTime || prevTime1) && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff1) {
                                                                    dist = UniversalFunctions.getDistanceBetweenPoints({lat:prev_lat, long:prev_long}, {lat: userdata.address.customerLocation.coordinates[1], long: userdata.address.customerLocation.coordinates[0]});
                                                                    temp = {
                                                                        id: Job[key]._id,
                                                                        name: Job[key].firstName+ " "+Job[key].lastName,
                                                                        long: prev_long,
                                                                        lat: prev_lat,
                                                                        distance: dist
                                                                    };
                                                                    console.log("prev_lat "+ userdata.addLatitude + "prev_long " + prev_long)
                                                                    console.log("temp distance = "+ dist)
                                                                    console.log("temp = "+temp)
                                                                    embeddedCb(null,temp);
                                                                }
                                                                else if (nextTime && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff2) {
                                                                    lat = dataToSave.address.customerLocation.coordinates[1]
                                                                    long = dataToSave.address.customerLocation.coordinates[0]
                                                                    dist = UniversalFunctions.getDistanceBetweenPoints({lat:lat, long:long}, {lat: userdata.address.customerLocation.coordinates[1], long: userdata.address.customerLocation.coordinates[0]});
                                                                    temp = {
                                                                        id: Job[key]._id,
                                                                        name: Job[key].firstName+ " "+Job[key].lastName,
                                                                        long: long,
                                                                        lat: lat,
                                                                        distance: dist
                                                                    };
                                                                    console.log("lat "+ lat + "long " + long)
                                                                    console.log("temp distance = "+ dist)
                                                                    console.log("temp = "+temp)
                                                                    embeddedCb(null,temp);
                                                                }
                                                                else
                                                                    embeddedCb(null)
                                                            }
                                                        });
                                                    }
                                                }
                                                else
                                                    embeddedCb(null);
                                            }
                                            else {
                                                for (i = 0; i < data1.length; i++)
                                                {
                                                    if (data1[i]._id == Job[key]._id)
                                                    {
                                                        long = data1[i].homeAddress.customerLocation.coordinates[0];
                                                        lat = data1[i].homeAddress.customerLocation.coordinates[1];
                                                        break;
                                                    }
                                                }
                                                dist = UniversalFunctions.getDistanceBetweenPoints({lat:lat, long:long}, {lat: userdata.address.customerLocation.coordinates[1], long: userdata.address.customerLocation.coordinates[0]});
                                                temp = {
                                                    id: Job[key]._id,
                                                    name: Job[key].firstName+ " "+Job[key].lastName,
                                                    long: long,
                                                    lat: lat,
                                                    distance: dist*1000
                                                };
                                                console.log("temp distance = "+temp.distance)
                                                if(dist<=20000)
                                                    embeddedCb(null,temp);
                                                else
                                                    embeddedCb(null);
                                            }
                                        }
                                    });
                                }
                            })(key))
                        }(key));
                    }
                    async.parallel(taskInImages, function (err, result) {
                        data2=result.slice()
                        var i, j, l = data2.length, k = {};
                        if (l > 0 && data2) {
                            for (i = 1; i < l && data2[i]; i++)
                            {
                                k = data2[i];
                                j = i - 1;
                                while (data2[j] && j >= 0 && data2[j].distance > k.distance) {
                                    data2[j + 1] = data2[j];
                                    j = j - 1;
                                }
                                data2[j + 1] = k;
                            }
                            for (i = 0; i < l; i++)
                            {
                                if(data2[i] && data2[i].id && data2[i].distance <=20000) {
                                    dataToSave.push(data2[i]);
                                    flag1 = true;
                                }
                            }
                            cb(null,dataToSave);
                        }
                    });
                }
                else
                    cb(null,[]);
            },
            function(cb,data7){
                console.log(">>>>>>>>hello"+userdata.patientId)
                if(flag1==true)
                    cb(null,data7);
                else{
                    var query ={
                        _id: userdata.patientId
                    }
                    Service.patient.getPatient(query, {_id: 0}, {}, function (err, data) {
                        if (err)
                            cb(err)
                        else {
                            data3 = data[0];
                            console.log(">>>>>>>>"+data)
                            cb(null,data7)
                        }
                    });
                }
            },
        function(cb,data7){
            if(flag1==true)
                cb(null,data7);
            else{
                var query ={
                    _id: userdata.patientId
                }
                console.log(">>>>>>>>eee"+JSON.stringify(data3))
                var credits = parseInt(data3.walletCredits + userdata.payment.totalPrice - userdata.payment.discount)
                var dataToUpdate = {
                    walletCredits: credits
                }
                Service.patient.updatePatient(query, {$set: dataToUpdate}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else
                        cb(null,data7)
                });
            }
        }
        ],
        function (error, results) {
            if (!error)
                callback(null,dataToSave);
            else {
                callback(error);
            }
        })
};

var reassignment = function (tokenData, payload, callback) {
    var userdata = null;
    async.series([
        function (cb) {
            var query = {
                _id: tokenData.id
            };
            Service.admin.getAdmin(query, {}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
            var query = {
                _id: payload.previousAppointmentId
            };
            Service.appointment.getAppointment(query, {_id: 0}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (!data && data.length == 0)
                        cb(ERROR.INVALID_APPOINTMENT)
                    else {
                        userdata = data[0];
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
            if(userdata.doctorId!=null)
                userdata.doctorId=payload.doctorId;
            else
                userdata.diagnosticId=payload.diagnosticId;
            Service.appointment.createAppointment(userdata, function (err, data) {
                if (err)
                    cb(err)
                else {
                        cb(null)
                    }
            });
        },
    ],
        function(error,result){
            if(error)
                callback(error)
            else
                callback(null)
        });
}

var calculateDistanceViaGoogleDistanceMatrix = function (origins, destinations, callback) {
    distance.matrix(origins, destinations, function (err, distances) {
        if (!err) {
            callback(null, distances);
        }
    });
};

var getOffsetViaLatLong = function (long, lat, callback) {
    var rawOffset = 0;
    /* Request timezone with location coordinates */
    timezoner.getTimeZone(
        lat, // Latitude coordinate
        long, // Longitude coordinate
        function (err, data) {
            if (err) {
                callback(err)
            } else {
                console.log('data', data)
                rawOffset = data.rawOffset;
                rawOffset = rawOffset + data.dstOffset;
                callback(null, {rawOffset: rawOffset})
            }
        }, {language: 'es', key: Config.serverConfig.GOOGLE_API_KEY}
    );
};



module.exports = {
    cancelAppointment: cancelAppointment,
    reassignment: reassignment
};
