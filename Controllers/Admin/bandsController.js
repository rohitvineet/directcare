'use strict';

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var async = require('async');
var Service = require('../../Services');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var TokenManager = require('../../lib/TokenManager');
var secRand = require("secure-random");
var moment = require('moment')

function generatePromoCode(length) {
    return secRand(length, {type: 'Buffer'}).toString('hex');
}

var addDomainOfService = function (payload, callback) {
    var data = {};
    payload.name=payload.name.toLowerCase();
    async.series([
        function (callback) {
            Service.service.getAllDomain({name: payload.name},{},{}, function (err, result) {
                if (!err) {
                    if (result.length > 0) {
                        callback(ERROR.DOMAIN_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (cb) {
            data.name= payload.name;
            Service.service.addDomain(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,results);
        else {
            callback(error);
        }
    })
};

var getAllDomainOfService = function (callback) {
    var domainList = [];
    async.series([
            function (callback) {
                var projection = {
                    key: 1 , name: 1, _id: 0
                };
                var setOptions = {
                };
                var options = {
                };
                Service.service.getAllDomain({}, projection,options,setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        domainList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'domainList': domainList});
            }
        }
    );
};


var getAllGeoFence = function (callback) {
    var geoFenceList = [];
    async.series([
            function (callback) {
                var projection = {
                    key: 1, locationName: 1, location: 1
                };
                var setOptions = {
                };
                var options = {
                };
                Service.service.getAllGeo({}, projection,options,setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        geoFenceList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'geoFenceList': geoFenceList});
            }
        }
    );
};

var getAllTypeOfService = function (callback) {
    var typeList = [];
    async.series([
            function (callback) {
                var projection = {
                    key: 1, name: 1, _id: 0
                };
                var setOptions = {};
                var options = {};
                Service.service.getAllType({}, projection,options,setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        typeList = result;
                        callback();
                    }
                });
            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'typeList': typeList});
            }
        }
    );
};

var getAllPriceList = function (callback) {
    var priceList = [];
    async.series([
            function (callback) {
                var projection = {
                    domainOfServiceId: 1, typeOfServiceId: 1, cost: 1, _id: 0
                };
                var setOptions = {
                };
                var options = {
                };
                Service.service.getAllPrice({}, projection,options,setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        priceList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'PriceList': priceList});
            }
        }
    );
};


var getAllDiagnosticTypeOfService = function (callback) {
    var diagnosticTypes = [];
    async.series([
            function (callback) {
                var projection = {
                    key: 1,name: 1, subType: 1, cost: 1, _id: 0
                };
                var setOptions = {
                };
                var options = {
                };
                Service.service.getAllDiagnosticTypes({}, projection,options,setOptions, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        diagnosticTypes = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'DiagnosticTypes': diagnosticTypes});
            }
        }
    );
};

var getAllPatients = function (callback) {
    var patientList = [];
    async.series([
            function (callback) {
                var projection = {};
                var setOptions = {
                };
                var options = {
                    sort: {firstName: 1}
                };
                Service.patient.getPatient({}, projection,options, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        patientList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'Patient': patientList});
            }
        }
    );
};


var getAllDoctors = function (callback) {
    var doctorList = [];
    async.series([
            function (callback) {
                var projection = {};
                var setOptions = {
                };
                var options = {
                    sort: {firstName: 1}
                };
                Service.doctor.getDoctor({}, projection,options, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        doctorList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'Doctor': doctorList});
            }
        }
    );
};

var getAllDiagnostic = function (callback) {
    var diagnosticList = [];
    async.series([
            function (callback) {
                var projection = {};
                var setOptions = {
                };
                var options = {
                    sort: {firstName: 1}
                };
                Service.diagnostic.getDiagnostic({}, projection,options, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        diagnosticList = result;
                        callback();
                    }
                });

            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'Diagnostic': diagnosticList});
            }
        }
    );
};


var validateLocation = function (payload, callback) {
    var GeoFenceId = -1;
    async.series([
            function (cb) {
                var query = {
                    location: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: [payload.longitude, payload.latitude]
                            }
                        }
                    }
                };
                Service.service.getAllGeo(query, {__v: 0}, {lean: true, limit: 1}, function (err, data) {
                    console.log('output', err, data);
                    if (!err) {
                        if (data.length > 0) {
                            GeoFenceId = data[0]._id;
                            cb(null);
                        }
                        else {
                            cb(ERROR.NOT_LOCATION);
                        }
                    }
                });
            }
        ],
        function (error, results) {
            if (error) {
                return callback(error);
            } else {
                return callback(null, {'GeoFence Id': GeoFenceId});
            }
        }
    );
};

//Adding Type of service
var addTypeOfService = function (payload, callback) {
    payload.name=payload.name.toLowerCase();
    async.series([
        function (callback) {
            Service.service.getAllType({name: payload.name},{},{}, function (err, result) {
                if (!err) {
                    if (result.length > 0) {
                        callback(ERROR.TYPE_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (cb) {
            var data = {};
            data.name= payload.name;
            Service.service.addType(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,results);
        else {
            callback(error);
        }
    })
};

//Adding Geofencing Area
var addGeoFence = function (payload, callback) {
    payload.locationName=payload.locationName.toLowerCase();
    var cod = payload.coordinates;
    async.series([
         function (callback) {
             for(var i=0;i<cod.length;i++) {
                 var valid = true;
                 if (cod[i][0] < -90 || cod[i][0] > 90) {
                     valid = false;
                     break;
                 }
                 if (cod[i][1] < -180 || cod[i][1] > 180) {
                     valid = false;
                     break;
                 }
             }
             if(!valid)
                 callback(ERROR.INVALID_LAT_LONG);
             else
                 callback(null);
        },
        function (callback) {
            Service.service.getAllGeo({locationName: payload.locationName},{},{}, function (err, result) {
                if (!err) {
                    if (result.length > 0) {
                        callback(ERROR.GEO_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (cb) {
            var data = {
                locationName: payload.locationName,
                location: {
                    type: "Polygon",
                    coordinates: [payload.coordinates]
                }};
            Service.service.addGeo(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,results);
        else {
            callback(error);
        }
    })
};

//Adding Pricing Table
var addPricingTable = function (payload, callback) {
    async.series([
        function (callback) {
            Service.service.getAllDomain({key: payload.domainOfServiceId},{},{}, function (err, result) {
                if (!err) {
                    if (result.length == 0) {
                        callback(ERROR.DOMAIN_NOT_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (callback) {
            Service.service.getAllType({key: payload.typeOfServiceId},{},{}, function (err, result) {
                if (!err) {
                    if (result.length == 0) {
                        callback(ERROR.TYPE_NOT_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (callback) {
            Service.service.getAllPrice({$and: [{typeOfServiceId: payload.typeOfServiceId},{domainOfServiceId: payload.domainOfServiceId}]},{},{}, function (err, result) {
                if (!err) {
                    if (result.length > 0) {
                        callback(ERROR.PRICE_LIST_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (cb) {
            var data = {};
            data.domainOfServiceId = payload.domainOfServiceId;
            data.typeOfServiceId = payload.typeOfServiceId;
            data.cost = payload.cost;
            Service.service.addPrice(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error) callback(null,results);
        else {
            callback(error);
        }
    })
};

//Adding Diagnostic type of service
var addDiagnosticType = function (payload, callback) {
    payload.name = payload.name.toLowerCase();
    async.series([
        function (callback) {
            Service.service.getAllDiagnosticTypes({name: payload.name},{},{}, function (err, result) {
                if (!err) {
                    if (result.length > 0) {
                        callback(ERROR.DIAGNOSTIC_EXIST);
                    }
                    else {
                        callback(null);
                    }

                }
                else
                    callback(err);
            });
        },
        function (cb) {
            var data = {};
            data.name = payload.name;
            data.subType = payload.subType;
            data.cost = payload.cost;
            Service.service.addDiagnostic(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error) callback(null,results);
        else {
            callback(error);
        }
    })
};

//Adding subscription
var addSubscription = function (tokenData, payload, callback) {
    payload.name = payload.name.toLowerCase();
    async.series([
        function(cb){
           var query = {
                name : payload.name
           }
            Service.subscription.getSubscription(query, {}, {}, function (err, data) {
                if (data.length == 0) {
                    cb(null);
                }
                else {
                    cb(ERROR.SUBSCRIPTION_EXIST)
                }
            });
        },
        function (cb) {
            var data = {};
            data.name = payload.name;
            data.numberOfServices = payload.numberOfServices;
            data.cost = payload.cost;
            data.duration = payload.duration;
            Service.subscription.createSubscription(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error) callback(null,results);
        else {
            callback(error);
        }
    })
};


//Get All subscription
var getAllSubscription = function (tokenData, callback) {
    var userdata = null;
    async.series([
        function(cb){
            var query = {}
            Service.subscription.getSubscription(query, {}, {}, function (err, data) {
                if (data.length > 0) {
                    userdata = data;
                    cb(null);
                }
                else {
                    cb(ERROR.NO_SUBSCRIPTION)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,userdata);
        else {
            callback(error);
        }
    })
};

//Block subscription
var blockSubscription = function (tokenData, payload, callback) {
    var userdata = null;
    async.series([
        function(cb){
            var query = {
                _id: payload.subscriptionId
            };
            var dataToUpdate = {
                $set: {isActive: false}
            }
            Service.subscription.updateSubscription(query, dataToUpdate, {}, function (err, data) {
                if (data && data.length > 0) {
                    cb(null);
                }
                else if(err){
                    cb(ERROR.INVALID_SUBSCRIPTION)
                }
                else
                    cb(null)
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null);
        else {
            callback(error);
        }
    });
};

//Block user
var blockUser = function (tokenData, payload, callback) {
    var userdata = null;
    async.series([
        function(cb){
            var query = {
                _id: payload.id
            };
            var dataToUpdate = {}
            if(payload.userType==UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT) {
                dataToUpdate = {
                    $set: {isBlocked: true}
                };
                Service.patient.updatePatient(query, dataToUpdate, {}, function (err, data) {
                    if (data) {
                        cb(null);
                    }
                    else if (err) {
                        cb(ERROR.USER_NOT_FOUND)
                    }
                    else
                        cb(null)
                });
            }
            else if(payload.userType==UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.DOCTOR){
                dataToUpdate = {
                    $set: {userBlock: true}
                };
                Service.doctor.updateDoctor(query, dataToUpdate, {}, function (err, data) {
                    if (data && data.length > 0) {
                        cb(null);
                    }
                    else if (err) {
                        cb(ERROR.USER_NOT_FOUND)
                    }
                    else
                        cb(null)
                });
            }
            else{
                dataToUpdate = {
                    $set: {userBlock: true}
                };
                Service.diagnostic.updateDiagnostic(query, dataToUpdate, {}, function (err, data) {
                    if (data && data.length > 0) {
                        cb(null);
                    }
                    else if (err) {
                        cb(ERROR.USER_NOT_FOUND)
                    }
                    else
                        cb(null)
                });
            }
        }
    ], function (error, results) {
        if (!error)
            callback(null);
        else {
            callback(error);
        }
    });
};

//View all Bookings
var allBookings = function (tokenData, callback) {
    async.series([
        function (cb) {
            var data = {};
            Service.appointment.getAppointment({},{},{}, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,results);
        else {
            callback(error);
        }
    })
};

//Setting PromoCode
var setPromoCode = function (tokenData, payload, callback) {
    var promoCode = generatePromoCode(Config.APP_CONSTANTS.PROMOCODE_LENGTH);
    promoCode = promoCode.toUpperCase();
    console.log(promoCode)
    async.series([
        function(cb){
            var flag=false;
            var query = {
                promoCode: promoCode
            }
            Service.promoCode.getPromoCode(query, {}, {}, function (err, data) {
                if (data) {
                    flag=true;
                    cb(null);
                }
                else{
                    promoCode = generatePromoCode(Config.APP_CONSTANTS.PROMOCODE_LENGTH);
                    promoCode = promoCode.toUpperCase();
                    cb(null);
                }
            });
        },
        function (cb) {
            var data = {};
            data.promoCode = promoCode;
            data.type = payload.type;
            data.value = payload.value;
            data.maxUsageCountLimit = payload.maxUsageCountLimit;
            data.validUpto = moment(new Date(payload.validUpto)).format('YYYY-MM-DD HH:mm:ss');
            //    data.validForServiceType = payload.validForServiceType;
            Service.promoCode.createPromoCode(data, function (err, data) {
                if (!err) {
                    cb(null,data);
                }
                else {
                    cb(err)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,results);
        else {
            callback(error);
        }
    })
};

module.exports = {
    addDomainOfService: addDomainOfService,
    addTypeOfService: addTypeOfService,
    getAllDomainOfService: getAllDomainOfService,
    getAllTypeOfService: getAllTypeOfService,
    addPricingTable: addPricingTable,
    getAllPriceList: getAllPriceList,
    addDiagnosticType: addDiagnosticType,
    getAllDiagnosticTypeOfService: getAllDiagnosticTypeOfService,
    addGeoFence: addGeoFence,
    getAllGeoFence: getAllGeoFence,
    getAllPatients: getAllPatients,
    getAllDoctors: getAllDoctors,
    getAllDiagnostic: getAllDiagnostic,
    validateLocation: validateLocation,
    addSubscription: addSubscription,
    getAllSubscription: getAllSubscription,
    blockSubscription: blockSubscription,
    setPromoCode: setPromoCode,
    blockUser: blockUser,
    allBookings: allBookings
  };
