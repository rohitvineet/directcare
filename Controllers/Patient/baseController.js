'use strict';

var Service = require('../../Services');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var async = require('async');
var moment = require('moment');
var TokenManager = require('../../lib/TokenManager');
var bcrypt = require('bcrypt');
var jwtDecode = require('jwt-decode');
var CodeGenerator = require('../../lib/CodeGenerator');
var NotificationManager = require('../../lib/NotificationManager');
var UploadManager = require('../../lib/UploadManager');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var SUCCESS = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS;
var nodemailer = require("nodemailer");
var smtpTransport = require('nodemailer-smtp-transport');
var emailConfig = require('../../Config/emailConfig');
var Templates = require('../../Templates');

var registerPatient = function (payloadData, callback) {
    payloadData.email = payloadData.email.toLowerCase();
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payloadData;
    var patientData = null;
    var dataToUpdate = {};
    if (payloadData.hasOwnProperty("profilePic") && payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }
    async.series([
        function (cb) {
                var query = {
                    $or: [{email: payloadData.email}, {mobileNo: payloadData.mobileNo}]
                };
                Service.patient.getPatient(query, {}, {lean: true}, function (error, data) {
                    if (error) {
                        cb(error);
                    } else {
                        if (data && data.length > 0) {
                            if (data[0].isVerified == true) {
                                cb(ERROR.USER_ALREADY_REGISTERED)
                            }
                            else {
                                Service.patient.deletePatient({_id: data[0]._id}, function (err, updatedData) {
                                    if (err)
                                        cb(err)
                                    else
                                        cb(null);
                                })
                            }
                        } else {
                            cb(null);
                        }
                    }
                });

        },
        function(cb){
            if(dataToSave.password)
            {
                const SALT_WORK_FACTOR = 10;

                bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                    if (err)
                        cb(err);
                    bcrypt.hash(dataToSave.password, salt, function (err, hash) {
                        if (err)
                            cb(err);
                        else {
                            dataToSave.password = hash;
                            cb(null);
                        }
                    });
                });
            }
        },
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            if (dataToSave.mobileNo && dataToSave.mobileNo.split('')[0] == 0) {
                cb(ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            //Insert Into DB
            dataToSave.mobileNo = payloadData.mobileNo;
            dataToSave.countryCode = payloadData.countryCode;
            dataToSave.firstName = payloadData.firstName;
            dataToSave.lastName = payloadData.lastName;
            var offset = payloadData.timezoneOffset;
            Service.patient.createPatient(dataToSave, function (err, patientDataFromDB) {
                console.log('hello', err, patientDataFromDB)
                if (err) {
                        cb(err)
                } else {
                    patientData = patientDataFromDB;
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            var profilePicURL = {};
            dataToUpdate.profilePicURL = {
                original: UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.agentDefaultPicUrl,
                thumbnail: UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.agentDefaultPicUrl
            };
            var profilePicture = payloadData.profilePic;
            if (payloadData.hasOwnProperty("profilePic") && profilePicture && profilePicture.hapi.filename) {
                UploadManager.uploadProfilePicture(profilePicture, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.patient, (payloadData.mobileNo).replace('+', '0'), function (err, uploadedInfo) {
                    console.log('update profile pic', err, uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL = {
                            original: uploadedInfo.profilePicture,
                            thumbnail: uploadedInfo.profilePictureThumb
                        }
                        cb();
                    }
                });
            }
            else {
                console.log('urls');
                console.log(profilePicURL);
                cb();
            }
        },
        function (cb) {
            if (patientData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update Patient
                var criteria = {
                    _id: patientData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.patient.updatePatient(criteria, setQuery, {new: true}, function (err, updatedData) {
                    patientData = updatedData;
                    cb(err, updatedData)
                })
            } else {
                if (patientData && patientData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original) {
                    var criteria = {
                        _id: patientData._id
                    };
                    Service.patient.deletePatient(criteria, function (err, updatedData) {
                        cb(ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                } else {
                    cb();
                }
            }
        },
        function (cb) {
            //Set Access Token
            if (patientData) {
                var tokenData = {
                    id: patientData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            var data = {
                user_name: dataToSave.firstName,
                FAQ: '#',
                headerLogo: '#',
                registrationDate: '63876328763',
                footerlogo: "#",
                instagramImage:"#",
                twitterImage:"#",
                facebookImage:"#",
                accessToken: accessToken
            }

            var text;

            text = Templates.customerRegister.register(data);
            console.log("eee",text)

            NotificationManager.register('REGISTRATION_MAIL', payloadData.email, text, function (err, data) {
                if (!err) {
                    console.log('mail', err, data);
                }
                console.log("error",err)
                cb(null);
            })
//
        },

    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(patientData)
            });
        }
    });
};

var loginPatient = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var updatedUserDetails = null;
    var password = '';
    payloadData.email = payloadData.email.toLowerCase();
    var uniqueCode;
        async.series([
            function (callback) {
                var query = {
                    email: payloadData.email
                };
                Service.patient.getPatient(query, {password: 1, firstName: 1, lastName: 1, isVerified: 1}, {lean: true}, function (err, result) {
                    console.log("err,result", err, result)
                    if (err) {
                        callback(err);
                    }
                    else
                    {
                        if (result && result.length > 0 && result[0].isVerified==true) {
                            userFound = result && result[0] || null;
                            password = result[0].password;
                            callback(null);
                        }
                        else {
                            if(result.length > 0 && result[0].isVerified !=true)
                                callback(ERROR.EMAIL_NOT_VERIFIED);
                            else
                                callback(ERROR.INCORRECT_EMAIL);
                        }
                    }
                });
            },
            function (callback){
                bcrypt.compare(payloadData.password,password,function(err,isMatch){
                    if(err)
                        callback(err);
                    else {
                        if(isMatch) {
                            successLogin=true;
                            callback(null);
                        }
                        else {
                            callback(ERROR.INVALID_USER_PASS);
                        }
                    }
                });
            },

        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(userFound)

            });
        }
    });
};


var logoutPatient = function (userData, callbackRoute) {
    async.series([
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$unset: {accessToken: 1}};
                Service.patient.updatePatient(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};



var accessTokenLogin = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1,
                    firstName: 1,
                    lastName: 1
                };

                Service.patient.getPatient(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }

                })
            },
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                };

                var setQuery = {
                };

                Service.patient.updatePatient(criteria, setQuery, {new: true}, function (err, data) {
                    cb(err, data);
                });

            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};


var verifyAccount = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1
                };

                Service.patient.getPatient(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }

                })
            },
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                };

                var setQuery = {
                    isVerified: true
                };

                Service.patient.updatePatient(criteria, setQuery, {new: true}, function (err, data) {
                    cb(err, data);
                });

            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};

var changePassword = function (userData, data, callbackRoute) {
    var userdata = null;
    async.series([
            function(cb) {
                Service.patient.getPatient({_id: userData.id}, {password: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (callback){
                bcrypt.compare(data.oldPassword,userdata[0].password,function(err,isMatch){
                    if(err)
                        callback(err);
                    else {
                        if(isMatch) {
                            callback(null);
                        }
                        else {
                            callback(ERROR.INCORRECT_OLD_PASS);
                        }
                    }
                });
            },
            function(cb){
                if(data.newPassword != data.oldPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
                else
                    cb(ERROR.SAME_PASSWORD);
            },
            function (callback) {
                var condition = {_id: userData.id};
                var dataToUpdate = {$set: {password: data.newPassword}};
                Service.patient.updatePatient(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};


var resetPassword = function (data, callbackRoute) {
    var decodedToken = null;
    var userdata = null;
    async.series([
            function(cb){
                decodedToken = jwtDecode(data.hash);
                Service.patient.getPatient({email: decodedToken.email}, {resetToken: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_RESET_TOKEN);
                        else {
                            if(dataFromDB[0].resetToken != "") {
                                userdata = jwtDecode(dataFromDB[0].resetToken);
                                cb(null);
                            }
                            else
                                cb(ERROR.INCORRECT_RESET_TOKEN);
                        }
                    }
                });
            },
            function(cb){
                if(decodedToken.hash === userdata.hash)
                    cb(null);
                else
                    cb(ERROR.INCORRECT_RESET_TOKEN);
            },
            function(cb){
                if(data.newPassword)
                {
                    const SALT_WORK_FACTOR = 10;
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        if (err)
                            cb(err);
                        bcrypt.hash(data.newPassword, salt, function (err, hash) {
                            if (err)
                                cb(err);
                            else {
                                data.newPassword = hash;
                                cb(null);
                            }
                        });
                    });
                }
            },
            function (callback) {
                var condition = {email: decodedToken.email};
                var dataToUpdate = {$set: {password: data.newPassword, accessToken: "", resetToken: ""}};
                Service.patient.updatePatient(condition, dataToUpdate, {}, function (err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        callback();
                    }
                });
            }
        ],
        function (error, result) {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

var forgetPassword = function (data, callback) {
    var resetToken = null;
    var userdata = null;
    async.series([
            function(cb) {
                Service.patient.getPatient({email: data.email}, {}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (dataFromDB.length == 0)
                            cb(ERROR.INCORRECT_EMAIL);
                        else {
                            userdata = dataFromDB;
                            cb(null);
                        }
                    }

                })
            },
            function (cb) {
                var rand = Math.floor(Math.random() * 90000) + 10000;
                var resetTokenData = {
                    email: data.email,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.PATIENT,
                    hash: rand
                };
                TokenManager.resetToken(resetTokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            resetToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                });
            }],
        function (error, result) {
            if (error) {
                return callback(error);
            } else {
                return callback(null);
            }
        });
};

var getProfile = function (tokenData, data, callback) {
    var userdata = {};
    var userFound = null;
    var details;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    _id: 0
                };

                Service.patient.getPatient(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }
                });
            }],
        function (err, data)
        {
            if (!err)
                callback(null, {    data : userdata    });
            else
                callback(err);

        }
    );
};

var editProfile = function (tokenData, payload, callback) {
    var userdata = {};
    var userFound = null;
    var details, setQuery = {};
    var c=0;
    async.series([
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                }

                var projection = {
                    accessToken: 1,
                    firstName: 1,
                    lastName: 1,
                    mobileNo: 1,
                    countryCode: 1
                };

                Service.patient.getPatient(criteria, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var criteria = {
                    mobileNo: payload.mobileNo,
                    _id: {$ne : tokenData._id}
                }
                Service.patient.getPatient(criteria, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length > 0)
                            cb(ERROR.MOBILE_NO_ALREADY_USED)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function (cb) {
                console.log(payload.profilePic)
                var profilePicture = payload.profilePic;
                if (typeof(payload.profilePic)=="object" && payload.hasOwnProperty("profilePic") && payload.profilePic && profilePicture.hapi.filename) {

                    UploadManager.uploadProfilePicture(profilePicture, UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.patient, payload.mobileNo, function (err, uploadedInfo) {
                        if (err) {
                            cb(err)
                        } else {
                            setQuery.profilePicURL = {
                                original: uploadedInfo.profilePicture,
                                thumbnail: uploadedInfo.profilePictureThumb
                            }
                            c++;
                            cb();
                        }
                    });
                }
                else {
                    cb();
                }
            },
            function (cb) {
                var criteria = {
                    _id: tokenData.id
                };

                if(payload.firstName != userdata[0].firstName) {
                    setQuery.firstName = payload.firstName;
                    c++;
                }

                if(payload.lastName != userdata[0].lastName) {
                    setQuery.lastName = payload.lastName;
                    c++;
                }
                if(payload.mobileNo != userdata[0].mobileNo) {
                    setQuery.mobileNo = payload.mobileNo;
                    c++;
                }
                if(payload.countryCode.localeCompare(userdata[0].countryCode)!=0){
                    setQuery.countryCode = payload.countryCode;
                    c++;
                }
                if(c!=0) {
                    Service.patient.updatePatient(criteria, {$set: setQuery}, {new: true}, function (err, data) {
                        userdata = data;
                        cb(null);
                    });
                }
                else
                    cb(ERROR.NOTHING_TO_UPDATE);
            }],
        function (err, data)
        {
            if (!err)
                callback(null);
            else
                callback(err);
        }
    );
};

var addAddress = function(tokenData, payload, callback){
    payload.label=payload.label.toLowerCase();
    payload.streetAddress=payload.streetAddress.toLowerCase();
    async.series([
        function (cb) {
            var query = {
                _id: tokenData.id
            };
            Service.patient.getPatient(query, {}, {}, function (err, data) {
                if (err)
                    cb(err);
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        cb(null)
                    }
                }
            })

        },
        function (cb) {
            //Insert Into DB
            var dataToSave = {};
            dataToSave.Address = {};
            dataToSave.Address.label = payload.label;
            dataToSave.Address.streetAddress = payload.streetAddress;
            dataToSave.Address.customerLocation= {};
            dataToSave.Address.customerLocation.coordinates= [];
            dataToSave.Address.customerLocation.coordinates[0] = payload.addressLongitude;
            dataToSave.Address.customerLocation.coordinates[1] = payload.addressLatitude;
            var condition = {
                _id: tokenData.id
            };
            var dataToUpdate = {
                $push : {address : dataToSave}
            };
            Service.patient.updatePatient(condition, dataToUpdate, {}, function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null);
                }
            });
        },
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    });
};

var delAddress = function(tokenData, payload, callback){
    async.series([
        function (cb) {
            var query = {
                _id: tokenData.id
            };
            Service.patient.getPatient(query, {}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        cb(null)
                    }
                }
            })

        },
        function (cb) {
            var query = {
                "address":{
                    $elemMatch : {
                        "Address._id": payload.id,
                        "Address.isDeleted": false
                    }
                }
            };
            var projection = {
                "address": 1
            }
            Service.patient.getPatient(query, projection, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.ADDRESS_NOT_FOUND)
                    else {
                        cb(null)
                    }
                }
                console.log("hello");
                console.log(data);
            })

        },
        function (cb) {
            var criteria = {
                $set: { "address.$.Address.isDeleted": true }
            };
            var query = {
                "address":{
                    $elemMatch : {
                        "Address._id": payload.id
                    }
                }
            };
            Service.patient.updatePatient(query, criteria, function (err, data) {
                if (err)
                    cb(err)
                else
                    cb(null)
            });

        }],
        function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    });
};

var getAddress = function(tokenData, callback){
    var userdata = [];
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })

            },
            function (cb) {
                var query = {_id: tokenData.id};
                Service.patient.getPatient(query, { _id: 0, "address":1  } , {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.ADDRESS_NOT_FOUND);
                        else {
                            var c=0;
                           for(var i=0;i<data[0].address.length;i++)
                            {
                                if(data[0].address[i].Address.isDeleted == false)
                                {
                                    userdata.push(data[0].address[i]);
                                }
                            }
                            //userdata = data;
                            cb(null)
                        }
                    }
                })
            }],
        function (err, data) {
            if (err) {
                callback(err);
            } else {
                callback(null, {data: userdata});
            }
        });
};

var updateAddress = function(tokenData, payload, callback){
    var userdata = null;
    var dataToSave = {};
    async.series([
        function (cb) {
            var query = {
                    "address":{
                        $elemMatch : {
                            "Address._id": payload.id
                        }
                    }
                 };
            Service.patient.getPatient(query, {}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        userdata = data;
                        cb(null)
                    }
                }
            });

        },
        function (cb) {
            //Insert Into DB
            var Address = {
                _id: payload.id,
                isDeleted : false,
                label: payload.label,
                streetAddress: payload.streetAddress,
                customerLocation:{
                    coordinates : [payload.addressLongitude,payload.addressLatitude],
                    type:"Point"
                }
            }
            dataToSave = Address;
            var query = {
                    "address": {
                        $elemMatch: {
                            "Address._id": payload.id
                        }
                    }
                };
            var dataToUpdate =  {$set : {"address.$.Address": dataToSave}};
            Service.patient.updatePatient(query, dataToUpdate, {}, function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    callback(null);
                }
            });
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    });
};



module.exports = {
    registerPatient: registerPatient,
    loginPatient: loginPatient,
    logoutPatient: logoutPatient,
    accessTokenLogin: accessTokenLogin,
    verifyAccount: verifyAccount,
    changePassword: changePassword,
    resetPassword: resetPassword,
    forgetPassword: forgetPassword,
    getProfile: getProfile,
    editProfile: editProfile,
    addAddress: addAddress,
    delAddress: delAddress,
    getAddress: getAddress,
    updateAddress: updateAddress
  };
