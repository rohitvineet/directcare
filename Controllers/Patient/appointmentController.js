'use strict';

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var async = require('async');
var Service = require('../../Services');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var TokenManager = require('../../lib/TokenManager');
var UploadManager = require('../../lib/UploadManager');
var moment = require('moment')
var timezoner = require('timezoner');
var distance = require('google-distance');
distance = require('google-distance-matrix');
var CONST = require('../../Config/constants');
distance.apiKey = Config.serverConfig.GOOGLE_API_KEY;
var GeoPoint = require('geopoint');

var bookAppointment = function (tokenData, payload, callback) {
    var dataToSave = {};
    var appointmentData = {};
    var timezoneOffset = null;
    var data1 = null, type = null;
    var data2 = [], date = null, time = null,kk=null;
    if(!Array.isArray(payload.attachImages))
        payload.attachImages=[payload.attachImages]

    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })

            },
            function (cb) {
                //get offset via latitude and longitude
                getOffsetViaLatLong(payload.addLongitude, payload.addLatitude, function (err, data) {
                    console.log('fifth:', err, data);
                    if (!err) {
                        timezoneOffset = (data.rawOffset / 60);
                        console.log(timezoneOffset)
                        var y = moment(new Date(payload.scheduleDate)).utcOffset(-timezoneOffset).format('YYYY-MM-DD HH:mm:ss');
                        y = new Date(y).toISOString();

                        var diff = UniversalFunctions.getRange(new Date().toISOString(), y, UniversalFunctions.CONFIG.APP_CONSTANTS.TIME_UNITS.DAYS);
                        if (diff >= 0) {
                            cb(null);
                        }
                        else {
                            cb(ERROR.SCHEDULE_DATE);
                        }

                    }
                    else {
                        cb(err);
                    }
                });
            },
            function(cb){
                var Jobimages = payload.attachedImages;
                dataToSave.attachedImagesURL = [];
                if(payload.hasOwnProperty("attachedImages") && Jobimages.length!=0){
                    if (Jobimages) {
                        var taskInImages = [];
                        for (var key in Jobimages) {
                            (function (key) {
                                taskInImages.push((function (key) {
                                    return function (embeddedCb) {
                                        console.log("%%%%%%%%% Image"+[key],Jobimages[key]);
                                        UploadManager.uploadAttachedImage(Jobimages[key], UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.folder.patient, (UniversalFunctions.generateRandomString()+"_"+UniversalFunctions.getTimestamp()), function (err, uploadedInfo) {
                                            console.log('update attached image', err, uploadedInfo)
                                            if (err) {
                                                console.log("Not Uploaded Image")
                                                embeddedCb()
                                            } else {
                                                dataToSave.attachedImagesURL.push(
                                                    uploadedInfo.filename
                                                )
                                                embeddedCb();
                                            }
                                        });
                                    }
                                })(key))
                            }(key));
                        }
                        async.parallel(taskInImages, function (err, result) {
                            cb(null);
                        });
                    }
                }
                else
                    cb()
            },
            function (cb) {
                dataToSave.address = {};
                var coordinates = [];
                coordinates[0]=payload.addLongitude;
                coordinates[1]=payload.addLatitude;
                var location = {
                    customerLocation : {
                        coordinates: coordinates
                    },
                    isDeleted: false,
                    streetAddress: payload.streetAddress
                };
                dataToSave.patientId = tokenData._id;
                dataToSave.address = location;
                dataToSave.additionalDetails = {};
                dataToSave.additionalDetails={
                    symptoms: payload.symptoms,
                    allergies: payload.allergies,
                    medication: payload.medication,
                    specialInstruction: payload.specialInstruction
                };
                dataToSave.promoCode = payload.promoCode;
                dataToSave.paymentMethod = payload.paymentMethod;
                dataToSave.subscriptionId = payload.subscriptionId;
                dataToSave.totalCost = payload.totalCost;
                dataToSave.paymentCardId = payload.paymentCardId;
                dataToSave.discount = payload.discount;
                var y = moment(new Date(payload.scheduleDate)).utcOffset(-timezoneOffset).format('YYYY-MM-DD HH:mm:ss');
                //y = new Date(y).toISOString();
                dataToSave.dates = {};
                dataToSave.dates.scheduleDate = y;
                var v= moment(y).utcOffset(2*timezoneOffset).format('YYYY-MM-DD HH:mm:ss')
                console.log("jde"+v)
                dataToSave.diagnosticTypeOfServiceId = payload.diagnosticTypeOfServiceKey;
                dataToSave.typeOfServiceId = payload.typeOfServiceKey;
                dataToSave.domainOfServiceId = payload.domainOfServiceKey;
                var t1 = moment(dataToSave.dates.scheduleDate).format("hh:mm:ss a")
                date = moment(dataToSave.dates.scheduleDate).format("YYYY-MM-DD")
                dataToSave.scheduleDate = date;
                var arr1 = t1.split(' ');
                var ampm = arr1[1];
                var arr2 = arr1[0].split(':');
                var hh = arr2[0], mm = arr2[1], ss = arr2[2];
                if(ampm == 'pm')
                    hh=12+parseInt(hh);
                time=hh+":"+mm+":"+ss;
                cb();
            },
            function(cb){
                var query = {
                    key: payload.typeOfServiceKey
                };
                var projection = {
                    name: 1
                };
                Service.service.getAllType(query, projection, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data && data.length > 0) {
                            type = data;
                        }
                        cb(null);
                    }
                });
            },
            function(cb){
                var query,projection;
                if(payload.domainOfServiceKey!=0) {
                    query = {
                        domainOfServiceId: {$in: [payload.domainOfServiceKey]},
                        userBlock: false,
                        isVerified: true,
                        //isApprovedByAdmin: true
                    };
                    projection = {
                        homeAddress: 1,
                        _id: 1
                    };
                    Service.doctor.getDoctor(query, projection, {}, function (err, data) {
                        if (err)
                            cb(err)
                        else {
                            if (data.length > 0) {
                                data1 = data;
                            }
                            cb(null);
                        }
                    });
                }
                else {
                    query = {
                        diagnosticTypeOfServiceId: {$in: [payload.diagnosticTypeOfServiceKey]},
                        userBlock: false,
                        isVerified: true, 
    //                   isApprovedByAdmin: true
                    };
                    projection = {
                        homeAddress: 1,
                        _id: 1
                    };
                    Service.diagnostic.getDiagnostic(query, projection, {}, function (err, data) {
                        if (err)
                            cb(err)
                        else {
                            if (data.length > 0){
                                data1 = data;
                            }
                            cb(null);
                        }
                    });
                }
            },
            function(cb) {
                var query, projection, Job = data1, c = 0, dist, flag = false, endTime, endTime1, nextTime = null, prevTime = null, schDate = new Date(dataToSave.dates.scheduleDate).toISOString(), lat, long, temp = {}, prev_lat, prev_long, next_lat, next_long, prevTime1;
                endTime = moment(schDate).add(1, 'hours');
                endTime = new Date(endTime).toISOString();
                var doctorAssigned = false;
                if (Job && Job.length != 0) {
                        var taskInImages = [];
                        for (var key in Job)
                        {
                            (function (key) {
                                taskInImages.push((function (key) {
                                    return function (embeddedCb) {
                                        if(payload.domainOfServiceKey)
                                        {
                                            query = {
                                                doctorId: Job[key]._id,
                                                scheduleDate: dataToSave.scheduleDate
                                            };
                                            projection = {
                                                dates: 1,
                                                doctorId: 1,
                                                address: 1,
                                                _id: 0
                                            };
                                        }
                                        else
                                        {
                                            query = {
                                                diagnosticId: Job[key]._id,
                                                scheduleDate: dataToSave.scheduleDate
                                            };
                                            projection = {
                                                dates: 1,
                                                diagnosticId: 1,
                                                address: 1,
                                                _id: 0
                                            };
                                        }
                                        Service.appointment.getAppointment(query, projection, {sort: {"dates.scheduleDate": 1}}, function (err, data) {
                                            console.log(data)
                                            if (err)
                                                embeddedCb(err);
                                            else {
                                                if (data && data.length > 0)
                                                {
                                                    var origins, destinations, prev_add, next_add, date1, secondsDiff1, secondsDiff2;
                                                    date1=null, endTime1=null, prevTime = null, prevTime1 = null, nextTime = null, prev_lat = null, prev_long = null, next_lat = null, next_long = null, flag = false, dist = null, temp= {};

                                                    for (var i = 0; i < data.length; i++)
                                                    {
                                                        date1 = new Date(data[i].dates.scheduleDate).toISOString()
                                                        console.log("date1 :"+date1)
                                                        endTime1 = moment(date1).add(1, 'hours');
                                                        endTime1 = new Date(endTime1).toISOString();
                                                        console.log("date1 = "+date1 +"\t"+"endTime1 = "+endTime1 +"\t"+"schDate = "+schDate+"\t"+"endtime = "+endTime)
                                                        if((schDate >=date1  && schDate <= endTime1) || (endTime==date1)) {
                                                            console.log("busy")
                                                            flag = true;
                                                            break;
                                                        }
                                                        else if(type=="video consultation"){
                                                            dataToSave.doctorId=Job[key]._id;
                                                            doctorAssigned=true
                                                            break;
                                                        }
                                                        else if (date1 > schDate) {
                                                            prev_lat = dataToSave.address.customerLocation.coordinates[1]
                                                            prev_long = dataToSave.address.customerLocation.coordinates[0]
                                                            prevTime = endTime;
                                                        }
                                                        else if (schDate > endTime1) {
                                                            prev_lat = data[i].address.customerLocation.coordinates[1]
                                                            prev_long = data[i].address.customerLocation.coordinates[0]
                                                            prevTime1 = endTime1;
                                                        }
                                                        else{
                                                            next_lat = data[i].address.customerLocation.coordinates[1]
                                                            next_long = data[i].address.customerLocation.coordinates[0]
                                                            nextTime = endTime1;
                                                            break;
                                                        }
                                                    }

                                                    console.log("prevTime: "+prevTime)
                                                    console.log("nextTime: "+nextTime)

                                                    if (flag != true && doctorAssigned!=true)
                                                    {
                                                        var endTimeComputed = moment(schDate, 'YYYY-MM-DD HH:mm:ss')
                                                        var schTimeComputed = moment(date1, 'YYYY-MM-DD HH:mm:ss')

                                                        if (prevTime) {
                                                            var prevTimeComputed = moment(prevTime, 'YYYY-MM-DD HH:mm:ss')
                                                            secondsDiff1 = Math.abs(schTimeComputed.diff(prevTimeComputed, 'seconds'))
                                                        }

                                                        if (nextTime) {
                                                            var nextTimeComputed = moment(nextTime, 'YYYY-MM-DD HH:mm:ss')
                                                            secondsDiff2 = Math.abs(endTimeComputed.diff(nextTimeComputed, 'seconds'))
                                                        }

                                                        if (prevTime1) {
                                                            var prevTimeComputed1 = moment(prevTime1, 'YYYY-MM-DD HH:mm:ss')
                                                            secondsDiff1 = Math.abs(endTimeComputed.diff(prevTimeComputed1, 'seconds'))
                                                        }

                                                        console.log("prevAddress Diff: "+secondsDiff1);
                                                        console.log("nextAddress Diff: "+secondsDiff2);

                                                        if (prevTime)
                                                            prev_add = prev_lat + ',' + prev_long;

                                                        if (nextTime)
                                                            next_add = next_lat + ',' + next_long;

                                                        if (prevTime1)
                                                            prev_add = prev_lat + ',' + prev_long;

                                                        console.log("prev_add "+prev_add)
                                                        console.log("next_add "+next_add)

                                                        var curr_add = payload.addLatitude + ',' + payload.addLongitude;
                                                        console.log("curr_add "+curr_add)

                                                        origins = [curr_add];

                                                        if (prevTime && nextTime)
                                                            destinations = [prev_add, next_add];
                                                        else if (prevTime || prevTime1)
                                                            destinations = [prev_add];
                                                        else
                                                            destinations = [next_add];

                                                        console.log("origins:  "+origins)
                                                        console.log("destinations "+destinations)

                                                        if (prevTime!=null || nextTime!=null || prevTime1!=null)
                                                        {
                                                            calculateDistanceViaGoogleDistanceMatrix(origins, destinations, function (err, data) {
                                                                console.log(">>>>>>>>",JSON.stringify(data.rows));
                                                                if(data.rows[0].elements[0].status=="ZERO_RESULTS")
                                                                    embeddedCb(null)
                                                                if(data.rows[0])
                                                                    console.log("google data prev eta time = "+data.rows[0].elements[0].duration.value)
                                                                if(data.rows[1])
                                                                    console.log("google data next eta time = "+data.rows[1].elements[0].duration.value)
                                                                if (!err) {
                                                                    if ((prevTime || prevTime1) && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff1 && nextTime && data.rows[1] && data.rows[1].elements[0].duration.value <= secondsDiff2) {
                                                                        dist = UniversalFunctions.getDistanceBetweenPoints({lat:prev_lat, long:prev_long}, {lat: payload.addLatitude, long: payload.addLongitude});
                                                                        temp = {
                                                                            id: Job[key]._id,
                                                                            long: prev_long,
                                                                            lat: prev_lat,
                                                                            distance: dist
                                                                        };
                                                                        console.log("prev_lat "+ payload.addLatitude + "prev_long " + prev_long)
                                                                        console.log("temp = "+temp)
                                                                        embeddedCb(null,temp);
                                                                    }
                                                                    else if ((prevTime || prevTime1) && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff1) {
                                                                        dist = UniversalFunctions.getDistanceBetweenPoints({lat:prev_lat, long:prev_long}, {lat: payload.addLatitude, long: payload.addLongitude});
                                                                        temp = {
                                                                            id: Job[key]._id,
                                                                            long: prev_long,
                                                                            lat: prev_lat,
                                                                            distance: dist
                                                                        };
                                                                        console.log("prev_lat "+ payload.addLatitude + "prev_long " + prev_long)
                                                                        console.log("temp distance = "+ dist)
                                                                        console.log("temp = "+temp)
                                                                        embeddedCb(null,temp);
                                                                    }
                                                                    else if (nextTime && data.rows[0] && data.rows[0].elements[0].duration.value <= secondsDiff2) {
                                                                        lat = dataToSave.address.customerLocation.coordinates[1]
                                                                        long = dataToSave.address.customerLocation.coordinates[0]
                                                                        dist = UniversalFunctions.getDistanceBetweenPoints({lat:lat, long:long}, {lat: payload.addLatitude, long: payload.addLongitude});
                                                                        temp = {
                                                                            id: Job[key]._id,
                                                                            long: long,
                                                                            lat: lat,
                                                                            distance: dist
                                                                        };
                                                                        console.log("lat "+ lat + "long " + long)
                                                                        console.log("temp distance = "+ dist)
                                                                        console.log("temp = "+temp)
                                                                        embeddedCb(null,temp);
                                                                    }
                                                                    else
                                                                        embeddedCb(null)
                                                                }
                                                            });
                                                        }
                                                    }
                                                    else
                                                        embeddedCb(null);
                                                }
                                                else {
                                                    for (i = 0; i < data1.length; i++)
                                                    {
                                                        if (data1[i]._id == Job[key]._id)
                                                        {
                                                            long = data1[i].homeAddress.customerLocation.coordinates[0];
                                                            lat = data1[i].homeAddress.customerLocation.coordinates[1];
                                                            break;
                                                        }
                                                    }
                                                    dist = UniversalFunctions.getDistanceBetweenPoints({lat:lat, long:long}, {lat: payload.addLatitude, long: payload.addLongitude});
                                                    temp = {
                                                        id: Job[key]._id,
                                                        long: long,
                                                        lat: lat,
                                                        distance: dist*1000
                                                    };
                                                    console.log("temp distance = "+temp.distance)
                                                    if(dist<=20000)
                                                        embeddedCb(null,temp);
                                                    else
                                                        embeddedCb(null);
                                                }
                                            }
                                        });
                                    }
                                })(key))
                            }(key));
                        }
                        async.parallel(taskInImages, function (err, result) {
                            data2=result.slice()
                            var i, j, l = data2.length, k = {};
                            if (l > 0 && data2) {
                                for (i = 1; i < l && data2[i]; i++)
                                {
                                    k = data2[i];
                                    j = i - 1;
                                    while (data2[j] && j >= 0 && data2[j].distance > k.distance) {
                                        data2[j + 1] = data2[j];
                                        j = j - 1;
                                    }
                                    data2[j + 1] = k;
                                }
                                for (i = 0; i < l; i++)
                                {
                                    if (data2[i] && data2[i].id && data2[i].distance <=20000)
                                    {
                                        if(payload.domainOfServiceKey!=0)
                                            dataToSave.doctorId = data2[i].id;
                                        else
                                            dataToSave.diagnosticId = data2[i].id;
                                        dataToSave.applicationStatus = CONST.APPLICATION_STATUS.CONFIRMED;
                                        break;
                                    }
                                }
                                console.log(data2)
                                if (dataToSave.doctorId || dataToSave.diagnosticId)
                                    cb(null);
                                else {
                                    if(payload.domainOfServiceKey!=0)
                                        cb(ERROR.DOCTOR_NOT_AVAILABLE);
                                    else
                                        cb(ERROR.DIAGNOSTICIAN_NOT_AVAILABLE);
                                }
                            }
                            else {
                                if(payload.domainOfServiceKey!=0)
                                    cb(ERROR.DOCTOR_NOT_AVAILABLE);
                                else
                                    cb(ERROR.DIAGNOSTICIAN_NOT_AVAILABLE);
                            }
                        });
                }
                else {
                    if(payload.domainOfServiceKey!=0)
                        cb(ERROR.DOCTOR_NOT_AVAILABLE);
                    else
                        cb(ERROR.DIAGNOSTICIAN_NOT_AVAILABLE);
                }
            },
            function(cb){
                Service.appointment.createAppointment(dataToSave, function (err, appointmentDataFromDB) {
                    if (err) {
                        cb(err);
                    } else {
                        appointmentData = appointmentDataFromDB;
                        cb();
                    }
                });
            },
            function(cb){
                var query = null
                if(payload.domainOfServiceKey!=0) {
                    query = {
                        _id: dataToSave.doctorId
                    }
                    Service.doctor.getDoctor(query, {
                        firstName: 1,
                        lastName: 1
                    }, {}, function (err, appointmentDataFromDB) {
                        if (err) {
                            cb(err);
                        } else {
                            console.log(appointmentDataFromDB);
                            dataToSave.spAssigned = appointmentDataFromDB[0].firstName+" "+appointmentDataFromDB[0].lastName;
                            cb();
                        }
                    });
                }
                else {
                    query = {
                        _id: dataToSave.diagnosticId
                    }
                    Service.diagnostic.getDiagnostic(query, {
                        firstName: 1,
                        lastName: 1
                    }, {}, function (err, appointmentDataFromDB) {
                        if (err) {
                            cb(err);
                        } else {
                            dataToSave.spAssigned = appointmentDataFromDB[0].firstName+" "+appointmentDataFromDB[0].lastName;
                            cb();
                        }
                    });
                }
            },
        ],
        function (error, results) {
            if (!error)
                callback(null,dataToSave);
            else {
                callback(error);
            }
        })
};

var getETA = function(origin,destination,callback) {
    var distanceBwPickupDel, pickupDeliveryTotalDistance;
    async.series([
        function (cb) {
            distance.get(
                {
                    origin: origin,
                    destination: destination
                },
                function (err, data) {
                    console.log("google data - >  ", data);
                    if (data != undefined) {
                        distanceBwPickupDel = data.durationValue;
                        pickupDeliveryTotalDistance = data.distanceValue;
                    }
                    cb()
                });
        }
    ], function (err, response) {
        if (err)
            callback(err)
        else {
            callback(null, {ETA: distanceBwPickupDel, Distance: pickupDeliveryTotalDistance})
        }
    });
};

var calculateDistanceViaGoogleDistanceMatrix = function (origins, destinations, callback) {
    distance.matrix(origins, destinations, function (err, distances) {
        if (!err) {
            callback(null, distances);
        }
    });
};

var getOffsetViaLatLong = function (long, lat, callback) {
    var rawOffset = 0;
    /* Request timezone with location coordinates */
    timezoner.getTimeZone(
        lat, // Latitude coordinate
        long, // Longitude coordinate
        function (err, data) {
            if (err) {
                callback(err)
            } else {
                console.log('data', data)
                rawOffset = data.rawOffset;
                rawOffset = rawOffset + data.dstOffset;
                callback(null, {rawOffset: rawOffset})
            }
        }, {language: 'es', key: Config.serverConfig.GOOGLE_API_KEY}
    );
};

var viewAppointment = function (tokenData, payload, callback) {
    var userdata = null;
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })

            },
            function(cb){
                var query = {
                    _id: payload.appointmentId
                };
                var projection = {
                    doctorNotes: 0
                };
                Service.appointment.getAppointment(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        console.log(data)
                        if(data.length == 0)
                            cb(ERROR.INVALID_APPOINTMENT)
                        else{
                            userdata = data;
                            cb(null)
                        }
                    }
                })
            }],
        function(error,result){
            if(error)
                callback(error)
            else
                callback(null,userdata)

        }
    );
};

var appointmentHistory = function (tokenData, payload, callback) {
    var userdata = {};
    userdata.appointmentData=null
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var query = {
                    patientId: tokenData.id
                };
                var projection = {doctorNotes: 0};
                Service.appointment.getAppointment(query, projection, {sort: {"dates.scheduleDate": -1}}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.NO_BOOKINGS)
                        else{
                            userdata.appointmentData=data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var Jobimages = userdata.appointmentData;
                userdata.SP=null;
                if(Jobimages && Jobimages.length!=0){
                        var taskInImages = [];
                        for (var key in Jobimages) {
                            (function (key) {
                                taskInImages.push((function (key) {
                                    return function (embeddedCb) {
                                        if(Jobimages[key].doctorId!=null) {
                                            var query = {
                                                _id: Jobimages[key].doctorId
                                            };
                                            var projection = {
                                                firstName: 1,
                                                lastName: 1,
                                                email: 1,
                                                mobileNo: 1,
                                                countryCode: 1,
                                                profilePicURL: 1
                                            };
                                            Service.doctor.getDoctor(query, projection, {}, function (err, data) {
                                                if (err)
                                                    cb(err)
                                                else {
                                                    if (data.length == 0)
                                                        cb(ERROR.NO_BOOKINGS)
                                                    else {
                                                        embeddedCb(null,data[0])
                                                    }
                                                }
                                            })
                                        }
                                        else {
                                            var query = {
                                                _id: Jobimages[key].diagnosticId
                                            };
                                            var projection = {
                                                firstName: 1,
                                                lastName: 1,
                                                email: 1,
                                                mobileNo: 1,
                                                countryCode: 1,
                                                profilePicURL: 1
                                            };
                                            Service.diagnostic.getDiagnostic(query, projection, {}, function (err, data) {
                                                if (err)
                                                    cb(err)
                                                else {
                                                    if (data.length == 0)
                                                        embeddedCb(ERROR.NO_BOOKINGS)
                                                    else {
                                                        embeddedCb(null,data[0])
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })(key))
                            }(key));
                        }
                        async.parallel(taskInImages, function (err, result) {
                            userdata.SP=result;
                            cb(null);
                        });
                }
                else
                    cb(null)
            },
            function(cb){
                var query = {};
                var projection = {};
                userdata.domainOfService = null
                Service.service.getAllDomain(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.DOMAIN_NOT_EXIST)
                        else{
                            userdata.domainOfService=data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var query = {};
                var projection = {};
                userdata.typeOfService = null
                Service.service.getAllType(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.TYPE_NOT_EXIST)
                        else{
                            userdata.typeOfService=data;
                            cb(null)
                        }
                    }
                })
            },
        ],

        function(error,result){
            if(error)
                callback(error)
            else {
                console.log(userdata)
                var outputData = {};
                outputData.upcoming={};
                outputData.past={}
                outputData.upcoming.sp = [], outputData.upcoming.appointmentData = [], outputData.past.sp = [],outputData.past.appointmentData = []
                for(var i=0;i<userdata.appointmentData.length;i++) {
                    if (userdata.appointmentData[i].applicationStatus == CONST.APPLICATION_STATUS.COMPLETED || userdata.appointmentData[i].applicationStatus == CONST.APPLICATION_STATUS.CANCELLED)
                    {
                        outputData.past.sp.push(userdata.SP[i]);
                        outputData.past.appointmentData.push(userdata.appointmentData[i]);
                    }
                    else
                    {
                        outputData.upcoming.sp.push(userdata.SP[i]);
                        outputData.upcoming.appointmentData.push(userdata.appointmentData[i]);
                    }
                }
                outputData.domainOfService = userdata.domainOfService
                outputData.typeOfService = userdata.typeOfService
                callback(null, outputData)
            }
        }
    );
};

var submitFeedback = function (tokenData, payload, callback) {
    var userdata = null
    async.series([
            function (cb)
            {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data
                            cb(null)
                        }
                    }
                })
            },
            function(cb){

                var criteria = {
                    patientId: tokenData.id,
                    _id: payload.appointmentId
                };

                var dataToUpdate = {
                    "feedbackPatient.timestamp": Date.now(),
                    "feedbackPatient.feedback": payload.feedback,
                    "feedbackPatient.rating": payload.rating
                };
                Service.appointment.updateAppointment(criteria, dataToUpdate, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(err);
                        else{
                            cb(null)
                        }
                    }
                })
            }
        ],

        function(error,result){
            if(error)
                callback(error)
            else{
                callback(null)
            }
        }
    );
};

var buySubscription = function (tokenData, payload, callback) {
    var userdata = null, dataFromDB= null, timezoneOffset=null, subscriptionDate=payload.currentTime;
    async.series([
            function (cb)
            {
                var query = {
                    _id: tokenData.id
                };
                Service.patient.getPatient(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data
                            cb(null)
                        }
                    }
                })
            },
            function (cb)
            {
                var query = {
                    _id: payload.subscriptionId,
                    isActive: true
                };
                Service.subscription.getSubscription(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (!data && data.length == 0)
                            cb(ERROR.SUBSCRIPTION_EXPIRED)
                        else {
                            dataFromDB = data[0]
                            cb(null)
                        }
                    }
                })
            },
            function (cb) {
                var query = {_id: tokenData.id};
                Service.patient.getPatient(query, { _id: 0, "subscription":1  } , {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.ADDRESS_NOT_FOUND);
                        else {
                            var c=0;
                            for(var i=0;i<data[0].subscription.length;i++)
                            {
                                if(String(data[0].subscription[i].id)===String(payload.subscriptionId) && data[0].subscription[i].count != 0)
                                    cb(ERROR.SUBSCRIPTION_ALREADY_BOUGHT)
                            }
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var dataToUpdate = {}
                var criteria = {
                    _id: tokenData.id
                };

                var expireDate = new Date(subscriptionDate);
                expireDate.setMonth(expireDate.getMonth() + dataFromDB.duration);
                dataToUpdate.subscription = {
                    id : payload.subscriptionId,
                    count : dataFromDB.numberOfServices,
                    subscribedOn: subscriptionDate,
                    expireDate: expireDate
                };
                Service.patient.updatePatient(criteria, {$push: dataToUpdate}, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(err);
                        else{
                            cb(null)
                        }
                    }
                })
            }
        ],

        function(error,result){
            if(error)
                callback(error)
            else{
                callback(null)
            }
        }
    );
};


//Get Bought subscription
var getBoughtSubscription = function (tokenData, callback) {
    var userdata = null;
    async.series([
        function(cb){
            var query = {
                _id: tokenData.id
            }
            Service.patient.getPatient(query, {subscription: 1}, {}, function (err, data) {
                if (data.length > 0) {
                    userdata = data;
                    cb(null);
                }
                else {
                    cb(ERROR.NO_SUBSCRIPTION)
                }
            });
        }
    ], function (error, results) {
        if (!error)
            callback(null,userdata);
        else {
            callback(error);
        }
    })
};

//Applying PromoCode
var applyPromoCode = function (tokenData, payload, callback) {
    var userdata = null;
    var returnData = {};
    async.series([
        function(cb){
            var query = {
                promoCode: payload.promoCode
            }
                Service.promoCode.getPromoCode(query, {}, {}, function (err, data) {
                    if (data) {
                        userdata = data[0];
                        cb(null);
                    }
                    else {
                        cb(ERROR.PROMOCODE_INVALID);
                    }
                });
        },
        function (cb) {
            if(userdata.type==UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.PROMO_TYPE.PERCENT){
                payload.price =(userdata.value * payload.price)/100;
                returnData.discountedPrice = payload.price;
            }
            else{
                payload.price = ((payload.price - userdata.value)<0)?payload.price:(payload.price - userdata.value)
                returnData.discountedPrice = payload.price;
            }
            cb(null)
        },
        function (cb) {
            var query = {
                promoCode: payload.promoCode
            }
            if(userdata.usageCount != userdata.maxUsageCountLimit) {
            Service.promoCode.updatePromoCode(query, {$set: {usageCount: userdata.usageCount + 1}}, {}, function (err, data) {
                if (data) {
                    userdata = data[0];
                    cb(null);
                }
                else{
                    cb(err);
                }
            });
            }
            else
                cb(ERROR.PROMOCODE_MAXUSAGE);
        }
    ], function (error, results) {
        if (!error)
            callback(null,returnData);
        else {
            callback(error);
        }
    })
};


var cancelAppointment = function (tokenData, payload, callback) {
    var dataToSave = [];
    var userdata = null;
    var appointmentData = {};
    var timezoneOffset = null;
    var data1 = null;
    var data2 = [], date = null, time = null,kk=null, data3 = null;
    async.series([
        function (cb) {
            var query = {
                _id: tokenData.id
            };
            Service.patient.getPatient(query, {}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0)
                        cb(ERROR.INCORRECT_ACCESSTOKEN)
                    else {
                        cb(null)
                    }
                }
            })

        },
        function (cb) {
            var query = {
                _id: payload.appointmentId
            };
            Service.appointment.getAppointment(query, {}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    if (data.length == 0) {
                        cb(ERROR.INVALID_APPOINTMENT)
                    }
                    else {
                        userdata =data[0]
                        console.log(userdata)
                        cb(null)
                    }
                }
            });
        },
        function (cb) {
            var query = {
                _id: payload.appointmentId
            };
            Service.appointment.updateAppointment(query, {$set: {applicationStatus: Config.constants.APPLICATION_STATUS.CANCELLED}}, {}, function (err, data) {
                if (err)
                    cb(err)
                else {
                    cb(null)
                }
            });
        },
        function(cb){
                var query ={
                    _id: userdata.patientId
                }
                Service.patient.getPatient(query, {_id: 0}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        data3 = data[0];
                        console.log(">>>>>>>>"+data)
                        cb(null)
                    }
                });
        },
        function(cb){
            var query ={
                _id: userdata.patientId
            }
            console.log(">>>>>>>>eee"+JSON.stringify(data3))
            var credits = parseInt(data3.walletCredits + userdata.payment.totalPrice - userdata.payment.discount - Config.constants.CREDITS_DEDUCTED_ON_CANCELLATION.AMOUNT)
            var dataToUpdate = {
                walletCredits: credits
            }
            Service.patient.updatePatient(query, {$set: dataToUpdate}, {}, function (err, data) {
                if (err)
                    cb(err)
                else
                    cb(null)
            });
        }
    ],
        function(error, result){
            if(error)
                callback(error)
            else
                callback(null)
        })
}

module.exports = {
    bookAppointment: bookAppointment,
    viewAppointment: viewAppointment,
    appointmentHistory: appointmentHistory,
    submitFeedback: submitFeedback,
    buySubscription: buySubscription,
    getBoughtSubscription: getBoughtSubscription,
    applyPromoCode: applyPromoCode,
    cancelAppointment: cancelAppointment
};
