'use strict';

var async = require('async');
var moment = require('moment');
var paypal = require('paypal-rest-sdk');

var Service = require('../../Services');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var CONFIG = require('../../Config');

var UploadManager = require('../../lib/UploadManager');
var TokenManager = require('../../lib/TokenManager');
//var NotificationManager = require('../../lib/NotificationManager');
var CodeGenerator = require('../../lib/CodeGenerator');

var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var SUCCESS = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS;

//var client = require('twilio')(UniversalFunctions.CONFIG.SMS_CONFIG.twilioCredentials.accountSid, UniversalFunctions.CONFIG.SMS_CONFIG.twilioCredentials.authToken);

//paypal-rest-sdk configuration

var paypalconfig = {

    "mode":"sandbox",    //sandbox or live
    "client_id":"AenTJlv4gTsxsw5jUx3veUlQYCFz7vDukiAsQxM4plRjpAF4uSPNiF6BFCYTNPYU2pnVBCoJg4T0cCHE",
    "client_secret":"EEQ_V-8NzD3NUPU3hnP1TBv_lMfIe8D0niQ8qrmfopPdOu8xfq6hbCDtrh53zVbf-DiNArhPrU4JukLJ",
    "headers":{
        "custom":"header"
    }
};

paypal.configure(paypalconfig);

var getAllCards = function(tokenData,callback){
    var temp_paymentCard = [];
    async.series([
        function(cb){
            var query = {
                _id : tokenData.id
            };
            var projection = {
                creditCardDetails :1
            };
            var options = {lean:true};
            Service.patient.getPatient(query, projection, options, function(err,user){
                if(err)
                    callback(ERROR.INCORRECT_ACCESSTOKEN);
                else{
                    if(user && user.length > 0){
                        for(var i=0; i < user[0].creditCardDetails.length; i++){
                            if(user[0].creditCardDetails[i].isActive==true)
                                temp_paymentCard.push(user[0].creditCardDetails[i]);
                        }
                        cb(null);
                    }
                    else
                        cb(err)
                }
            });
        }
    ],function(err,result){
        if(err)
            return callback(err);
        else {
            console.log(temp_paymentCard)
            return callback(null,  temp_paymentCard);
        }
    });
};

var addPaymentCard = function(tokenData, data, callback){
    var cardDetails= {}, cardId = null;

    async.waterfall([
        function(cb){
            var query = {
                _id :tokenData.id
            };
            var projection = {};
            var options = {lean:true};
            Service.patient.getPatient(query,projection,options,function(err,result){

                if(err)
                    cb(err);
                else{
                    if(result && result.length >0){
                        cb()
                    }
                    else
                        cb(ERROR.INCORRECT_ACCESSTOKEN);
                }
            });
        },
        function(cb){
            var cardToBeSaved = {
                "type"   : data.type,
                "number" : data.number,
                "expire_month" : data.expire_month,
                "expire_year"  : data.expire_year,
                "cvv2" : data.cvv2,
                "first_name" : data.first_name,
                "last_name" : data.last_name,
                "payer_id" : tokenData.id
            };

            //paypal rest api call to save credit card on their server
            paypal.creditCard.create(cardToBeSaved,function(error,credit_card){
                if(error)
                    cb(error);
                else{
                  // storing only last three numbers
                    if(credit_card.number) {
                        var str='x';
                        for (var i = credit_card.number.length - 5; i >= 0; i--) {
                            str= str+'x';
                        }
                    }
                    var lastthree = credit_card.number.substr(credit_card.number.length-3);
                    str=str+lastthree;
                    cardDetails.cardNumber = str;
                    cardDetails.cardType = credit_card.type;
                    cardDetails.cardToken = credit_card.id;
                    cardDetails.isActive = true;
                    cardDetails.isDefault = false;
                    var options = {};
                    console.log(cardDetails);
                    var query = {
                        _id: tokenData.id
                    }
                    Service.patient.updatePatient(query, {$push: {creditCardDetails:cardDetails}}, {}, function(err,res){
                        if(err)
                            cb(err);
                        else {
                            console.log(res);
                            cb();
                        }
                    });

                }
            });
        }
    ],
    function(err,result) {
    callback(err,result);
    });
};

var makePayment = function(tokenData, data, callback){

    console.log("Card",data.cardToken);
    console.log("payer_id",tokenData.id);

    var saved_card = {
        "intent": "sale",
        "payer": {
            "payment_method": "credit_card",
            "funding_instruments": [{
                "credit_card_token": {
                    "credit_card_id": data.cardToken,
                    "payer_id":tokenData.id
                }
            }]
        },
        "transactions": [{
            "amount": {
                "currency": "USD",
                "total": "1.00"
            },
            "description": "This is the payment description."
        }]
    };

    paypal.payment.create(saved_card, function (error, payment) {
        if (error) {
            console.log("error",error);
            return callback(error);
        } else {
            console.log("Create Payment Response");
            console.log(payment);
            return callback();
        }
    });
};

var getPaymentCardDetails = function(data,tokenData,callback){

    var data;

    async.series([
        function(cb){

            var query = {
                _id : tokenData.id
            };

            var projection = {
                creditCardDetails :1
            };

            var options = {
                lean:true
            };

            Service.patient.getPatient(query,projection,options,function(err,res){
                if(err)
                    cb(err);
                else{
                    var card_token = data.cardToken;
                    paypal.creditCard.get(card_token, function (error, credit_card) {
                        if (error) {
                            throw error;
                        } else {
                            data = credit_card;
                            cb();
                        }
                    });
                }
            });
        }
    ], function(err,results)
    {
        if(err)
            return callback(err);
        else
            return callback(null,{data : data});
    });
};

var deleteCard = function(data,tokenData,callback){
    async.series([
        function(cb) {
            var query = {
                _id : tokenData.id
            };

            var projection = {
                creditCardDetails :1
            };

            var options = {
                lean:true
            };
            var flag=true;
            Service.patient.getPatient(query,projection,options,function(err,res){
                if(err)
                    cb(err);
                else {
                    if (res.length > 0) {
                        for(var i=0;i<res[0].creditCardDetails.length;i++)
                        {
                            if(res[0].creditCardDetails[i].cardToken==data.cardToken && res[0].creditCardDetails[i].isDefault==true){
                                flag=false;
                                break;
                            }
                        }
                        if(flag==false)
                            cb(ERROR.DEFAULTCARD_NOT_DELETE)
                        else
                            cb();
                    }
                    else
                        cb(ERROR.INCORRECT_ACCESSTOKEN);
                }
            });
        },
        function(cb){
            var card_token = data.cardToken;
            paypal.creditCard.del(card_token, function (error, credit_card) {
                if (error) {
                    throw error;
                } else {
                    console.log("card deleted");
                    cb();
                }
            });
        },
        function(cb){
            var query = {
                _id : tokenData.id
            };

            var dataToUpdate = {
             $pull: { creditCardDetails : { cardToken: data.cardToken } }
        };

            var options = {
                lean:true
            };

            Service.patient.updatePatient(query,dataToUpdate,options,function(err,res){
                if(err)
                    cb(err);
                else
                    cb();
            });
        }
    ],function(err,results){
        if(err)
            return callback(err);
        else
            return callback(null);
    });

};

var setCardDefault = function(tokenData, data, callback){
    var dataFromDB = [];
    async.series([
        function(cb) {
            var query = {
                _id : tokenData.id
            };

            var projection = {
                creditCardDetails :1
            };

            var options = {
                lean:true
            };

            Service.patient.getPatient(query,projection,options,function(err,user){
                if(err)
                    cb(ERROR.INCORRECT_ACCESSTOKEN);
                else {
                        if(user && user.length > 0){
                            for(var i=0; i < user[0].creditCardDetails.length; i++)
                            {
                                if(user[0].creditCardDetails[i].cardToken==data.cardToken){
                                    var dat = {};
                                    dat = {
                                        cardNumber : user[0].creditCardDetails[i].number,
                                        cardType : user[0].creditCardDetails[i].cardType,
                                        cardToken : user[0].creditCardDetails[i].cardToken,
                                        isDefault : true,
                                        isActive : user[0].creditCardDetails[i].isActive
                                    };
                                    dataFromDB.push(dat);
                                }
                                else if(user[0].creditCardDetails[i].isDefault == false)
                                           dataFromDB.push(user[0].creditCardDetails[i]);
                                else
                                {
                                    var dat = {};
                                    dat = {
                                        cardNumber : user[0].creditCardDetails[i].number,
                                        cardType : user[0].creditCardDetails[i].cardType,
                                        cardToken : user[0].creditCardDetails[i].cardToken,
                                        isDefault : false,
                                        isActive : user[0].creditCardDetails[i].isActive
                                    };
                                    dataFromDB.push(dat);
                                }
                            }
                            cb(null);
                        }
                    }
                });
            },
        function(cb){
            var query = {
                _id : tokenData.id
            };

            var dataToUpdate = {
                $set: { 'creditCardDetails': dataFromDB }
            };

            var options = {
                lean:true
            };

            Service.patient.updatePatient(query,dataToUpdate,options,function(err,res){
                if(err)
                    cb(err);
                else
                    cb();
            });
        }
    ],function(err,results){
        if(err)
            return callback(err);
        else
            return callback(null);
    });

};

module.exports = {
    addPaymentCard : addPaymentCard,
    getAllCards : getAllCards,
    makePayment : makePayment,
    getPaymentCardDetails : getPaymentCardDetails,
    deleteCard : deleteCard,
    setCardDefault: setCardDefault
};