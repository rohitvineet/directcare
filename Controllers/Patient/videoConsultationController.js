'use strict';

var async = require('async');
var moment = require('moment');
var Service = require('../../Services');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var CONFIG = require('../../Config');

var UploadManager = require('../../lib/UploadManager');
var TokenManager = require('../../lib/TokenManager');
var CodeGenerator = require('../../lib/CodeGenerator');

var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var SUCCESS = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS;
var OpenTok = require('opentok'),
    opentok = new OpenTok(CONFIG.serverConfig.OPENTOK_KEY, CONFIG.serverConfig.OPENTOK_SECRET);

var publishVideoConsultation = function(tokenData, payload, callback){
    var userdata = {};
    var sessionId = null, token = null;
    async.series([
        function(cb)
        {
            opentok.createSession({mediaMode:"routed"},function(err, session) {
                if (err)
                    return console.log(err);

                // save the sessionId
                sessionId = session.sessionId;
                userdata.sessionId = sessionId;
                console.log(session)
                cb();
            });
        },
        function(cb)
        {
            token = opentok.generateToken(sessionId);

            userdata.token = token;
            userdata.apiKey = CONFIG.serverConfig.OPENTOK_KEY;
            console.log(token)
            cb();
        },
        function(cb)
        {
            console.log(payload.appointmentId)
            Service.appointment.updateAppointment({_id: payload.appointmentId, patientId: tokenData.id}, {$set: {videoConsultationSessionId: sessionId}}, {}, function (err, dataFromDB) {
                if (err)
                    cb(err)
                else {
                    if(!dataFromDB)
                        cb(ERROR.NO_BOOKINGS)
                    else
                        cb(null);
                }
            })
        }
    ],
    function(error,results){
        if(error)
            callback(error)
        else
            callback(null,userdata);
    })
};



module.exports = {
    publishVideoConsultation : publishVideoConsultation
};