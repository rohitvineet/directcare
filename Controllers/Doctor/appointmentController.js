'use strict';

var Controller = require('../../Controllers');
var UniversalFunctions = require('../../utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../../Config');
var async = require('async');
var Service = require('../../Services');
var ERROR = UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR;
var TokenManager = require('../../lib/TokenManager');
var UploadManager = require('../../lib/UploadManager');
var moment = require('moment')
var timezoner = require('timezoner');
const CONST = require('../../Config/constants');
var CONFIG = require('../../Config');
var OpenTok = require('opentok'),
    opentok = new OpenTok(CONFIG.serverConfig.OPENTOK_KEY, CONFIG.serverConfig.OPENTOK_SECRET);
var sessionId = null, token = null;

var viewAppointment = function (tokenData, payload, callback) {
    var userdata = null;
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.doctor.getDoctor(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })

            },
            function(cb){
                var query = {
                    _id: payload.appointmentId
                };
                var projection = {
                };
                Service.appointment.getAppointment(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        console.log(data)
                        if(data.length == 0)
                            cb(ERROR.INVALID_APPOINTMENT)
                        else{
                            userdata = data;
                            cb(null)
                        }
                    }
                })
            }],
        function(error,result){
            if(error)
                callback(error)
            else
                callback(null,userdata)

        }
    );
};

var appointmentHistory = function (tokenData, payload, callback) {
    var userdata = {};
    userdata.appointmentData=null
    async.series([
            function (cb) {
                var query = {
                    _id: tokenData.id
                };
                Service.doctor.getDoctor(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var query = {
                    doctorId: tokenData.id
                };
                var projection = {};
                Service.appointment.getAppointment(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.NO_BOOKINGS)
                        else{
                            userdata.appointmentData=data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var Jobimages = userdata.appointmentData;
                userdata.patient=null;
                if(Jobimages && Jobimages.length!=0){
                    var taskInImages = [];
                    for (var key in Jobimages) {
                        (function (key) {
                            taskInImages.push((function (key) {
                                return function (embeddedCb) {
                                        var query = {
                                            _id: Jobimages[key].patientId
                                        };
                                        var projection = {
                                            firstName: 1,
                                            lastName: 1,
                                            email: 1,
                                            mobileNo: 1,
                                            countryCode: 1,
                                            profilePicURL: 1
                                        };
                                        Service.patient.getPatient(query, projection, {sort: {"dates.scheduleDate": -1}}, function (err, data) {
                                            if (err)
                                                cb(err)
                                            else {
                                                if (data.length == 0)
                                                    embeddedCb(ERROR.NO_BOOKINGS)
                                                else {
                                                    embeddedCb(null,data[0])
                                                }
                                            }
                                        })
                                }
                            })(key))
                        }(key));
                    }
                    async.parallel(taskInImages, function (err, result) {
                        userdata.patient=result;
                        cb(null);
                    });
                }
                else
                    cb(null)
            },
            function(cb){
                var query = {};
                var projection = {};
                userdata.domainOfService = null
                Service.service.getAllDomain(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.DOMAIN_NOT_EXIST)
                        else{
                            userdata.domainOfService=data;
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var query = {};
                var projection = {};
                userdata.typeOfService = null
                Service.service.getAllType(query, projection, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(data.length == 0)
                            cb(ERROR.TYPE_NOT_EXIST)
                        else{
                            userdata.typeOfService=data;
                            cb(null)
                        }
                    }
                })
            },
        ],

        function(error,result){
            if(error)
                callback(error)
            else {
                console.log(userdata)
                var outputData = {};
                outputData.upcoming={};
                outputData.past={}
                outputData.upcoming.patient = [], outputData.upcoming.appointmentData = [], outputData.past.patient = [],outputData.past.appointmentData = []
                for(var i=0;i<userdata.appointmentData.length;i++) {
                    if (userdata.appointmentData[i].applicationStatus == CONST.APPLICATION_STATUS.COMPLETED || userdata.appointmentData[i].applicationStatus == CONST.APPLICATION_STATUS.CANCELLED)
                    {
                        outputData.past.patient.push(userdata.patient[i]);
                        outputData.past.appointmentData.push(userdata.appointmentData[i]);
                    }
                    else
                    {
                        outputData.upcoming.patient.push(userdata.patient[i]);
                        outputData.upcoming.appointmentData.push(userdata.appointmentData[i]);
                    }
                }
                outputData.domainOfService = userdata.domainOfService
                outputData.typeOfService = userdata.typeOfService
                callback(null, outputData)
            }
        }
    );
};

var startAppointment = function (tokenData, payload, callback) {
    var userdata = new Array();
    async.series([
            function (cb)
            {
                var query = {
                    _id: tokenData.id
                };
                Service.doctor.getDoctor(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var criteria = {
                    doctorId: tokenData.id,
                    _id: payload.appointmentId
                };
                
                var dataToUpdate = {
                    "dates.startDate": Date.now(),
                    applicationStatus: CONST.APPLICATION_STATUS.STARTED
                };
                Service.appointment.updateAppointment(criteria, dataToUpdate, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(ERROR.NO_BOOKINGS);
                        else{
                            cb(null)
                        }
                    }
                })
            }
        ],

        function(error,result){
            if(error)
                callback(error)
            else{
                callback(null)
            }
        }
    );
};

var endAppointment = function (tokenData, payload, callback) {
    var userdata = new Array();
    async.series([
            function (cb)
            {
                var query = {
                    _id: tokenData.id
                };
                Service.doctor.getDoctor(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                var criteria = {
                    doctorId: tokenData.id,
                    _id: payload.appointmentId
                };
                var dataToUpdate = {
                    "dates.endDate": Date.now(),
                    applicationStatus: CONST.APPLICATION_STATUS.COMPLETED
                };
                Service.appointment.updateAppointment(criteria, dataToUpdate, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(ERROR.NO_BOOKINGS);
                        else{
                            cb(null)
                        }
                    }
                })
            }
        ],

        function(error,result){
            if(error)
                callback(error)
            else{
                callback(null)
            }
        }
    );
};

var submitFeedback = function (tokenData, payload, callback) {
    var userdata = null
    async.series([
            function (cb)
            {
                var query = {
                    _id: tokenData.id
                };
                Service.doctor.getDoctor(query, {}, {}, function (err, data) {
                    if (err)
                        cb(err)
                    else {
                        if (data.length == 0)
                            cb(ERROR.INCORRECT_ACCESSTOKEN)
                        else {
                            userdata = data
                            cb(null)
                        }
                    }
                })
            },
            function(cb){
                
                var criteria = {
                    doctorId: tokenData.id,
                    _id: payload.appointmentId
                };
                
                var dataToUpdate = {
                    "feedbackDoctor.timestamp": Date.now(),
                    "feedbackDoctor.feedback": payload.feedback,
                    "feedbackDoctor.rating": payload.rating,
                };
                Service.appointment.updateAppointment(criteria, dataToUpdate, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(err);
                        else{
                            cb(null)
                        }
                    }
                })
            },
            function(cb){

                var criteria = {
                    _id: tokenData.id
                };

                var rating = userdata[0].rating!=0 ? (userdata[0].rating+payload.rating)/2 : payload.rating;

                var dataToUpdate = {
                    rating: rating
                };
                Service.doctor.updateDoctor(criteria, dataToUpdate, {}, function(err, data){
                    if(err)
                        cb(err)
                    else{
                        if(!data || data.length == 0)
                            cb(err);
                        else{
                            cb(null)
                        }
                    }
                })
            }
        ],

        function(error,result){
            if(error)
                callback(error)
            else{
                callback(null)
            }
        }
    );
};

var subscribeVideoConsultation = function(tokenData, payload, callback){
    var userdata = {}
    async.series([
            function(cb)
            {
                console.log(payload.appointmentId)
                Service.appointment.getAppointment({_id: payload.appointmentId, doctorId: tokenData.id}, {videoConsultationSessionId: 1}, {}, function (err, dataFromDB) {
                    if (err)
                        cb(err)
                    else {
                        if (!dataFromDB)
                            cb(ERROR.NO_BOOKINGS);
                        else {
                            userdata.sessionId  = dataFromDB[0].videoConsultationSessionId;
                            cb(null);
                        }
                    }
                })
            },
            function(cb)
            {
                console.log(userdata.sessionId)
                token = opentok.generateToken(userdata.sessionId);
                userdata.token = token;
                console.log(token)
                cb();
            },

        ],
        function(error,results){
            if(error)
                callback(error)
            else
                callback(null,userdata);
        })
};

module.exports = {
    viewAppointment: viewAppointment,
    appointmentHistory: appointmentHistory,
    startAppointment: startAppointment,
    endAppointment: endAppointment,
    submitFeedback: submitFeedback,
    subscribeVideoConsultation: subscribeVideoConsultation
};
