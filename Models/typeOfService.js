'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const typeOfServiceSchema = new Schema({
    key: {
		type: Number,
		unique: true
	},
	name: {
    	type: String,
        required: true
    }
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

typeOfServiceSchema.plugin(autoIncrement.plugin, {
	model: 'typeOfService',
	field: 'key',
	startAt: 123,
	incrementBy: 1
});

module.exports = mongoose.model('typeOfService',typeOfServiceSchema,'typeOfService');
