'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const subscriptionSchema = new Schema({

	name: {
		type: String,
		required: true
	},

	cost: {
    	type: Number,
    	required: true
    },

    numberOfServices: {
    	type: Number,
    	required: true
    },

	duration: {
		type: Number,
		required: true
	},

	isActive: {
		type: Boolean,
		default: true
	}

},
{
	timestamps:
	{
		createdAt: 'created_at',
		updatedAt: 'updated_at'
	}
}
);

module.exports = mongoose.model('subscription',subscriptionSchema,'subscription');
