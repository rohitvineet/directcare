var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var allpromo = new Schema({
    promoCode: {
        type: String,
        required: true
    },
    validUpto: {
        type: Date,
    },
    type: {
        type: String,
        required: true
    },
    /**
     * For percentage type this will be 0 -100
     *
     * For Discete discounts this will be ammount in cents
     */
    value: {
        type: Number,
        required: true
    },
    validForServiceType: {
        type: String,
    },
    maxUsageCountLimit: {
        type: Number,
        required: true,
        default: 1
    },
    usageCount: {
        type: Number,
        required: true,
        default: 0
    },
    updatedOn: {
        type: Number,
        required: true,
        default: Date.now
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
});
module.exports = mongoose.model('allpromos', allpromo);