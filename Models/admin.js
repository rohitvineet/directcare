'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CONST = require('../Config/constants');
var UniversalFunctions = require('../utils/UniversalFunctions');
const adminSchema = new Schema({

    email: {
    	type: String,
    	required: true,
    	unique: true,
    	trim: true,
    	validate: CONST.REGEX.EMAIL
    },

    password: {
    	type: String,
    	required: true,
    	select: false
    },

    name: {
    	type: String,
    	trim: true
    },

    accessToken: {
      type: String,
      unique: true,
      sparse: true
    },

    registrationDate: {
      type: Date, default: Date.now()
    },

    adminType: {
    	type: String,
    	enum: [
	    	UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.ADMIN_TYPE.SUPER_ADMIN,
	    	UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.ADMIN_TYPE.SUB_ADMIN
    	]
    },

	resetToken: {
		type: String,
		default: null
	}

},
{
	timestamps:
	 {
	 	createdAt: 'created_at', updatedAt: 'updated_at'
	 }
}
);

module.exports = mongoose.model('admin', adminSchema,'admin');
