'use strict';

const mongoose = require('mongoose');
const CONST = require('../Config/constants');
const Schema = mongoose.Schema;

const appointmentSchema = new Schema({

    domainOfServiceId :{
      type: Number,
      ref: 'domainOfService.key'
    },

    typeOfServiceId :{
      type: Number,
      ref: 'typeOfService.key'
    },

    diagnosticTypeOfServiceId :{
        type: Number,
        ref: 'diagnosticTypeOfService.key'
    },

    dates: {
        scheduleDate: {
            type: Date
        },
        bookingDate: {
            type: Date,
            default : Date.now()
        },
        startDate: {
            type: Date
        },
        cancelDate: {
            type: Date
        },
        endDate: {
            type: Date
        }
    },

    scheduleDate: {
        type: String
    },

    address: {
        customerLocation: {
            'type': {
                type: String,
                enum: CONST.GEO_JSON_TYPES.Point,
                default: CONST.GEO_JSON_TYPES.Point
            },

            coordinates: {
                type: [Number],
                default: [0, 0]
            }
        },

        streetAddress: {
            type: String,
            default: 'None'
        },

        isDeleted : {
            type : Boolean,
            default:false
        }
    },

    additionalDetails: {
        symptoms: {type: String},
        allergies: {type: String},
        medication: {type: String},
        specialInstruction: {type: String}
    },

    attachedImagesURL: [{type: String}],

    payment: {
        subTotal: {
            type: Number,
            default : 0
        },
      totalPrice: {
        type: Number,
        default : 0
      },
       discount: {
        type: Number,
        default : 0
      },
       status: {
        type: String,
        enum: [
            CONST.PAYMENT_STATUS.PENDING,
            CONST.PAYMENT_STATUS.COMPLETED
        ],
        default: CONST.PAYMENT_STATUS.PENDING
      },
       paymentMethod: {
        type:String,
        enum: [
            CONST.PAYMENT_METHOD.CARD,
            CONST.PAYMENT_METHOD.PROMO,
            CONST.PAYMENT_METHOD.BOTH,
            CONST.PAYMENT_METHOD.SUBSCRIPTION
        ]
      }
    },

    applicationStatus: {
      type: String,
      enum: [
          CONST.APPLICATION_STATUS.COMPLETED,
          CONST.APPLICATION_STATUS.STARTED,
          CONST.APPLICATION_STATUS.CONFIRMED,
          CONST.APPLICATION_STATUS.CANCELLED,
          CONST.APPLICATION_STATUS.SAMPLE_COLLECTED
      ],
      default: CONST.APPLICATION_STATUS.CONFIRMED
    },

    abilityToCallDoctor: {
      type: Boolean,
      default: false
    },

    videoConsultationSessionId: {
        type: String,
        default: null
    },

    promoCode: {
        type:String,
        default: null
    },

    prescription :{
       diagnosis: {
        type: String,
        default: null
       },
       medicine: {
        type: String,
        default: null
       },
       timesADay: {
        type: Number,
        default : 0
       },
       meal: {
        type: String,
        default: null
       },
       specialInstruction: {
        type: String,
        default: null
       }
    },

    patientId :{
      type: String,
      default: null,
      required: true
    },

    doctorId :{
      type: String,
      default: null
    },

    diagnosticId :{
      type: String,
      default: null
    },

    feedbackPatient :{
         feedback: {
          type: String,
          default: null
         },
         rating: {
          type: Number,
          default : 0
         },
         timestamp: {
          type: Date
         }
    },

    feedbackDoctor :{
       feedback: {
          type: String,
          default: null
        },
         rating: {
          type: Number,
          default : 0
        },
       timestamp: {
        type: Date
        }
    },

    feedbackDiagnostican :{
        feedback: {
            type: String,
            default: null
        },
        rating: {
            type: Number,
            default : 0
        },
        timestamp: {
            type: Date
        }
    },

    doctorNotes :{
           subject: {
            type: String,
            default: null
           },
           objective: {
            type: String,
            default: null
           },
           plan: {
            type: String,
            default: null
           },
           assessment: {
            type: String,
            default: null
           }
    },

    cancellation:{
           flag: {
            type: String,
            default: null
           },
           reason: {
            type: String,
            enum: [
                CONST.CANCEL_REASON.DOCTOR,
                CONST.CANCEL_REASON.PATIENT,
                CONST.CANCEL_REASON.NOT_CANCELLED
            ],
            default: CONST.CANCEL_REASON.NOT_CANCELLED
           },
           date: {
            type: Date,
            default : Date.now()
           }
    },

    collectSample:{
        type: Date
    }

  },
{
  timestamps:
  {
    createdAt: 'created_at' ,
    updatedAt: 'updated_at'
  }
}
);

appointmentSchema.index({'address.customerLocation': '2dsphere'});

module.exports = mongoose.model('appointment',appointmentSchema);
