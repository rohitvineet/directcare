'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const domainOfServiceSchema = new Schema({
    key:{
      type: Number,
      unique: true
    },
    name: {
    	type: String,
    	required: true
    }
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

domainOfServiceSchema.plugin(autoIncrement.plugin, {
    model: 'domainOfService',
    field: 'key',
    startAt: 56,
    incrementBy: 1
});


module.exports = mongoose.model('domainOfService',domainOfServiceSchema);
