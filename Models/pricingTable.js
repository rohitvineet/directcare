'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const pricingTableSchema = new Schema({
        key:{
            type: Number,
            unique: true
        },
        domainOfServiceId: {
            type: Number,
            ref: 'domainOfService.key'
        },
        typeOfServiceId:{
            type: Number,
            ref: 'typeOfService.key'
        },
        cost:{
            type: Number,
            required: true
        }
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

pricingTableSchema.plugin(autoIncrement.plugin, {
    model: 'pricingTable',
    field: 'key',
    startAt: 96,
    incrementBy: 1
});


module.exports = mongoose.model('pricingTable',pricingTableSchema);
