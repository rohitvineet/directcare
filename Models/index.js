'use strict';

module.exports = {
    appointment: require('./appointment'),
    doctor: require('./doctor'),
    domainOfService: require('./domainOfService'),
    typeOfService: require('./typeOfService'),
    patient: require('./patient'),
    subscription: require('./subscription'),
    admin: require('./admin'),
    diagnostic: require('./diagnostic'),
    diagnosticTypeOfService: require('./diagnosticTypeOfService'),
    pricingTable: require('./pricingTable'),
    geoFencing: require('./geoFencing'),
    cardDetails: require('./cardDetails'),
    promoCode: require('./promoCode')
};
