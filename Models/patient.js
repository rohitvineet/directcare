'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CONST = require('../Config/constants');

const ADDRESS = new Schema({

      customerLocation: {
        'type': {
          type: String, 
          enum: CONST.GEO_JSON_TYPES.Point, 
          default: CONST.GEO_JSON_TYPES.Point
        },

        coordinates: {
          type: [Number], 
          default: [0, 0]
        }
       },

        label: {
            type: String,
            default: 'None'
        },

        streetAddress: {
          type: String,
          default: 'None'
        },

        isDeleted : {
          type : Boolean,
          default:false
        }

      });

const patientSchema = new Schema({

    firstName: {
      type: String, 
      trim: true
    },

    lastName: {
      type: String, 
      trim: true
    },

    email: {
      type: String, 
      required: true, 
      unique: true,
      trim: true,
      validate: CONST.REGEX.EMAIL
    },
    
    password: {
      type: String, 
      required: true
    },

    mobileNo:{
        type:Number,
        unique: true
    },

    countryCode: {
        type:String
    },

    profilePicURL: {
      original: {
        type: String, 
        default: null
      },
      thumbnail: {
        type: String, 
        default: null
      }
    },

    accessToken :{
      type: String,
      unique: true,
      sparse: true
    },

    walletCredits :{
      type: Number, 
      default: 0
    },
    
    address: [{
        Address: ADDRESS
    }],

    subscription :[{
        id: {
          type: Schema.ObjectId,
          ref: 'subscription'
        },
        count: {
          type:Number, 
          default: 0
        },
        subscribedOn: {
          type:Date
        },
        expireDate: {
            type: Date
        },
   }],

    creditCardDetails :[{
      cardNumber: {
        type: String,
        trim: true, 
        required: true
      },
      cardType: {
        type: String, 
        trim: true, 
        required: true
      },
      cardToken: {
        type: String, 
        trim: true, 
        required: true
      },
      isDefault: {
        type: Boolean, 
        default: false
      },
      isActive: {
        type: Boolean, 
        default: true
      }
    }],

    isBlocked: {type: Boolean, default: false},

    isVerified: {
        type:Boolean,
        default: false
    },

    resetToken: {
        type: String,
        default: null
    }
  },
{
 timestamps: 
 { 
  createdAt: 'created_at', updatedAt: 'updated_at' 
 }
}
);

patientSchema.index({'address.Address.customerLocation': '2dsphere'});

module.exports = mongoose.model('patient',patientSchema,'patient');
