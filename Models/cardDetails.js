'use strict';

const mongoose = require('mongoose');
const CONST = require('../Config/constants');
const Schema = mongoose.Schema;

const cardDetailsSchema = new Schema({

        cardNumber: {
            type: String,
            required: true
        },
        cardType :{
            type: String
        },

        cardToken :{
            type: String,
            unique: true,
            sparse: true
        },

        isDefault: {
            type: Boolean,
            default: false
        },

        patientId :{
            type: Schema.ObjectId,
            ref: 'patient'
        }
    },
    {
        timestamps:
        {
            createdAt: 'created_at', updatedAt: 'updated_at'
        }
    }
);

module.exports = mongoose.model('cardDetails',cardDetailsSchema,'cardDetails');
