'use strict';

const mongoose = require('mongoose');
const CONST = require('../Config/constants');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

const doctorSchema = new Schema({

      referenceNumber: {
        type: Number,
        unique: true
      },

    firstName: {
        type: String,
        trim: true
    },

    lastName: {
        type: String,
        trim: true
    },

    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate: CONST.REGEX.EMAIL
    },

    password: {
        type: String,
        required:true,
        select: false
    },

    mobileNo:{
        type:Number,
        unique: true
    },

    countryCode: {
        type:String
    },

    accessToken :{
        type: String,
        unique: true,
        sparse: true
    },

    profilePicURL: {
        original: {
            type: String,
            default: null
        },
        thumbnail: {
            type: String,
            default: null
        }
    },

    domainOfServiceId :[{
        type: Number,
        ref: 'domainOfService.key'
    }],

    homeAddress: {
        customerLocation: {
        'type': {
          type: String,
          enum: CONST.GEO_JSON_TYPES.Point,
          default: CONST.GEO_JSON_TYPES.Point
        },

        coordinates: {
          type: [Number],
          default: [0, 0]
        }
       },

        streetAddress: {
          type: String,
          default: 'None'
        },

        isDeleted : {
          type : Boolean,
          default:false
        }

    },

    geoFencingId: {
        type: Schema.ObjectId,
        ref: 'geoFencing'
    },

    uploadedDocumentURL: {
        MDCN_certificate: {
            type: String
        },
        College_certificate: {
            type: String
        }
    },

    isVerified: {
        type: Boolean,
        default: false
    },

    isVerfiedByAdmin: {
        type: Boolean,
        default: false
    },

    rating: {
        type: Number,
        default: 0
    },

    userBlock: {
        type: Boolean,
        default: false
    },

    resetToken: {
        type: String,
        default: null
    }

},
{
    timestamps:
    {
        createdAt: 'created_at', updatedAt: 'updated_at'
    }
}
);

doctorSchema.plugin(autoIncrement.plugin, {
    model: 'doctor',
    field: 'referenceNumber',
    startAt: 1000,
    incrementBy: 1
});

doctorSchema.index({'homeAddress.customerLocation': '2dsphere'});

module.exports = mongoose.model('doctor',doctorSchema,'doctor');
