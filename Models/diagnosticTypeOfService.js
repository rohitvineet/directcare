'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const diagnosticTypeOfServiceSchema = new Schema({
	key:{
		type: Number,
		unique: true
	},
	name: {
    	type: String
    },
    subType: {
    	type: String,
    	default:null
    },
    cost: {
    	type: Number,
    	required: true
    }
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});


diagnosticTypeOfServiceSchema.plugin(autoIncrement.plugin, {
	model: 'diagnosticTypeOfService',
	field: 'key',
	startAt: 6,
	incrementBy: 1
});

module.exports = mongoose.model('diagnosticTypeOfService',diagnosticTypeOfServiceSchema,'diagnosticTypeOfService');
