'use strict';

const mongoose = require('mongoose');
const CONST = require('../Config/constants');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const geoFencingSchema = new Schema({

        key: {
            type: Number,
            unique: true,
        },

        locationName: {
            type: String,
            required: true
        },

        location: {
            'type': {type: String, enum: "Polygon", default: "Polygon"},
            coordinates: {type: Array}
        },

        isActive: {
            type: Boolean,
            default: true
        }
    },
    {
        timestamps:
        {
            createdAt: 'created_at', updatedAt: 'updated_at'
        }
    }
);


geoFencingSchema.plugin(autoIncrement.plugin, {
    model: 'geoFencing',
    field: 'key',
    startAt: 5000,
    incrementBy: 1
});

geoFencingSchema.index({'location': '2dsphere'});

module.exports = mongoose.model('geoFencing',geoFencingSchema,'geoFencing');
