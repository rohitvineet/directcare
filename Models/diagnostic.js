'use strict';

const mongoose = require('mongoose');
const CONST = require('../Config/constants');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);
const diagnosticSchema = new Schema({

    referenceNumber: {
        type: Number,
        unique: true
    },

    firstName: {
        type: String,
        trim: true
    },

    lastName: {
        type: String,
        trim: true
    },

    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate: CONST.REGEX.EMAIL
    },

    password: {
        type: String,
        required:true,
        select: false
    },

    mobileNo:{
        type: Number,
        unique: true
    },

    countryCode: {
        type: String
    },
    
    accessToken :{
        type: String,
        unique: true,
        sparse: true
    },

    profilePicURL: {
        original: {
            type: String,
            default: null
        },
        thumbnail: {
            type: String,
            default: null
        }
    },

    companyName :{
        type: String,
        default: 'none'
    },

    diagnosticTypeOfServiceId :[{
        type: Number,
        ref: 'diagnosticTypeOfService.key'
    }],

    homeAddress: {
        customerLocation: {
        'type': {
          type: String,
          enum: CONST.GEO_JSON_TYPES.Point,
          default: CONST.GEO_JSON_TYPES.Point
        },

        coordinates: {
          type: [Number],
          default: [0, 0]
        }
       },

        streetAddress: {
          type: String,
          default: 'None'
        },

        isDeleted : {
          type : Boolean,
          default:false
        }

    },

    geoFencingId: {
        type: Schema.ObjectId,
        ref: 'geoFencing'
    },

    uploadedDocumentURL: {
        Diagnostic_lab_registry: {
            type: String
        },
        Identity_certificate: {
            type: String
        }
    },

    isVerified: {
        type: Boolean,
        default: false
    },

    isVerfiedByAdmin: {
        type: Boolean,
        default: false
    },

    rating: {
        type: Number,
        default: 0
    },

    userBlock: {
        type: Boolean,
        default: false
    },

    resetToken: {
        type: String,
        default: null
    }

},
{
    timestamps:
    {
        createdAt: 'created_at', updatedAt: 'updated_at'
    }
}
);

diagnosticSchema.plugin(autoIncrement.plugin, {
    model: 'diagnostic',
    field: 'referenceNumber',
    startAt: 3000,
    incrementBy: 1
});


diagnosticSchema.index({'homeAddress.customerLocation': '2dsphere'});

module.exports = mongoose.model('diagnostic',diagnosticSchema,'diagnostic');
