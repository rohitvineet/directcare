'use strict';
/**
 * Created by Priya Sethi on 12/7/15.
 */
var Config = require('../Config');
var async = require('async');
var nodeMailerModule = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transport = nodeMailerModule.createTransport("SMTP",(Config.emailConfig.nodeMailer.Mandrill));
//var mandrillTransport = require('nodemailer-mandrill-transport');
//var transport = nodeMailerModule.createTransport(smtpTransport(Config.emailConfig.nodeMailer.Mandrill));
//var transport = nodeMailerModule.createTransport(mandrillTransport({
//    auth: {
//        apiKey: 'QLh2vY7dNMda3N_xyLyQ7w'
//    }
//}));
//var Path = require('path');
//var apns = require('apn');




var sendEmailToUser = function (emailType, emailVariables, emailId, callback) {
    var mailOptions = {
        from: 'veenit0327@gmail.com',
        to: emailId,
        subject: null,
        html: null
    };
    async.series([
        function (cb) {
            switch (emailType) {
                case 'REGISTRATION_MAIL' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.registrationEmail.emailMessage, emailVariables);
                    break;
                case 'FORGOT_PASSWORD' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.forgotPassword.emailMessage, emailVariables);
                    break;
                case 'BOOKING_CONFIRM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.BookingConfrmedForm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.BookingConfrmedForm.emailMessage, emailVariables);
                    break;
                case 'BOOKING_NOT_CONFIRM' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.bookingNotConfirm.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.bookingNotConfirm.emailMessage, emailVariables);
                    break;
                case 'BOOKING_COMPLETE' :
                    mailOptions.subject = Config.APP_CONSTANTS.notificationMessages.bookingComplete.emailSubject;
                    mailOptions.html = renderMessageFromTemplateAndVariables(Config.APP_CONSTANTS.notificationMessages.bookingComplete.emailMessage, emailVariables);
                    break;
            }
            cb();

        }, function (cb) {
            transport.sendMail({
                from: mailOptions.from,
                to: emailId,
                subject: mailOptions.subject,
                html: mailOptions.html
            }, function (err, info) {
                console.log(err, info);
                cb(null);
            })
            //sendMailViaTransporter(mailOptions, function (err, res) {
            //    cb(err, res);
            //})
        }
    ], function (err, responses) {
        if (err) {
            callback(err);
        } else {
            callback();
        }
    });

};

function renderMessageFromTemplateAndVariables(templateData, variablesData) {
    var Handlebars = require('handlebars');
    return Handlebars.compile(templateData)(variablesData);
}

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendSMS Function
 @ This function will initiate sending sms as per the smsOptions are set
 @ Requires following parameters in smsOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ Body:  // SMS text message
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */

/*
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @ sendMailViaTransporter Function
 @ This function will initiate sending email as per the mailOptions are set
 @ Requires following parameters in mailOptions
 @ from:  // sender address
 @ to:  // list of receivers
 @ subject:  // Subject line
 @ html: html body
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */
function sendMailViaTransporter(mailOptions, cb) {
    transporter.sendMail(mailOptions, function (error, info) {
        console.log('Mail Sent Callback Error:', error);
        console.log('Mail Sent Callback Ifo:', info);
    });
    cb(null, null) // Callback is outside as mail sending confirmation can get delayed by a lot of time
}




var register = function (code, email, message, callback) {
    transport.sendMail({
        from: 'veenit0327@gmail.com',
        to: email,
        subject: 'Welcome to Direct Care! Verify your Account.',
        html: message
    }, function (err, info) {
        callback(err, info);
    });
}

module.exports = {

    register: register,
   
};