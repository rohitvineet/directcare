'use strict';

module.exports = {
   admin : require('./admin'),
   patient: require('./patient'),
   service: require('./service'),
   doctor: require('./doctor'),
   diagnostic: require('./diagnostic'),
   appointment: require('./appointment'),
   cardDetails: require('./cardDetails'),
   subscription: require('./subscription'),
   promoCode: require('./promoCode')
};
