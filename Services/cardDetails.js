'use strict';

var Models = require('../Models');

//Get appointment details from DB
var getCardDetails = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.cardDetails.find(criteria, projection, options, callback);
};

//Insert appointment details in DB
var createCardDetails = function (objToSave, callback) {
    new Models.cardDetails(objToSave).save(callback)
};

//Update appointment details in DB
var updateCardDetails = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.cardDetails.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deleteCardDetails = function (criteria, callback) {
    Models.cardDetails.findOneAndRemove(criteria, callback);
};

module.exports = {
    getCardDetails: getCardDetails,
    createCardDetails: createCardDetails,
    updateCardDetails: updateCardDetails,
    deleteCardDetails: deleteCardDetails,
};
