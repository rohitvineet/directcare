
'use strict';

var Models = require('../Models');

//Get Users from DB
var getPatient = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.patient.find(criteria, projection, options, callback);
};

// aggregate function
var aggregatePatient = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.patient.aggregate({$match:criteria}, {$unwind: projection}, {$match:options}, callback);
};

//Insert User in DB
var createPatient = function (objToSave, callback) {
    new Models.patient(objToSave).save(callback)
};

//Update User in DB
var updatePatient = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.patient.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deletePatient = function (criteria, callback) {
    Models.patient.findOneAndRemove(criteria, callback);
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.patient.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};


module.exports = {
    getPatient: getPatient,
    createPatient: createPatient,
    updatePatient: updatePatient,
    deletePatient: deletePatient,
    aggregatePatient: aggregatePatient,
    getAllGeneratedCodes: getAllGeneratedCodes
};
