
/**
 * Created by cl-macmini-98 on 7/1/16.
 */

'use strict';

var Models = require('../Models');

//Get Subscription from DB
var getSubscription = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.subscription.find(criteria, projection, options, callback);
};

//Insert Subscription in DB
var createSubscription = function (objToSave, callback) {
    new Models.subscription(objToSave).save(callback)
};

//Update Subscription in DB
var updateSubscription = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.subscription.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deleteSubscription = function (criteria, callback) {
    Models.subscription.findOneAndRemove(criteria, callback);
};

module.exports = {
    getSubscription: getSubscription,
    createSubscription: createSubscription,
    updateSubscription: updateSubscription,
    deleteSubscription: deleteSubscription
};
