/**
 * Created by cl-macmini-98 on 7/1/16.
 */

'use strict';

var Models = require('../Models');

//Get Users from DB
var getDoctor = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.doctor.find(criteria, projection, options, callback);
};

//Insert User in DB
var createDoctor = function (objToSave, callback) {
    new Models.doctor(objToSave).save(callback)
};

//Update User in DB
var updateDoctor = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.doctor.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deleteDoctor = function (criteria, callback) {
    Models.doctor.findOneAndRemove(criteria, callback);
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.doctor.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};


module.exports = {
    getDoctor: getDoctor,
    createDoctor: createDoctor,
    updateDoctor: updateDoctor,
    deleteDoctor: deleteDoctor,
    getAllGeneratedCodes: getAllGeneratedCodes
};
