'use strict';

var Models = require('../Models');

//Get appointment details from DB
var getAppointment = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.appointment.find(criteria, projection, options, callback);
};

//Insert appointment details in DB
var createAppointment = function (objToSave, callback) {
    new Models.appointment(objToSave).save(callback)
};

//Update appointment details in DB
var updateAppointment = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.appointment.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deleteAppointment = function (criteria, callback) {
    Models.appointment.findOneAndRemove(criteria, callback);
};

module.exports = {
    getAppointment: getAppointment,
    createAppointment: createAppointment,
    updateAppointment: updateAppointment,
    deleteAppointment: deleteAppointment,
};
