/**
 * Created by cl-macmini-98 on 7/1/16.
 */

'use strict';

var Models = require('../Models');

//Get promoCode from DB
var getPromoCode = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.promoCode.find(criteria, projection, options, callback);
};

//Insert promoCode in DB
var createPromoCode  = function (objToSave, callback) {
    new Models.promoCode(objToSave).save(callback)
};

//Update promoCode in DB
var updatePromoCode  = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.promoCode.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deletePromoCode  = function (criteria, callback) {
    Models.promoCode.findOneAndRemove(criteria, callback);
};


module.exports = {
    getPromoCode: getPromoCode,
    createPromoCode: createPromoCode,
    updatePromoCode: updatePromoCode,
    deletePromoCode: deletePromoCode
};
