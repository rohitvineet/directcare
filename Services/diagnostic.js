
/**
 * Created by cl-macmini-98 on 7/1/16.
 */

'use strict';

var Models = require('../Models');

//Get Users from DB
var getDiagnostic = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.diagnostic.find(criteria, projection, options, callback);
};

//Insert User in DB
var createDiagnostic = function (objToSave, callback) {
    new Models.diagnostic(objToSave).save(callback)
};

//Update User in DB
var updateDiagnostic = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.diagnostic.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var deleteDiagnostic = function (criteria, callback) {
    Models.diagnostic.findOneAndRemove(criteria, callback);
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.diagnostic.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};


module.exports = {
    getDiagnostic: getDiagnostic,
    createDiagnostic: createDiagnostic,
    updateDiagnostic: updateDiagnostic,
    deleteDiagnostic: deleteDiagnostic,
    getAllGeneratedCodes: getAllGeneratedCodes
};
