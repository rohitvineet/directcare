
'use strict';

var Models = require('../Models');

var addDomain= function(objToSave, callback) {
    new Models.domainOfService(objToSave).save(callback)
};

var getAllDomain = function (criteria, projection, sortOptions,options,callback) {
    options.lean = true;
    Models.domainOfService.find(criteria, projection, options, callback);
};

var getAllType = function (criteria, projection, sortOptions,options,callback) {
    options.lean = true;
    Models.typeOfService.find(criteria, projection, options, callback);
};

var getAllPrice = function (criteria, projection, sortOptions,options,callback) {
    options.lean = true;
    Models.pricingTable.find(criteria, projection, options, callback);
};

var getAllDiagnosticTypes = function (criteria, projection, sortOptions,options,callback) {
    options.lean = true;
    Models.diagnosticTypeOfService.find(criteria, projection, options, callback);
};

var addType= function(objToSave, callback) {
    new Models.typeOfService(objToSave).save(callback)
};

var addPrice= function(objToSave, callback) {
    new Models.pricingTable(objToSave).save(callback)
};

var addGeo= function(objToSave, callback) {
    new Models.geoFencing(objToSave).save(callback)
};

var getAllGeo = function (criteria, projection, sortOptions,options,callback) {
    options.lean = true;
    Models.geoFencing.find(criteria, projection, options, callback);
};

var addDiagnostic= function(objToSave, callback) {
    new Models.diagnosticTypeOfService(objToSave).save(callback)
};

module.exports = {
    addDomain: addDomain,
    addType: addType,
    getAllDomain: getAllDomain,
    getAllType: getAllType,
    addPrice: addPrice,
    getAllPrice: getAllPrice,
    addDiagnostic: addDiagnostic,
    getAllDiagnosticTypes: getAllDiagnosticTypes,
    addGeo: addGeo,
    getAllGeo: getAllGeo
};
