'use strict';

const Hapi = require('hapi');
const Routes = require('./Routes');
const config = require('./Config');
const Plugins = require('./Plugins');
const mongoose = require('mongoose');
const MONGO_DB_URI = config.dbConfig.mongodbURI.local;


//collecting connection option
const PORT = config.serverConfig.PORT.DEV;
var connectionOptions = {
	port: PORT,
	routes: {
		cors: true
	}
};
//Create Server
var server = new Hapi.Server({
    app: {
        name: config.APP_CONSTANTS.SERVER.APP_NAME
    }
});
//server connection
server.connection(connectionOptions);

//Connect to MongoDB
mongoose.connect(MONGO_DB_URI, function (err) {
    if (err) {
        server.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected at', MONGO_DB_URI);
    }
});

 server.register(Plugins, function (err) {
    if (err) {
        server.error('Error while loading Plugins : ' + err)
    } else {
        server.log('info', 'Plugins Loaded')
    }


});

// API Routes
 Routes.forEach(function (api) {
   server.route(api);
 });

//start the server
server.start(function(err){

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
